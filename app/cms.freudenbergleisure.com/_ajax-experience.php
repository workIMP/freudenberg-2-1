<?php
require_once '../appdata/cms/bootstrap.php';
//error_reporting(E_ALL);

	//$uploadpath = APP_ROOT .'/uploads/';

	/*
	if(file_exists($_FILES['navitem-image']['tmp_name']))
	{
		move_uploaded_file($_FILES['navitem-image']['tmp_name'], APP_ROOT.'\\uploads\\'.$_FILES['navitem-image']['name']);
		echo json_encode(array('available' => true));

	}
	else
	{
		echo json_encode(array('available', false));
	}*/
	
	//echo json_encode(array('reveived' => 'Amila'));
	
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		// upload the image if submitted
		if(isset($_FILES['expitem-image']) && file_exists($_FILES['expitem-image']['tmp_name']))
		{
			// get type
			$type = end(explode('.', $_FILES['expitem-image']['name']));
			$filename = md5(time()) .'_'.mt_rand() .'.'.$type;

			if(!move_uploaded_file($_FILES['expitem-image']['tmp_name'], APP_ROOT . '\\public_html\images\\experiences\\'.$filename))
			{
				echo json_encode(array('status' => false, 'msg' => 'Failed uploading image! '.$filename));
				exit;
			}

		}

		if(isset($filename))
		{
			// a file has uploaded. include that also 
			$fields = '`tagline`,`description`,`image`,`destination_id`';
			$placeholders = '?,?,?,?';
			$values = array($_POST['expitem-tagline'], $_POST['expitem-desc'], $filename, $_POST['expitem-destination']);
		}
		else
		{
			// no file upload has taken place
			$fields = '`tagline`,`description`,`destination_id`';
			$placeholders = '?,?,?';
			$values = array($_POST['expitem-tagline'], $_POST['expitem-desc'], $_POST['expitem-destination']);
		}

		if($_POST['expitem-action'] == 'add')
		{
			if(!$db->addRecord('INSERT INTO tblexperiences ('.$fields.') VALUES('.$placeholders.')', $values))
			{
				echo json_encode(array('action' => 'add', 'status' => false, 'msg' => 'Failed adding record!'));
			}
			else
			{
				echo json_encode(array('action' => 'add', 'status' => true, 'msg' => 'success', 'page_id' => $_POST['expitem-page-id']));
			}			
		}
		else
		{
			$updstr = '';

			foreach(explode(',', $fields) as $field)
			{
				$updstr .= $field .'=?,';
			}

			//$updstr = substr($updstr, strlen($updstr) - 1);
			$updqry = 'UPDATE tblexperiences SET '. (substr($updstr, 0, strlen($updstr) - 1)).' WHERE `id` = ?';
			array_push($values, $_POST['expitem-id']);

			if(!$db->updateRecord($updqry, $values))
			{
				echo json_encode(array('action' => 'update', 'status' => false, 'msg' => print_r($db->getError(), true), 'qry' => $updqry));
			}
			else
			{
				echo json_encode(array('action' => 'update', 'status' => true, 'msg' => 'update success!', 'page_id' => $_POST['expitem-page-id']));
			}			
		}

	}