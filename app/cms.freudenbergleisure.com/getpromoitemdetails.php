<?php
require '../appdata/cms/bootstrap.php';

	// get navigation item list
	$promoitemid = (int)$_GET['promoitem'];

	$promodata = $db->getRow('SELECT tblpromotions.*, tblpromotionimages.image_src, tblproperties.property_slug FROM tblpromotions LEFT JOIN tblpromotionimages ON tblpromotions.id = tblpromotionimages.promotion_id INNER JOIN tblproperties ON tblpromotions.property_id = tblproperties.id WHERE tblpromotions.id = ?', array($promoitemid));

	echo json_encode($promodata);