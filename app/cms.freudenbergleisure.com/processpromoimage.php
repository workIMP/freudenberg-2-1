<?php
require '../appdata/cms/bootstrap.php';

//echo json_encode(array('postdata' => $_POST, 'filedata' => $_FILES));		//works
//exit;

$propertydata = $db->getRow('SELECT tblproperties.property_slug FROM tblproperties INNER JOIN tblpromotions ON tblpromotions.property_id = tblproperties.id WHERE tblpromotions.id = ?', array((int)$_POST['promo-id']));

//echo json_encode($propertydata);
//exit;

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

		// upload the image if submitted
		if(isset($_FILES['promopop-image']) && file_exists($_FILES['promopop-image']['tmp_name']))
		{
			// get type
			$type = end(explode('.', $_FILES['promopop-image']['name']));
			$filename = md5(time()) .'_'.mt_rand() .'.'.$type;

			$uploaddir = APP_ROOT . '/uploads/';
			$originalpath = APP_ROOT.'/public_html/images/promotions/'.$propertydata->property_slug.'/popups/';		


			if(!move_uploaded_file($_FILES['promopop-image']['tmp_name'], $uploaddir.$filename))
			{
				echo json_encode(array('status' => false, 'msg' => 'Failed uploading image! '.$filename));
				exit;
			}

			// move file
			rename($uploaddir.$filename, $originalpath.$filename);				

		}

		switch($_POST['do'])
		{

			case 'add':				

				if(isset($filename))
				{
					// add db record
					$addpromoimg = $db->addRecord('INSERT INTO tblpromotionimages (`promotion_id`,`image_src`) VALUES(?,?)', array((int)$_POST['promo-id'], $filename));


					if($addpromoimg)
					{
						echo json_encode(array('action' => 'add', 'status' => true, 'msg' => 'Image Upload Success!'));
					}
					else
					{
						echo json_encode(array('action' => 'add', 'status' => false, 'msg' => 'Failed uploading Promotion Image!'));
					}

				}

			break;

			case 'update':

				// update image details
				if(isset($filename))
				{
					// get current image name
					$currentimagedata = $db->getRow('SELECT * FROM tblpromotionimages WHERE id = ?', array($_POST['image-id']));

					// unlink current image
					if(unlink(APP_ROOT .'/public_html/images/promotions/'.$propertydata->property_slug.'/popups/'.$currentimagedata->image_src))
					{
						// image delete success -> now update db record
						$updres = $db->updateRecord('UPDATE tblpromotionimages SET `image_src` = ? WHERE id = ?', array($filename, (int)$_POST['image-id']));

						if($updres)
						{
							echo json_encode(array('action' => 'update', 'status' => true, 'msg' => 'update success'));
						}
						else
						{
							echo json_encode(array('action' => 'update', 'status' => false, 'msg' => print_r($db->getError(), true)));
						}						

					}
					else
					{
						echo json_encode(array('action' => 'delete-file', 'status' => false, 'msg' => APP_ROOT.'\public_html\images\\promotions'.$propertydata->property_slug.'\\popups\\'.$currentimagedata->image_src));
					}	
					
				}

			break;

			case 'delete':

				// get current image name
				$currentimagedata = $db->getRow('SELECT * FROM tblpromotionimages WHERE id = ?', array($_POST['image-id']));


				// unlink current image
				if(unlink(APP_ROOT .'/public_html/images/promotions/'.$propertydata->property_slug.'/popups/'.$currentimagedata->image_src))
				{
					// image delete success -> now delete db record
					$deleteimg = $db->deleteRecord('DELETE FROM tblpromotionimages WHERE id = ?', array((int)$_POST['image-id']));

					if($deleteimg)
					{
						echo json_encode(array('action' => 'delete', 'status' => true, 'msg' => 'delete success'));
					}
					else
					{
						echo json_encode(array('action' => 'delete', 'status' => false, 'msg' => print_r($db->getError(), true)));
					}						

				}
				else
				{
					echo json_encode(array('action' => 'delete-file', 'status' => false, 'msg' => APP_ROOT.'\public_html\images\\promotions'.$propertydata->property_slug.'\\popups\\'.$currentimagedata->image_src));
				}

			break;

		}
}
