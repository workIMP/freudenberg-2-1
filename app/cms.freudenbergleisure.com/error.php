<html>
	<head>
		<title>An Error Encountered!</title>
		<style type="text/css">
			.wrapper{
				width: 960px;
				margin: 0 auto;
			}
		</style>
	</head>
	<body>
		<div class='wrapper'>
			<h1>An Error Occurred!</h1>
			<p>
				<?=$exception->getMessage()?>
			</p>
		</div>
	</body>
</html>