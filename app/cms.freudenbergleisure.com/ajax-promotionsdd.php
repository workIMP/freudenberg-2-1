<?php
require_once '../appdata/cms/bootstrap.php';
//error_reporting(E_ALL);

	//$uploadpath = APP_ROOT .'/uploads/';

	/*
	if(file_exists($_FILES['navitem-image']['tmp_name']))
	{
		move_uploaded_file($_FILES['navitem-image']['tmp_name'], APP_ROOT.'\\uploads\\'.$_FILES['navitem-image']['name']);
		echo json_encode(array('available' => true));

	}
	else
	{
		echo json_encode(array('available', false));
	}*/
	
	//echo json_encode($_POST);
	//exit;

	$propertydata = $db->getRow('SELECT property_slug FROM tblproperties WHERE `id` = ?', array((int)$_POST['promoitem-property']));
	
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		// upload the image if submitted
		if(isset($_FILES['promoitem-image']) && file_exists($_FILES['promoitem-image']['tmp_name']))
		{
			// get type
			$type = end(explode('.', $_FILES['promoitem-image']['name']));
			$filename = md5(time()) .'_'.mt_rand() .'.'.$type;

			$uploaddir = APP_ROOT . '/uploads/';
			$originalpath = APP_ROOT.'/public_html/images/promotions/'.$propertydata->property_slug.'/';				

			if(!move_uploaded_file($_FILES['promoitem-image']['tmp_name'], $uploaddir.$filename))
			{
				echo json_encode(array('status' => false, 'msg' => 'Failed uploading image! '.$filename));
				exit;
			}

			// move file
			rename($uploaddir.$filename, $originalpath.$filename);			

		}

		//$haspage = (isset($_POST['promoitem-trigger']) ? 1 : 0);
		$promostatus = (isset($_POST['promoitem-enabled']) ? 1 : 0);


		if(isset($filename))
		{
			// a file has uploaded. include that also. for both new promotions and updating details with the image 
			$fields = '`property_id`,`title`,`description`,`image`,`enabled`,`trigger_action`';
			$placeholders = '?,?,?,?,?,?';
			$values = array((int)$_POST['promoitem-property'], $_POST['promoitem-title'], $_POST['promoitem-desc'], $filename, $promostatus, $_POST['promoitem-trigger']);
		}
		else
		{
			// no file upload has taken place. only used when updating details
			$fields = '`property_id`,`title`,`description`,`enabled`,`trigger_action`';
			$placeholders = '?,?,?,?,?';
			$values = array((int)$_POST['promoitem-property'], $_POST['promoitem-title'], $_POST['promoitem-desc'], $promostatus, $_POST['promoitem-trigger']);
		}

		if($_POST['promoitem-action'] == 'add')
		{

			$addres = $db->addRecord('INSERT INTO tblpromotions ('.$fields.') VALUES('.$placeholders.')', $values);
			
			if(!$addres)
			{
				echo json_encode(array('action' => 'add', 'status' => false, 'msg' => 'Failed adding Promotion!'));
				exit;
			}

			// create a dir
			//mkdir(APP_ROOT.'\\public_html\\images\\promotions\\'.$propertydata->property_slug.'\\'.$addres);

			// upload promo trigger image if submitted
			/*
			if($_POST['promoitem-trigger'] == 'image')
			{
				if(isset($_FILES['promoitem-trigger2-image']) && file_exists($_FILES['promoitem-trigger2-image']['tmp_name']))
				{
					// get type
					$type = end(explode('.', $_FILES['promoitem-trigger2-image']['name']));
					$filename = md5(time()) .'_'.mt_rand() .'.'.$type;					

					if(!move_uploaded_file($_FILES['promoitem-trigger2-image']['tmp_name'], APP_ROOT . '\\public_html\images\\promotions\\'.$propertydata->property_slug.'\\'.$addres.'\\'.$filename))
					{
						echo json_encode(array('status' => false, 'msg' => 'Failed uploading image! '.$filename));
						exit;
					}

					$addpromoimg = $db->addRecord('INSERT INTO tblpromotionimages (`promotion_id`,`image_src`) VALUES(?,?)', array($addres, $filename));

				}				
			}*/



			echo json_encode(array('action' => 'add', 'status' => true, 'msg' => 'success', 'item_id' => $addres));
			exit;			

			/* NO NEED
			else
			{
				// promo added.
				if($haspage)
				{
					// add a new page
					//$addpgres = $db->addRecord('INSERT INTO tblpromotionpages (`promotion_id`,`title`,`description`) VALUES(?,?,?)', array($addres, $_POST['promoitem-page-title'], $_POST['promoitem-page-desc']));
					$addpgres = $db->addRecord('INSERT INTO tblpages (`page_name`,`page_template`,`child_of`,`property_id`,`page_slug`,`section_desc`,`section_nav`,`section_slider`,`section_gallery`,`allow_subpages`,`show_tabs`,`visible_in`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)', array($_POST['promoitem-page-title'],'standard-page.tpl',29,(int)$_POST['promoitem-property'],strtolower($_POST['promoitem-page-title']),1,0,1,1,0,1,3));

					if(!$addpgres)
					{
						echo json_encode(array('action' => 'add', 'status' => false, 'msg' => 'Failed adding Page!'));
						exit;
					}

					// update promotion table with page id
					$db->updateRecord('UPDATE tblpromotions SET `page_id` = ? WHERE id = ?', array($addpgres, $addres));
				}

				echo json_encode(array('action' => 'add', 'status' => true, 'msg' => 'success', 'item_id' => $addres));
				exit;
			}
			*/		
		}
		else
		{
			$updstr = '';

			foreach(explode(',', $fields) as $field)
			{
				$updstr .= $field .'=?,';
			}

			//$updstr = substr($updstr, strlen($updstr) - 1);
			$updqry = 'UPDATE tblpromotions SET '. (substr($updstr, 0, strlen($updstr) - 1)).' WHERE `id` = ?';
			array_push($values, $_POST['promoitem-id']);

			$updres = $db->updateRecord($updqry, $values);

			
			if(!$updres)
			{
				echo json_encode(array('action' => 'update', 'status' => false, 'msg' => print_r($db->getError(), true), 'qry' => $updqry, 'vals' => $values));
				exit;
			}		

			/* NO NEED
			// check tblpromotionpages to see if there is a promotion_id field matching $_POST['promoitem-id']
			$pagedata = $db->getRow('SELECT id FROM tblpromotionpages WHERE promotion_id = ?', array((int)$_POST['promoitem-id']));				
			
			$pgupdstatus = true;

			// page is checked
			if($haspage)
			{
				//if(isset($_POST['promoitem-id']))					

				if(isset($pagedata->id))
				{
					// has a page 
					// see if we need to update it
					if(isset($_POST['promoitem-page-title']) && isset($_POST['promoitem-page-desc']))
					{
						// fields present -> update 
						$pgupdstatus = $db->updateRecord('UPDATE tblpromotionpages SET `title` = ?, `description` = ? WHERE id = ?', array($_POST['promoitem-page-title'], $_POST['promoitem-page-desc'], $pagedata->id));


						
						//if(!$updpgres)
						//{
							//echo json_encode(array('action' => 'update', 'status' => false, 'msg' => 'Failed updating page details!'));
							//exit;
						//}
					}
				}
				else
				{
					// add a new page
					$pgupdstatus = $db->addRecord('INSERT INTO tblpromotionpages (`promotion_id`,`title`,`description`) VALUES(?,?,?)', array((int)$_POST['promoitem-id'], $_POST['promoitem-page-title'], $_POST['promoitem-page-desc']));

					
					//if(!$addpgres)
					//{
						//echo json_encode(array('action' => 'add', 'status' => false, 'msg' => 'Failed adding Page!'));
						//exit;
					//}
				}

			}
			else
			{
				// page is not checked
				if(isset($pagedata->id))
				{
					// has a page -> delete it
					$pgupdstatus = $db->deleteRecord('DELETE FROM tblpromotionpages WHERE id = ?', array($pagedata->id));

					@$db->updateRecord('UPDATE tblpromotionpages SET `haspage` = ? WHERE id = ?', array(0,$_POST['promoitem-id']));

					
					//if(!$delpg)
					//{
						//echo json_encode(array('action' => 'update', 'status' => false, 'msg' => 'Failed deleting page!'));
						//exit;							
					//}
				}
			}

			*/

			//$msg = (($updres && $pgupdstatus) ? 'Promotion Details and Page Details Successfully Updated!' : (($updres && !$pgupdstatus) ? 'Promotion Details Updated! Page Details Update Failed!' : ((!$updres && $pgupdstatus) ? 'Promotion Details Update Failed! Page Details Updated!' : 'Promotion Details and Page Details Update Failed!')));

			//$status = (!$updres && !$pgupdstatus) ? false : true;

			/*

			if(!is_dir(APP_ROOT.'\\public_html\\images\\promotions\\'.$propertydata->property_slug.'\\'.$_POST['promoitem-id']))
			{
				mkdir(APP_ROOT.'\\public_html\\images\\promotions\\'.$propertydata->property_slug.'\\'.$_POST['promoitem-id']);
			}

			// upload promo trigger image if submitted
			if($_POST['promoitem-trigger'] == 'image')
			{
				if(isset($_FILES['promoitem-trigger2-image']) && file_exists($_FILES['promoitem-trigger2-image']['tmp_name']))
				{
					// get type
					$type = end(explode('.', $_FILES['promoitem-trigger2-image']['name']));
					$filename = md5(time()) .'_'.mt_rand() .'.'.$type;					

					if(!move_uploaded_file($_FILES['promoitem-trigger2-image']['tmp_name'], APP_ROOT . '\\public_html\images\\promotions\\'.$propertydata->property_slug.'\\'.$_POST['promoitem-id'].'\\'.$filename))
					{
						echo json_encode(array('status' => false, 'msg' => 'Failed uploading image! '.$filename));
						exit;
					}


					// check to see if we have an image already
					$getimg = $db->getRow('SELECT id FROM tblpromotionimages WHERE `promotion_id` = ?', array($_POST['promotion-id']));

					if(isset($getimg->id))
					{
						// already has -> run an update query
						$updatepromoimg = $db->updateRecord('UPDATE tblpromotionimages SET `image_src` = ? WHERE `id` = ?', array($filename, $_POST['promotion_id']));
					}
					else
					{
						// add new
						$addpromoimg = $db->addRecord('INSERT INTO tblpromotionimages (`promotion_id`,`image_src`) VALUES(?,?)', array($addres, $filename));
					}
					

				}				
			} */

			echo json_encode(array('action' => 'update', 'status' => true, 'msg' => 'Promotion Details Updated!', 'posted_values' => $_POST));
			exit;
						
		}

	}
