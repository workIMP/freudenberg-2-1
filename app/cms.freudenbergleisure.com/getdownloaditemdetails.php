<?php
require '../appdata/cms/bootstrap.php';

	// get navigation item list
	$downloaditemid = (int)$_GET['downloaditem'];

	$downloaditemdata = $db->getRow('SELECT tbldownloaditems.*, tblproperties.property_slug FROM tbldownloaditems INNER JOIN tblproperties ON tbldownloaditems.property_id = tblproperties.id WHERE tbldownloaditems.id = ?', array($downloaditemid));

	echo json_encode($downloaditemdata);