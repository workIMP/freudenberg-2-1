function getPageGallery(baseurl, pg, data)
{
	//console.log(baseurl);
	//console.log(pg);
	//console.log(data);
	console.log('in function getPageGallery -> loading '+data);
	var url = baseurl+'/getsliderimages.php?pgid='+pg+'&section='+data+'&ts='+new Date().getTime();

	//console.log('ajax returned');

	$(".slider-image-list").load(url);
}


function populatePgSlider(baseurl, pg)
{
	console.log('loading slider');
	var url = baseurl+'/getsliderimages.php?pgid='+pg+'&ts='+new Date().getTime();

	$(".slider-image-list").load(url);
}

function populatePgDesc(baseurl, pg)
{
	$.get(baseurl+'/getdata.php', { pgid: pg }, function(res){

		//$('.nav-item-list').html(navitemlist);
		//console.log(navitemlist);
		data = JSON.parse(res);

		/*
		tinymce.init({
			selector: '#bodytext',
			inline: true,
			toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
			menubar: false
		});*/

		/*
	    $('#frmpgdesc textarea[name=bodytext]').froalaEditor({
	      toolbarInline: true,
	      charCounterCount: false,
	      toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'color', 'emoticons', '-', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent', '-', 'insertImage', 'insertLink', 'insertFile', 'insertVideo', 'undo', 'redo']
	    });*/

		// populate form here
		$('#frmpgdesc input[name=subtitle]').val(data.sub_title);

		$('#frmpgdesc input[name=maintitle]').val(data.main_title);
		
		/*
		$('#frmpgdesc textarea[name=bodytext]').val(data.body_text).froalaEditor({
			heightMin: 350,
			heightMax: 500,
			toolbarInline: true,
			theme: 'royal',
			fontSize: ['8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25']
		});*/

		// find div.fr-inline element and add form-control class
		//$('div.fr-inline').css('height', '300px').addClass('form-control');

		//$('#frmpgdesc textarea[name=bodytext]').val(data.body_text);			// this we'll replace with tinymce
		tinymce.get('bodytext').setContent(data.body_text);

		$('#frmpgdesc input[name=desc-id]').val(isNaN(data.id) ? 0 : data.id);

		$('#frmpgdesc input[name=desc-page-id]').val(pg);
	});

	//$('#frmpgdesc textarea[name=bodytext]').froalaEditor();
}

function populatePgNav(baseurl, pg)
{
	$.get(baseurl+'/getnavitemlist.php', { pgid: pg }, function(navitemlist){
		$('.nav-item-list').html(navitemlist);
		//console.log(navitemlist);
	})	
}

function populatePgExp(baseurl)
{
	$.get(baseurl+'/getexpitemlist.php', function(expitemlist){
		$('.exp-item-list').html(expitemlist);
		//console.log(navitemlist);
	})	
}


function getPropertyList(baseurl, target, prop, sel = 0)
{

	//var el = $('#frmnavigation select[name=navitem-target-pg-property]');
	var el = target;

	el.html('');
	el.append('<option value="">Select Property</option>');

	$.get(baseurl+'/getpropertylist.php', {propid: prop}, function(propertylist){

		list = JSON.parse(propertylist);		

		$.each(list, function(i,property){

			el.append('<option value="'+property.id+'" '+(sel != 0 && sel == property.id ? 'selected="selected"' : '')+'>'+property.property_name+'</option>');

		})

	})	
}

function getPagesByProperty(baseurl, propval, sel = 0)
{
	// populate properties select list
	var selpropval = propval;
	console.log('PRID :'+selpropval);
	console.log('Select page: '+sel);

	// populate target select
	var el = $('#frmnavigation select[name=navitem-target]');

	el.html('');
	
	//el.val('').change();

	if(selpropval != '')
	{
		el.append('<option value="">Select Page</option>');

		$.get(baseurl+'/getpageslist.php', { prid:selpropval }, function(pageslist){

			list = JSON.parse(pageslist);
			var mainpages = list.main_pages;
			var subpages = list.sub_pages;


			console.log(subpages);

			//el = $('#frmnavigation select[name=navitem-target]');

			$.each(mainpages, function(i,pg){
				if(pg.id in subpages)
				{
					var pid = pg.id;
					var submenuitems = subpages[pid];
					console.log('page '+pg.id+' has sub pages');
					el.append('<option value="'+pg.id+'" '+(sel != 0 && sel == pg.id ? 'selected="selected"' : '')+'>'+pg.page_name+'</option>');
					//console.log(submenuitems);

					$.each(submenuitems, function(si, spg){
						el.append('<option value="'+spg.id+'" '+(sel != 0 && sel == spg.id ? 'selected="selected"' : '')+'>&nbsp;&nbsp;'+spg.page_name+'</option>');
					})

				}
				else
				{
					el.append('<option value="'+pg.id+'" '+(sel != 0 && sel == pg.id ? 'selected="selected"' : '')+'>'+pg.page_name+'</option>');
				}
			})

			/*
			el = $('#frmnavigation select[name=navitem-target]');

			$.each(list, function(index, value){
				el.prepend('<option value="'+value.id+'">'+value.page_name+'</option>');
			})

			//el.prepend('<option value="">Select A Property</option>');
			*/

			//el.append('<option value="29">Promotions</option>');
		})					
	}
	else	
	{
		el.append('<option value="">Select a Property First ...</option>');
	}	
}

function getHomePageProperties(baseurl)
{
	$.get(baseurl+'/getpropitemlist.php', function(propitemlist){
		$('.prop-item-list').html(propitemlist);
		//console.log(navitemlist);
	})		
}

function getPromotionData(baseurl, prop)
{
	$.get(baseurl+'/getpromoitemlist.php', { propertyid: prop }, function(promoitemlist){
		$('.promo-item-list').html(promoitemlist);
		//console.log(navitemlist);
	})	
}

function getDownloadItemData(baseurl, prop)
{
	$.get(baseurl+'/getdownloaditemlist.php', { propertyid: prop }, function(downloaditemlist){
		$('.download-item-list').html(downloaditemlist);
		//console.log(navitemlist);
	})	
}