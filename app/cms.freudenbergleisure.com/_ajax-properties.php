<?php
require_once '../appdata/cms/bootstrap.php';
//error_reporting(E_ALL);

	//$uploadpath = APP_ROOT .'/uploads/';

	/*
	if(file_exists($_FILES['navitem-image']['tmp_name']))
	{
		move_uploaded_file($_FILES['navitem-image']['tmp_name'], APP_ROOT.'\\uploads\\'.$_FILES['navitem-image']['name']);
		echo json_encode(array('available' => true));

	}
	else
	{
		echo json_encode(array('available', false));
	}*/
	
	//echo json_encode(array('reveived' => 'Amila'));
	
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		// upload the image if submitted
		if(isset($_FILES['propitem-image']) && file_exists($_FILES['propitem-image']['tmp_name']))
		{
			// get type
			$type = end(explode('.', $_FILES['propitem-image']['name']));
			$filename = md5(time()) .'_'.mt_rand() .'.'.$type;

			if(!move_uploaded_file($_FILES['propitem-image']['tmp_name'], APP_ROOT . '\\public_html\images\\properties\\'.$filename))
			{
				echo json_encode(array('status' => false, 'msg' => 'Failed uploading image! '.$filename));
				exit;
			}

		}

		if(isset($filename))
		{
			// a file has uploaded. include that also 
			$fields = '`prop_name`,`prop_description`,`prop_image`,`prop_link`,`browser_window`';
			$placeholders = '?,?,?,?,?';
			$values = array($_POST['propitem-name'], $_POST['propitem-desc'], $filename, $_POST['propitem-tlink'], $_POST['propitem-browsertab']);
		}
		else
		{
			// no file upload has taken place
			$fields = '`prop_name`,`prop_description`,`prop_link`,`browser_window`';
			$placeholders = '?,?,?,?';
			$values = array($_POST['propitem-name'], $_POST['propitem-desc'], $_POST['propitem-tlink'], $_POST['propitem-browsertab']);
		}

		if($_POST['propitem-action'] == 'add')
		{
			if(!$db->addRecord('INSERT INTO tbl_home_property_data ('.$fields.') VALUES('.$placeholders.')', $values))
			{
				echo json_encode(array('action' => 'add', 'status' => false, 'msg' => 'Failed adding record!'));
			}
			else
			{
				echo json_encode(array('action' => 'add', 'status' => true, 'msg' => 'success', 'page_id' => $_POST['propitem-page-id']));
			}			
		}
		else
		{
			$updstr = '';

			foreach(explode(',', $fields) as $field)
			{
				$updstr .= $field .'=?,';
			}

			//$updstr = substr($updstr, strlen($updstr) - 1);
			$updqry = 'UPDATE tbl_home_property_data SET '. (substr($updstr, 0, strlen($updstr) - 1)).' WHERE `id` = ?';
			array_push($values, $_POST['propitem-id']);

			if(!$db->updateRecord($updqry, $values))
			{
				echo json_encode(array('action' => 'update', 'status' => false, 'msg' => print_r($db->getError(), true), 'qry' => $updqry));
			}
			else
			{
				echo json_encode(array('action' => 'update', 'status' => true, 'msg' => 'update success!', 'page_id' => $_POST['propitem-page-id']));
			}			
		}

	}