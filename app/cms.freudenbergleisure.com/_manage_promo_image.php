<?php
require '../appdata/cms/bootstrap.php';

	$promoid = (int)$_GET['promo'];

	//$image = $_GET['image'];

	//list($imagename, $ext) = explode('.', $image);

	//$imageid = substr($imagename, -32);
	//$imageid = $imagename;

	$promoimagedata = $db->getRow('SELECT tblproperties.property_slug, tblpromotionimages.* FROM `tblpromotions` INNER JOIN tblproperties ON tblpromotions.property_id = tblproperties.id LEFT JOIN tblpromotionimages ON tblpromotions.id = tblpromotionimages.promotion_id WHERE tblpromotions.id = ?', array($promoid));


?>
<div class="image-detail-view white-popup">
	<div id="promopop-image-preview" style="float: left; width: 330px;">
		<?php if(!$promoimagedata->id): ?>
		No image available. Please upload!
		<?php else: ?>
		<img src="<?=LIVE_SITE_URL.'/images/promotions/'.$promoimagedata->property_slug.'/'.$promoimagedata->image_src?>" style="width: 100%;" />
		<?php endif; ?>
	</div>
	<div class="" style="margin-left: 330px; padding-left: 10px;">
		<form id="frmpromoimgprocess" action="<?=SITE_URL.'/processpromoimage.php'?>">
                <div class="form-group">
                  <label for="promopop-image"><?php echo (isset($promoimagedata->id) ? 'Change Image' : 'Upload an Image'); ?> <span class="requiredmsg"></span></label><br>
                  <input type="file" id="promopop-image" name="promopop-image" data-preview-element="promopop-image-preview" onchange="preview_image(this);"/>
                  <p class="help-block">Example block-level help text here.</p>
                </div>

                <!--
				<div class="form-group">
					<label for="promopop-image-alttext">Alt Tag Text <span style="font-size: 10px;">(Optional)</span></label><br>
					<input type="text" name="promopop-image-alttext" id="promopop-image-alttext" value=""/>
				</div>

				<div class="form-group">
					<label for="promopop-image-desctext">Short Caption <span style="font-size: 10px;">(Optional)</span></label><br>
					<textarea name="promopop-image-desctext" id="promopop-image-desctext" rows="3" cols="50"></textarea>
				</div>-->

				<div class="form-group">
					<input type="hidden" name="promo-item-id" value="<?=$promoid?>"/>
					<input type="hidden" name="promopop-image-id" value="<?php echo ($promoimagedata->id ? $promoimagedata->id : 0) ?>"/>
					<input type="submit" name="submit" data-action="<?php echo ($promoimagedata->id ? 'update' : 'add') ?>" value="<?php echo ($promoimagedata->id ? 'Update Image' : 'Save') ?>"/><?php ($promoimagedata->id ? '&nbsp;&nbsp;<input type="submit" name="submit" data-action="delete" value="Delete Image"/>' : '') ?>
				</div>

		</form>
	</div>
	<br style="clear: left;"/>
</div>
