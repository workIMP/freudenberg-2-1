<?php $pageNavigation = $cms->getNavigation(); ?>
<ul>
<?php foreach($pageNavigation as $navitem): ?>
    <li class="highlight" style="background: #ebebeb url('<?=PARENT_DOMAIN?>images/navigation/<?=$navitem->page_id?>/<?=$navitem->image?>') no-repeat 50% 50%; background-size: cover;">
        <a class="highlight-hotspot" href="<?=$cms->getPageUrl($navitem->target_pg,'facilities')?>"></a>
        <div class="highlight-background"></div><!--  .highlight-background  -->
        <div class="highlight-content">
            <div class="highlight-content-inside">       
                <h2 class="hdr-four"><?=$navitem->title?></h2>    
                <div class="twoo fadeitem"><?=$navitem->description?></div>
            </div><!--  .highlight-content-inside  -->
        </div><!--  .highlight-content  -->
    </li><!--  .highlight  -->
<?php endforeach; ?>
</ul>