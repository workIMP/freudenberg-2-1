<?php $pageNavigation = $cms->getNavigation(); ?>
<ul>
<?php foreach($pageNavigation as $navitem): ?>
    <li class="highlight" style="background: #ebebeb url('<?=PARENT_DOMAIN?>images/navigation/<?=$navitem->page_id?>/<?=$navitem->image?>') no-repeat 50% 50%; background-size: cover;">
        <a class="highlight-hotspot" href="<?=$cms->getPageUrl($navitem->target_pg, $mnusec)?>"></a>
        <div class="highlight-background"></div><!--  .highlight-background  -->
        <div class="highlight-content">
            <div class="highlight-content-inside">       
                <h2 class="hdr-four"><?=$navitem->title?></h2> 
                <?php
                    $parts = explode("\n", $navitem->description);
                   
                    if(count($parts) == 2)
                    {
                        list($tagline, $btntext) = $parts;
                        ?>
                        <div class="hdr-two fadeitem"><?=(isset($tag_em) ? '<em>'.$tagline.'</em>' : $tagline)?></div>
                        <a class="<?=(isset($link_style) ? $link_style : 'btn-underline btn-underline--big-white fadeitem')?>" href="#"><?=$btntext?></a>
                        <?php
                    }
                    else if(count($parts) == 3)
                    {
                        list($tagline, $desc, $btntext) = $parts;
                        ?>
                        <div class="hdr-two fadeitem"><?=$tagline?></div>
                        <p class="fadeitem"><?=$desc?></p>
                        <a href="" class="<?=(isset($link_style) ? $link_style : 'btn-arrow fadeitem')?>"><?=$btntext?></a>
                        <?php
                    }
                    else
                    {
                        ?>
                        <a class="<?=(isset($link_style) ? $link_style : 'btn-underline btn-underline--big-white fadeitem')?>" href="#"><?=trim($navitem->description)?></a>
                        <?php
                    }                    
                   
                ?>   
            </div><!--  .highlight-content-inside  -->
        </div><!--  .highlight-content  -->
    </li><!--  .highlight  -->
<?php endforeach; ?>
</ul>