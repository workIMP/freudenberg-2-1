<div class="online_reservation">
    <div class="b_room">
        <div class="booking_room">
            <div class="reservation">
                
                    <div class="booking-heading">MAKE A RESERVATION&nbsp;<a id="booking_widget_open_close" class=""><i id="close" class="fa fa-times"></i></a></div>
                    <div class="toggler">
                        <div id="effect" class="ui-widget-content ui-corner-all">
                            <form id="central_booking" action="<?php echo MAIN_URL ?>booking_process.php" method="POST">
							<ul>
                                <li class="span1_of_1">
                                    <!-- start section_room -->
                                    <div class="section_room">
                                        <select id="hotels" name="hotels" class="frm-field required">
                                            <option value="-1">Select a Hotel</option>
                                            <option value="randholee">Randholee Resort &#38; Spa</option>
                                            <option value="firs">The Firs</option>
                                            <option value="ellens">Ellen's Place</option>
                                        </select>
                                    </div>
                                </li>

                                <li  class="span1_of_1 left">
                                    <div class="book_date">
                                        <!--
                                        <input class="date" id="arrival_date" name="arrival_date" type="text" value="CHECK IN" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                    this.value = '<?php echo date("Y-m-d") ?>';
                                                }">-->
                                        <input class="date" id="arrival_date" name="eZ_chkin" type="text" value="" placeholder="CHECK IN">
                                    </div>
                                </li>
                                <li  class="span1_of_1 right">
                                    <div class="book_date">
                                        <!--
                                        <input class="date" id="departure_date" name="departure_date" type="text" value="CHECK OUT" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                    this.value = '<?php echo date("Y-m-d", strtotime("tomorrow")) ?>';
                                                }">-->
                                        <input class="date" id="departure_date" name="eZ_chkout" type="text" value="" placeholder="CHECK OUT">
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <input type="hidden" name="site" value="fsaleisure" />
                                        <input type="submit" name="submit_fsaleisure" value="CHECK AVAILABILITY" style="width: 98%; padding-left: 13px;"/>
                                    </div>
                                </li>
                                <!--<div class="clearfix"></div>-->
							</ul>
							</form>
                        </div>
                    </div>
                

            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>