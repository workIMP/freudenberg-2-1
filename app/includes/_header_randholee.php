<?php require_once '../../top.php'; ?>
<!DOCTYPE html> 
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta content='width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0' name='viewport'>
        <base href="<?=RANDHOLEE_DOMAIN?>"/>
        <link rel="shortcut icon" href="assets/img/favicon.ico">

        <link rel="stylesheet" type="text/css" href="assets/fonts/fonts.css">
        <link type="text/css" rel="stylesheet" href="assets/css/styles.css">
        <link type="text/css" rel="stylesheet" href="assets/css/randholle_main.css">
        <link type="text/css" rel="stylesheet" href="assets/css/responsive.css">
        <link type="text/css" rel="stylesheet" href="assets/css/accordian.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/jquery-ui.css" />

        <style type="text/css">
            .add_rand{
                margin-left: 363px;
                left: 0 !important;
            }
        </style>

        <meta name="google-site-verification" content="dLvri9wLRRHzdRH7Q390kxKzz_qm_TXW_xurNJoWtKk" />

        <!-- Google Tag Manager -->
        <noscript>
            <iframe src="//www.googletagmanager.com/ns.html?id=GTM-TXLJBQ"
                          height="0" width="0" style="display:none;visibility:hidden">
                              
            </iframe>
        </noscript>
        
        <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start':
                        new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                    '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TXLJBQ');</script>
        <!-- End Google Tag Manager -->

        <!--Randholee Resort Google Analytics Code-->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-33749419-37', 'auto');
            ga('send', 'pageview');
        </script>
        <!--Randholee Resort Google Analytics Code-->
    </head>

