
<style>
    .toggler {
    }
    #button {
        text-decoration: none;
    }
    #effect {
        position: relative;
        height: 170px;
    }
    #effect h3 {
        margin: 0;
        text-align: center;
    }
</style>
<div class="online_reservation">
    <div class="b_room">
        <div class="booking_room">
            <div class="reservation">
                <h5>MAKE A RESERVATION &nbsp;<a id="booking_widget_open_close" class=""><i id="close" class="fa fa-times"></i></a></h5> 
                <div class="toggler">
                    <div id="effect" class="ui-widget-content ui-corner-all">
						<form name="_resBBBox" action="https://live.ipms247.com/booking/book-rooms-197353" method="POST" target="_self">
							<ul> 
                                <li  class="span1_of_1 left">
                                    <div class="book_date">
                                        <!--
                                        <input class="date" id="arrival_date" name="arrival_date" type="text" value="CHECK IN" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                    this.value = '<?php echo date("Y-m-d") ?>';
                                                }">-->
                                        <input class="date" id="arrival_date" name="eZ_chkin" type="text" value="" placeholder="CHECK IN">
                                    </div>
                                </li>
                                <li  class="span1_of_1 right">
                                    <div class="book_date">
                                        <!--
                                        <input class="date" id="departure_date" name="departure_date" type="text" value="CHECK OUT" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                    this.value = '<?php echo date("Y-m-d", strtotime("tomorrow")) ?>';
                                                }">-->
                                        <input class="date" id="departure_date" name="eZ_chkout" type="text" value="" placeholder="CHECK OUT">
                                    </div>
                                </li>
                                <li class="booking-submit-button">
                                    <div>
                                        <!--
                                        <input type="hidden" name="site" value="fsaleisure" />
                                         <input type="hidden" name="hotels" value="randholee" />
                                        <input type="submit" name="submit_fsaleisure" value="CHECK AVAILABILITY" />
                                        -->
                                        <input name="eZ_Nights" value="2" type="hidden">
                                        <input name="eZ_adult" value="1" type="hidden">
                                        <input name="eZ_child" value="0" type="hidden">
                                        <input name="eZ_room" value="1" type="hidden">
                                        <input name="hidBodyLanguage" value="en" type="hidden">
                                        <input name="calformat" value="yy-mm-dd" type="hidden">
                                        <input name="ArDt" value="<?=date('Y-m-d')?>" type="hidden">
                                        <input name="acturl" value="https://live.ipms247.com/booking/book-rooms-197353" type="hidden">
                                        <input type="submit" name="bb_resBtn" value="BOOK NOW" />                                                                                
                                    </div>
                                </li>
                                <!--<div class="clearfix"></div>-->
							</ul>
							</form>
                        
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
