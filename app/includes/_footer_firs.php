<div id="footer-info">

    <div class="wrapper">
        <nav class="nav-footer" role="navigation">  
            <h1 class="hide-visual">Main Website Sections</h1>
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
                <li><a href="#">Pay Now</a></li>
                <li><a href="downloads.php">Downloads</a></li>
                <li><a href="terms-and-conditions.php">Terms and Conditions</a></li>
                <li><a href="privacy-policy.php">Privacy Policy</a></li>   
                <li><a href="site-map.php">Site Map</a></li>                                          
            </ul>
        </nav>
        <aside role="complementary">
            <h1 class="hide-visual">The Firs Social Media Channels</h1>
            <ul class="footer-social-media">  
                <li><a target="_blank" href="https://www.facebook.com/Freudenberg.Leisure/?ref=bookmarks" title="Freudenberg Leisure Facebook" class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a target="_blank" href="https://plus.google.com/u/0/108199145368696310148/photos" title="Freudenberg Leisure G+" class="gplus"><i class="fa fa-google-plus"></i></a></li>
                <li><a target="_blank" href="https://www.youtube.com/channel/UCESdt6RC1D0m8EhcBaLvcKQ" title="Freudenberg Leisure Youtube" class="youtube"><i class="fa fa-youtube"></i></a></li>
                <li><a target="_blank" href="https://www.flickr.com/photos/136744566@N04/" title="Freudenberg Leisure Flickr" class="flickr"><i class="fa fa-flickr"></i></a></li>
            </ul>
        </aside>      
        <small class="copyright">Copyright &nbsp;&copy; Freudenberg Leisure <?php echo date('Y'); ?><br /><a href="https://www.3cs.lk/" target="_blank" style="color: #09F;cursor:pointer;">Web Design by 3CS</a></small>
    </div><!--  .wrapper  -->
</div><!--  #footer-info  -->
</footer><!--  #footer  -->


<script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-ui.js"></script>

<script type="text/javascript" src="assets/js/misc/drupal.js?nsbnfj"></script>

<script type="text/javascript" src="assets/js/modernizr.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>

<script type="text/javascript" src="assets/js/lazyloadxt.min.js"></script>
<script type="text/javascript" src="assets/js/lazyloadxt.bg.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.nivo.slider.js"></script>
<script type="text/javascript" src="assets/js/scripts.js"></script>

<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.bootstrap.newsbox.min.js"></script>

<script type="text/javascript" src="assets/js/responsive_navigation_firs.js"></script>
<script type="text/javascript" src="assets/js/cssmin.js"></script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#arrival_date, #departure_date").datepicker({
            dateFormat: "yy/mm/dd",
            beforeShow: function (input) {
                if (input.id == 'arrival_date') {
                    var minDate = new Date('<?php echo date("Y/m/d") ?>');
                    return {
                        minDate: minDate
                    };
                } else if (input.id == 'departure_date') {
                    if ($('#arrival_date').val().length > 0) {
                        var minDate = new Date($('#arrival_date').val());
                        minDate = new Date(minDate.getTime() + (24 * 60 * 60 * 1000));
                    } else {
                        var minDate = new Date('<?php echo date("Y/m/d", strtotime("tomorrow")) ?>');
                    }
                    minDate.setDate(minDate.getDate());
                    return {
                        minDate: minDate
                    };
                }
            },
            onSelect: function (current_date, input) {
                if (input.id == "arrival_date")
                {
                    var departure_date = new Date($('#departure_date').datepicker('getDate'));
                    var arrival_date = new Date($('#arrival_date').datepicker('getDate'));
                    var new_departure_date = new Date(arrival_date.getTime() + (24 * 60 * 60 * 1000));

                    if (arrival_date.getTime() >= departure_date.getTime())
                    {
                        $("#departure_date").datepicker("setDate", new_departure_date);
                    }
                }
            }
        });

        /* Trip Advisor Reviews*/

        $("#nt-example1").bootstrapNews({
            newsPerPage: 1,
            autoplay: true,
            pauseOnHover: true,
            navigation: false,
            direction: 'up',
            newsTickerInterval: 3000,
            onToDo: function () {
                //console.log(this);
            }
        });

        /* Inner Slider */
        $('#slider').nivoSlider();

        /* Booking Widget Open Close Function*/

        // run the currently selected effect
        function runEffect() {
            // get effect type from
            var selectedEffect = "blind";
            // Most effect types need no options passed by default
            var options = {};
            // some effects have required parameters
            if (selectedEffect === "scale") {
                options = {percent: 50};
            } else if (selectedEffect === "size") {
                options = {to: {width: 200, height: 60}};
            }
            // Run the effect
            $("#effect").toggle(selectedEffect, options, 500);
        }
        ;
        function close_icon() {
            if ($('#close').hasClass('fa-times')) {
                $("#close").removeClass("fa-times");
                $("#close").addClass("fa-arrows-alt");
            } else {
                $("#close").removeClass("fa-arrows-alt");
                $("#close").addClass("fa-times");
            }
        }
        // Set effect from select menu value
        $("#booking_widget_open_close").on("click", function () {
            runEffect();
            close_icon();
        });
        /* Booking Widget Open Close Function*/

    });
</script>