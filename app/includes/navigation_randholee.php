<?php $token = NoCSRF::generate('token'); ?>
<nav id="nav-primary-wrapper" role="navigation">
    <a class="skiptomain" href="#main">Skip to Main Content</a>
    <div class="logo"><a href="" class="logo_bg">Randholee Luxury Resort - Home</a></div>
    <div class="mobile-toggle">MENU<span></span></div>    
    <ul class="nav-primary">



        <div class="accordion">
            <dl>
                <dt><a href="" aria-controls="accordion1" class="accordion-title accordionTitle">Home</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">&nbsp;</dd>

                <dt>
                    <a href="accommodation" aria-expanded="false" aria-controls="accordion2" class="accordion-title accordionTitle js-accordionTrigger">Accommodation</a>
                </dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion2" aria-hidden="true">
                <li><a href="accommodation/super-deluxe">Super Deluxe</a></li>
                <li><a href="accommodation/deluxe-mountain-view">Deluxe Mountain View</a></li>
                <li><a href="accommodation/standard">Standard</a></li>
                </dd>

                <dt><a href="cuisine.php" aria-controls="accordion3" class="accordion-title accordionTitle js-accordionTrigger">Cuisine</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion3" aria-hidden="true">
                <li><a href="cuisine/buffet">Buffet</a></li>
                <li><a href="cuisine/bar">Bar</a></li>
                <li><a href="cuisine/signature-dining">Signature Dining</a></li>
                </dd>

                <dt><a href="promotions.php" aria-controls="accordion4" class="accordion-title accordionTitle">Promotions</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion4" aria-hidden="true">&nbsp;</dd>

                <dt><a href="facilities" aria-controls="accordion5" class="accordion-title accordionTitle js-accordionTrigger">Facilities</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion5" aria-hidden="true">
                <li><a href="facilities/squash-court">Squash Court</a></li>
                <li><a href="facilities/badminton-n-table-tennis">Badminton And Table Tennis</a></li>
                <li><a href="facilities/shopping-boutique">Shopping Boutique</a></li>
                <li><a href="facilities/spa">Spa</a></li>
                <li><a href="facilities/infinity-pool">Infinity Pool</a></li>
                <li><a href="facilities/conferences-and-workshops">Conferences and Workshops</a></li>
                <li><a href="facilities/fitness-centre">Fitness Centre</a></li>
                <li><a href="facilities/business-centre">Business Centre</a></li>          
                </dd>

                <dt><a href="honeymoon.php" aria-controls="accordion6" class="accordion-title accordionTitle">Honeymoon</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion6" aria-hidden="true">&nbsp;</dd>

                <dt><a href="about-us" aria-controls="accordion6" class="accordion-title accordionTitle">About Us</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion6" aria-hidden="true">&nbsp;</dd>

                <dt><a href="contact-us" aria-controls="accordion7" class="accordion-title accordionTitle">Contact Us</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion7" aria-hidden="true">&nbsp;</dd> 

                <div class="search">
                    <form method="POST" action="search-results.php">
                        <input name="search" type="text" size="15" class="input" required>
                        <input type="hidden" name="token" value="<?php echo $token ?>">
                        <input type="submit" class="submit" value="SEARCH" style="background:none; border: none;">
                        <div class="clear"></div>
                    </form>
                </div>         
            </dl>
        </div>



        <li class="prime-main-link active-here ddd"><a href="">Home</a></li>    
        <li class="prime-main-link active-act ddd"><a href="accommodation">Accommodation</a>
            <ul>
                <li><a href="accommodation/super-deluxe">Super Deluxe</a></li>
                <li><a href="accommodation/delux-mountain-view">Deluxe Mountain View</a></li>
                <li><a href="accommodation/standard">Standard</a></li>
            </ul>
        </li>
        <li class="prime-main-link active-accom ddd"><a href="cuisine.php">Cuisine</a>
            <ul>
                <li><a href="cuisine/buffet">Buffet</a></li>
                <li><a href="cuisine/bar">Bar</a></li>
                <li><a href="cuisine/signature-dinning">Signature Dining</a></li>
            </ul>
        </li>
        <li class="prime-main-link active-accom ddd"><a href="promotions.php">Promotions</a></li>
        <li class="nav-logo-center"><a href="">Randholee Luxury Resort - Home</a></li>
        <li class="prime-main-link active-resort ddd"><a href="facilities">Facilities</a>
            <ul>
                <li><a href="facilities/squash-court">Squash Court</a></li>
                <li><a href="facilities/badminton-n-table-tennis">Badminton And Table Tennis</a></li>
                <li><a href="facilities/shopping-boutique">Shopping Boutique</a></li>
                <li><a href="facilities/spa">Spa</a></li>
                <li><a href="facilities/infinity-pool">Infinity Pool</a></li>
                <li><a href="facilities/conferences-and-workshops">Conferences and Workshops</a></li>
                <li><a href="facilities/fitness-centre">Fitness Centre</a></li>
                <li><a href="facilities/business-centre">Business Centre</a></li>

            </ul>
        </li>    
        <li class="prime-main-link active-here ddd"><a href="honeymoon.php">Honeymoon</a></li>
        <li class="prime-main-link active-here ddd"><a href="about-us">About Us</a></li>
        <li class="prime-main-link active-here ddd"><a href="contact-us">Contact Us</a></li>
        <li class="prime-quick-link quick-link-contact nav-cont-no ddd">E-mail: reservations@randholeeresorts.com &nbsp;&nbsp;&nbsp; Tel: + 94 81 2217741 – 3</li>

    </ul>
    <ul class="prime-quick-link quick_nav">

        <li class="prime-quick-link quick-link-contact ddd">
            <div class="search">
                <form method="POST" action="search-results.php">
                    <input name="search" type="text" size="15" class="input" required>
                    <input type="hidden" name="token" value="<?php echo $token ?>">
                    <input type="submit" class="submit" value="SEARCH" style="background:none; border: none;">
                    <div class="clear"></div>
                </form>
            </div>
        </li>

        <li class="prime-quick-link quick-link-contact logo_main"><a href="<?php echo MAIN_URL ?>"><img class="nav-img" src="<?php echo HTTP_PATH ?>assets/img/logo_main.png" /></a></li>

        <div class="prime-quick-link quick-link-contact lanuge ddd">
            <div id="google_translate_element"></div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL, autoDisplay: false}, 'google_translate_element');
                }
            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>       
    </ul>
</nav>
