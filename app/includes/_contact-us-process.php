<?php
include_once DOC_ROOT.'../config/country_list.php';
use Mailgun\Mailgun;

$name = "";
$subject = "";
$email = "";
$comments = "";
$phone = "";
$country = "";
$errors = "";

if(isset($_POST) && !empty($_POST['contact_submit']) && $_POST['contact_submit'] === 'Submit'){

    //CSRF verification
    try{
        if(!NoCSRF::check('form_token', $_POST)){
            header('HTTP/1.0 403 Forbidden');
            header('Location:'.HTTP_PATH);
            exit;
        }
    }
    catch(exception $e){
        header('HTTP/1.0 403 Forbidden');
        die($e->getMessage());
    }

    $gump = new GUMP();

    //Added a news validation rule to validate recaptcha response
    $gump->add_validator("recaptcha", function($field, $input, $param = NULL) {
        $recaptcha = new \ReCaptcha\ReCaptcha(RECAPTCHA_SECRET);
        $resp = $recaptcha->verify($input[$field], $_SERVER['REMOTE_ADDR']);
        return $resp->isSuccess();
    });

    //Added a news validation rule to validate recaptcha response
    $gump->add_validator("mg_email", function($field, $input, $param = NULL) {
        $mgClient = new Mailgun(MG_PUB_KEY);
        $result = $mgClient->get("address/validate", array('address' => $input[$field]));
        return $result->http_response_body->is_valid;
    });

    $gump->validation_rules(array(
        'name' => 'required|valid_name|max_len,100|min_len,5',
        'subject' => 'required|max_len,100|min_len,6',
        'email' => 'required|valid_email|mg_email',
        'comments' => 'required|max_len,500|min_len,5',
        'phone' => 'required|phone_number|max_len,15|min_len,7',
        'country' => 'required|alpha_space|max_len,100|min_len,3',
        'g-recaptcha-response' => 'recaptcha',
        'hotel' => 'required|alpha|max_len,15|min_len,4'
    ));

    $gump->filter_rules(array(
        'name' => 'trim|sanitize_string',
        'subject' => 'trim|sanitize_string',
        'email' => 'trim|sanitize_email',
        'comments' => 'trim|sanitize_string',
        'phone' => 'trim|sanitize_string',
        'country' => 'trim|sanitize_string',
        'hotel' => 'trim|sanitize_string'
    ));

    $gump->set_field_name('name', 'Name');
    $gump->set_field_name('subject' , 'Subject');
    $gump->set_field_name('email' , 'Email Address');
    $gump->set_field_name('comments' , 'Message');
    $gump->set_field_name('phone' , 'Contact Number');
    $gump->set_field_name('country' , 'Country');
    $gump->set_field_name('g-recaptcha-response' , 'Recaptcha');

    $_POST = $gump->sanitize($_POST);

    $validated_data = $gump->run($_POST);

    $name = $_POST['name'];
    $subject = $_POST['subject'];
    $email = $_POST['email'];
    $comments = $_POST['comments'];
    $phone = $_POST['phone'];
    $user_country = $_POST['country'];

    if($validated_data === false)
    {
        $hasError = true;
        $errors = $gump->get_readable_errors(false);
    } 
    else
    {

        $data = array(
            'date_string'=> date('Y-m-d h:i:s'),
            'main_logo' => 'cid:fsaleisure_site.png',
            'name' => $validated_data['name'],
            'email' => $validated_data['email'],
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'contact_no' => $validated_data['phone'],
            'country' => $validated_data['country'],
            'comments' => $validated_data['comments']
        );

        if($validated_data['hotel'] == 'ellens'){
            $data['hotel_logo'] = 'cid:email_ellens_header.png';
            $data['header_image'] = 'cid:email_fsaleisure_ellens_logo.png';
            $data['footer_image'] = 'cid:email_ellens_footer.png';
        }

        switch ($validated_data['hotel']) {
            case 'ellens':
                $data['hotel_logo'] = 'cid:email_ellens_header.png';
                $data['header_image'] = 'cid:email_fsaleisure_ellens_logo.png';
                $data['footer_image'] = 'cid:email_ellens_footer.png';

                $mail_from = 'webmaster.ellensplace@freudenbergleisure.lk';
                $recipients = $contact_mail_ellens;

                $template_images = array(
                    DOC_ROOT.'assets/img/email_ellens_header.png',
                    DOC_ROOT.'assets/img/email_fsaleisure_ellens_logo.png',
                    DOC_ROOT.'assets/img/email_ellens_footer.png',
                    DOC_ROOT.'assets/img/fsaleisure_site.png'
                );
                break;
            case 'firs':
                $data['hotel_logo'] = 'cid:email_firs_header.jpg';
                $data['header_image'] = 'cid:email_fsaleisure_firs_logo.png';
                $data['footer_image'] = 'cid:email_firs_footer.jpg';
                
                $mail_from = 'webmaster.firs@freudenbergleisure.lk';
                $recipients = $contact_mail_firs;
                $template_images = array(
                    DOC_ROOT.'assets/img/email_firs_header.jpg',
                    DOC_ROOT.'assets/img/email_fsaleisure_firs_logo.png',
                    DOC_ROOT.'assets/img/email_firs_footer.jpg',
                    DOC_ROOT.'assets/img/fsaleisure_site.png'
                );
                break;
            case 'randholee':
                $data['hotel_logo'] = 'cid:email_randholee_header.jpg';
                $data['header_image'] = 'cid:email_fsaleisure_randholee_logo.png';
                $data['footer_image'] = 'cid:email_randholee_footer.jpg';
                
                $mail_from = 'webmaster.randholee@freudenbergleisure.lk';
                $recipients = $contact_mail_randholee;
                $template_images = array(
                    DOC_ROOT.'assets/img/email_randholee_header.jpg',
                    DOC_ROOT.'assets/img/email_fsaleisure_randholee_logo.png',
                    DOC_ROOT.'assets/img/email_randholee_footer.jpg',
                    DOC_ROOT.'assets/img/fsaleisure_site.png'
                );
                break;
            case 'freudenberg':
                $data['hotel_logo'] = 'cid:email_fsaleisure_header.jpg';
                $data['header_image'] = 'cid:email_fsaleisure_logo.png';
                $data['footer_image'] = 'cid:email_fsaleisure_footer.jpg';
                
                $mail_from = 'webmaster@freudenbergleisure.lk';
                $recipients = $contact_mail_main;
                $template_images = array(
                    DOC_ROOT.'assets/img/email_fsaleisure_header.jpg',
                    DOC_ROOT.'assets/img/email_fsaleisure_logo.png',
                    DOC_ROOT.'assets/img/email_fsaleisure_footer.jpg',
                    DOC_ROOT.'assets/img/fsaleisure_site.png'
                );
                break;
            
            default:
                die('Invalid Request source');
                break;
        }

        $mail_template = file_get_contents(DOC_ROOT.'templates/email_template.html');
        $parsed_template = Generic::parseTemplate($mail_template, $data);

        $mgClient = new Mailgun(MG_PRV_KEY);

        $result = $mgClient->sendMessage(MG_DOMAIN, array(
            'from'    => $mail_from,
            'to'      => $recipients,
            'bcc'      => DEV_EMAIL,
            'subject' => $validated_data['subject'],
            'html'    => $parsed_template
        ),array(
            'inline' => $template_images
        ));

        
        $settings = [
            'username' => $mail_from,
            'link_names' => true
        ];
        //Send a notification to slack
        $client = new Maknz\Slack\Client(WEB_HOOK_SLACK, $settings);

        $slack_template = file_get_contents(DOC_ROOT.'templates/slack-alert.txt');
        $slack_template = Generic::parseTemplate($slack_template, $data);

        $client->to(SLACK_CHANNEL)->attach([
            'fallback' => 'test1',
            'text' => $slack_template,
            'color' => '#36a64f',
        ])->send('Contat us form submission');

        if($result->http_response_code == 200)
        {

            $success_msg = 'Your message has been successfully sent. We will contact you very soon!';

            $name = '';
            $subject = '';
            $email = '';
            $comments = '';
            $phone = '';
            $user_country = '';

        }
        else
        {
            $hasError = true;
            $errors = "Something went wrong! Please contact administrator.";
        }
    }
}

//Generate CSRF token
$csrf_token = NoCSRF::generate('form_token');

?>