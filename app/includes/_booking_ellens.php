<style>
    .toggler {
    }
    #button {
        text-decoration: none;
    }
    #effect {
        position: relative;
        height: 170px;
    }
    #effect h3 {
        margin: 0;
        text-align: center;
    }
</style>
<div class="online_reservation">
    <div class="b_room">
        <div class="booking_room">
            <div class="reservation">
                <h5>MAKE A RESERVATION 	<a id="booking_widget_open_close" class=""><i id="close" class="fa fa-times"></i></a></h5>
                <div class="toggler">
                    <div id="effect" class="ui-widget-content ui-corner-all">
                        <form id="booking-widget-form" action="http://www.freudenbergleisure.lk/booking_process.php" method="POST">
							<ul> 
                                <li  class="span1_of_1 left">
                                    <div class="book_date">
                                        <input class="date" id="arrival_date" name="arrival_date" type="text" value="CHECK IN" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                    this.value = '<?php echo date("Y-m-d") ?>';
                                                }">
                                    </div>
                                </li>
                                <li  class="span1_of_1 right">
                                    <div class="book_date">
                                        <input class="date" id="departure_date" name="departure_date" type="text" value="CHECK OUT" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                    this.value = '<?php echo date("Y-m-d", strtotime("tomorrow")) ?>';
                                                }">
                                    </div>
                                </li>
                                <li class="booking-submit-button">
                                    <div>
                                        <input type="hidden" name="hotels" value="ellens" />
                                        <input type="hidden" name="site" value="fsaleisure" />
                                        <input type="submit" name="submit_fsaleisure" value="CHECK AVAILABILITY" />
                                    </div>
                                </li>
                                <!--<div class="clearfix"></div>-->
							</ul>
							</form>
						
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
