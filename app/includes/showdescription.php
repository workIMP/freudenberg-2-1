<?php 
$style = '';			// initialize to avoid NOTICES
$pageDesc = $cms->getDescription(); 

	if($pg['property'] == 'randholee' && $pg['page'] == 'home')
	{
		$style = 'margin-bottom: 20px;';
	}
?>
<?php if(!empty($pageDesc->sub_title)): ?>
<h1 class="hdr-seven" style="<?=$style?>"><?=$pageDesc->sub_title?></h1>
<?php endif; ?>
<div class="hdr-two"><?=$pageDesc->main_title?></div> 
<?php
$contents = explode('<!-- section separator -->', $pageDesc->body_text);
if(count($contents) == 2):
?>
<?php
$galleryincluded = true;
list($firstpara, $secondpara) = $contents;
?> 
<?=$firstpara?>
<div style="clear:both;"></div>
<?php include 'inner_page_gallery.php'; ?>
<div style="clear:both;"></div>
<?=$secondpara?>
<?php else: ?>   
<?=$pageDesc->body_text?>
<?php endif; ?>