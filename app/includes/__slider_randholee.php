<?php 
$sliderFlag = $cms->getSliderFlag(); 
$slides = ($sliderFlag == 1 ? $cms->getSlider(42) : $cms->getSlider());
?>
<div id="slidepanel" class="single-demo owl-carousel owl-theme">
<?php foreach($slides as $slide): ?>
	<div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('<?=PARENT_DOMAIN?>images/slider/<?=$slide->page_id?>/<?=$slide->image_name?>') no-repeat 50% 50%; background-size: cover;"></div>
<?php endforeach; ?>
</div>
<a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>