﻿<?php 
$page = 'downloads';
include 'includes/header.php'; 
?>
    <style>
        .hdr-seven{text-align:left;color: #484848;}
        .ctatext-wrapper{padding-top:0px !important;}
    </style>

    <body class="node-type-accommodation-list">
        <header id="header" role="banner"> 
            <?php include 'includes/navigation.php'; ?> 
        </header><!--  #header  -->

        <?php include 'includes/booking.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include 'includes/_slider.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Downloads</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text pad_top">         
                                <div class="hdr-two">Downloads</div>   
                                <div style="padding: 30px; padding-top: 45px"><p style="text-align:left; font-size:16px;">For further information about Freudenberg Leisure, you are welcome to download our brochure and fact sheet. Simply enter your email address to make your request.</p>

                                    <div class="downloads down1">        
                                        <h1 class="hdr-seven"><strong>Randholee Luxury Resorts</strong></h1>
                                        <form action="" method="POST" style="text-align:left; font-size:16px;">
                                            <input type="checkbox" name="Factsheet" value="yes" style="visibility: visible;"/>&nbsp;Fact Sheet<br/>
                                            <input type="checkbox" name="Factsheet" value="yes" style="visibility: visible;"/>&nbsp;Brochure<br/><br/>
                                            <div style="float:left;">Email : <input type="text" name="email" value="" style="width: 243px; box-shadow: 0px 0px 2px #333 inset; line-height: 30px; padding-right: 10px; padding-left: 10px; height: 30px; border: medium none;"/></div><span style="float: left; margin-left: 10px;"><input type="submit" value="Request" style="border-radius:0px;"/></span><br />
                                            <input type="hidden" name="submitted" value="true"/>

                                        </form>
                                    </div>

                                    <div class="downloads down2">        
                                        <h1 class="hdr-seven"><strong>The Firs</strong></h1>
                                        <form action="" method="POST" style="text-align:left; font-size:16px;">
                                            <input type="checkbox" name="Factsheet" value="yes" style="visibility: visible;"/>&nbsp;Fact Sheet<br/>
                                            <input type="checkbox" name="Factsheet" value="yes" style="visibility: visible;"/>&nbsp;Brochure<br/><br/>
                                            <div style="float:left;">Email : <input type="text" name="email" value="" style="width: 243px; box-shadow: 0px 0px 2px #333 inset; line-height: 30px; padding-right: 10px; padding-left: 10px; height: 30px; border: medium none;"/></div><span style="float: left; margin-left: 10px;"><input type="submit" value="Request" style="border-radius:0px;"/></span><br />
                                            <input type="hidden" name="submitted" value="true"/>

                                        </form>
                                    </div>

                                    <div class="downloads down3">        
                                        <h1 class="hdr-seven"><strong>Ellen's Place</strong></h1>
                                        <form action="" method="POST" style="text-align:left; font-size:16px;">
                                            <input type="checkbox" name="Factsheet" value="yes" style="visibility: visible;"/>&nbsp;Fact Sheet<br/><br/>
                                            <!--<input type="checkbox" name="Factsheet" value="yes" style="visibility: visible;"/>&nbsp;Brochure<br/>-->
                                            <div style="float:left;">Email : <input type="text" name="email" value="" style="width: 243px; box-shadow: 0px 0px 2px #333 inset; line-height: 30px; padding-right: 10px; padding-left: 10px; height: 30px; border: medium none;"/></div><span style="float: left; margin-left: 10px;"><input type="submit" value="Request" style="border-radius:0px;"/></span><br />
                                            <input type="hidden" name="submitted" value="true"/>

                                        </form>
                                    </div>

                                    <br>

                                </div>
                        <!--			<p style="text-align:left; font-size:16px;">* Note : We will be retaining your email address for future followup.</p>
                                -->          </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->


            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include 'includes/footer.php'; ?> 
            </footer>    
    </body>
</html>
