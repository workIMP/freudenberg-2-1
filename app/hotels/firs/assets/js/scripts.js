// jQuery initialized
(function ($) { 
  
/*----------------------------------------------------------------
	S M A R T   R E S I Z E
---------------------------------------------------------------- */
//Smart Resize
  Drupal.behaviors.vwtheme_smart_resize = {
    attach: function (context) {
      
      (function($,sr){
      
        // debouncing function from John Hann
        // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
        var debounce = function (func, threshold, execAsap) {
            var timeout;
      
            return function debounced () {
                var obj = this, args = arguments;
                function delayed () {
                    if (!execAsap)
                        func.apply(obj, args);
                    timeout = null;
                };
      
                if (timeout)
                    clearTimeout(timeout);
                else if (execAsap)
                    func.apply(obj, args);
      
                timeout = setTimeout(delayed, threshold || 100);
            };
        }
        // smartresize 
        jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };
      
      })(jQuery,'smartresize');

    } 
  };
//End Smart Resize


// Drupal behaviours initialized
Drupal.behaviors.vwtheme = { attach:function (context) {
/*
 * Slide Panel
 */  
    function panelWindowHeight() {
      var winHeightTall = $(window).height() - 130;
      //var winHeightShort = $(window).height() - 250; //= too short on laptop //326 = two lines              
      
      //set initial div height
      $('.tallpanelitem.item').css({
          'height': winHeightTall,
      });
      

      //set initial div height
/*
      $('.shortpanelitem.item').css({
          'height': winHeightShort,
      });
*/
      
    }//panelWindowHeight
    panelWindowHeight();
    
    //Check for resize
    $(window).smartresize(function(){
    
      //Call your function again
      panelWindowHeight();
        
    });
    
/*
 * Mobile Menu Toggle Display
 */
    $(".mobile-toggle").click(function(e){
      e.preventDefault();
      var menu = $('.mobile-toggle');
      var ac = 'open';
      //var bl = 'is-blurring';
      if(menu.hasClass(ac)) {
        menu.removeClass(ac);
        $(".nav-primary").slideUp(300); 
        //$('.blur').removeClass(bl);               
      } else {
        menu.addClass(ac);
        $(".nav-primary").slideDown(300);
        //$('.blur').addClass(bl);
      }
      
      return false;
      
    });
    
    $(window).resize(function(){
      if($(window).width() >= 960) {
        $('.nav-primary').show();
      }
      if($(window).width() <= 959) {
        $('.nav-primary').hide();
        //$('.blur').removeClass(bl);        
      }
    });
    
    
/**
 * Primary Nav to alternate look
 */
    var stickyHeader = {
      cfg : {
        b : $('#header'),
        is_scrolling : true,
        is_resizing : true,
        scrollTop : $(window).scrollTop(),
        offset : $(window).height(),
      },
      init : function() {
        stickyHeader.loop();
      },
      stickIt : function() {
        //update position and offset
        stickyHeader.cfg.scrollTop = $(window).scrollTop();
  	    stickyHeader.cfg.offset = $(window).height();
        //logic
        var isSticky = false;
        if(stickyHeader.cfg.scrollTop > stickyHeader.cfg.offset) {
          isSticky = true;
        }
        //apply classes
        if(isSticky) { stickyHeader.cfg.b.addClass('is-sticky'); }
        else { stickyHeader.cfg.b.removeClass('is-sticky'); }
      },
      loop : function() {
        if(stickyHeader.cfg.is_scrolling) {
          $(window).scroll(function(){
              stickyHeader.stickIt();
              stickyHeader.cfg.is_scrolling = true;
          });
          stickyHeader.cfg.is_scrolling = false;
        }
        if(stickyHeader.cfg.is_resizing) {
          $(window).resize(function(){
              stickyHeader.stickIt();
              stickyHeader.cfg.is_resizing = true;
          });
          stickyHeader.cfg.is_resizing = false;
           //$(.main-navigation).css('display', 'none'); 
        }
        requestAnimationFrame(stickyHeader.loop);
      }
        
    }
    
    stickyHeader.init();


/**
 * On Scroll hide class to Header to change position to fixed and top: -200px;
 */
    var header = $("html");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            header.addClass("is-fixed");
        } else {
            header.removeClass("is-fixed");
        }
    });


/**
 * Stop Hover on Scroll - keep the Highlight Panels at bay on scroll
 */
  
    $(window).scroll(function() {
			// remove hovers on scroll
			$("html, body").addClass("disable-hover");
			clearTimeout($.data(this, 'scrollTimer'));
		    $.data(this, 'scrollTimer', setTimeout(function() {
		        // once scrolling has stopped
		        $("html, body").removeClass("disable-hover");
		    }, 250));
    });    

/**
 * Equal Height CTA
 */
 
  function equalHeight(group) {
    tallest = 0;
    group.each(function() {
        thisHeight = $(this).height();
        if(thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    group.height(tallest);
  }
  
  var win_width = $(window).width();
  if(win_width > 699) {
    equalHeight($(".ctacard-wrapper .equal-height"));
  }//win_width
    
//Check for resize -todo not working
/*
$(window).smartresize(function(){
  equalHeight($(".ctacard-wrapper .equal-height"));
});//.smartresize 
*/
 
 
 
 
/*
  var win_width = $(window).width();
  if(win_width > 1024) {
    equalHeight($(".ctacard-wrapper .equal-height"));
  }//win_width
*/
    
    
/**
 * Webform Treatment and Packages - form reveal
 */    
 
   $("#offerinquiry-btn").click(function(e){
      e.preventDefault();
      var btn = $('#offerinquiry-btn');
      var ac = 'reveal'
      if(btn.hasClass(ac)) {
        btn.removeClass(ac);
        $(".offerinquiry-form").slideUp(400);        
      } else {
        btn.addClass(ac);
        $(".offerinquiry-form").slideDown(400);
      }
      
      return false;
      
    });
    

/*----------------------------------------------------------------
	S C R O L L   D O W N
---------------------------------------------------------------- */
//Getting Here Map page. Scroll to Map.    
    $('.regional-arrow').click(function() {
      var targetOffset = $('#main').offset().top - 130;
      $('html,body').animate({scrollTop: targetOffset}, 1000);
      return false;
    });
    
/*----------------------------------------------------------------
	Slide Panel - villa detail
---------------------------------------------------------------- */
$(document).ready(function() { 
  $("#slidepanel").owlCarousel({
    navigation: true,
    slideSpeed: 600,
    lazyLoad: true,
    paginationSpeed: 400,
    navigationText: false,
    singleItem: true,
    transitionStyle: 'fade',
    autoPlay: 5000,
    stopOnHover: true,
  }); 
});

/**
 * Accessability
 */
 
  // bind a click event to the 'skip' link
  $(".skiptomain").click(function(event){

      // strip the leading hash and declare
      // the content we're skipping to
      var skipTo="#"+this.href.split('#')[1];

      // Setting 'tabindex' to -1 takes an element out of normal 
      // tab flow but allows it to be focused via javascript
      $(skipTo).attr('tabindex', -1).on('blur focusout', function () {

          // when focus leaves this element, 
          // remove the tabindex attribute
          $(this).removeAttr('tabindex');

      }).focus(); // focus on the content container
  });
 


/**
 * Getting Here - Regional Map & Lodge Facilities
 * Turn on flight paths and display facility items
 */
  $('.dot-toggle').unbind('click').bind('click', function(e) {
    e.preventDefault();
    var t = $(this);
    var targ = t.attr('href');
    t.toggleClass('active');
    $(targ).toggleClass('active');
    return false;
  });


/** 
 * Book Your Own Adventure - image checkboxes
 */
  
$('.bookadv-option-image').unbind('click').bind('click', function(e) {
  //define stuff
  var p = $(this).parents('.bookadv-section'),
      nid = $(this).attr('data-id');
  var cb = p.find('input[value="' + nid + '"]');
  //do stuff
  $(this).toggleClass('is-selected');
  if(cb.is(':checked')) {
    cb.removeAttr('checked');
  } else {
    cb.attr('checked', 'checked');
  }
});


/**
 * Book Adventure - Accommodation transition up/down 
 */  
 
$(".bookadv-section--accommodations .bookadv-header").click(function(e){
  e.preventDefault();
  var targ = $('.bookadv-section--accommodations');
  var ac = 'is-open'
  if(targ.hasClass(ac)) {
    targ.removeClass(ac);     
  } else {
    targ.addClass(ac);
  }
  
  return false;
}); 


/**
 * Book Adventure - Activities transition up/down 
 */  

$(".bookadv-section--activities .bookadv-header").click(function(e){
  e.preventDefault();
  var targ = $('.bookadv-section--activities');
  var ac = 'is-open'
  if(targ.hasClass(ac)) {
    targ.removeClass(ac);     
  } else {
    targ.addClass(ac);
  }
  
  return false;
});


/**
 * Book Adventure - Trip Planning transition up/down 
 */  

$(".bookadv-section--trip-planning .bookadv-header").click(function(e){
  e.preventDefault();
  var targ = $('.bookadv-section--trip-planning');
  var ac = 'is-open'
  if(targ.hasClass(ac)) {
    targ.removeClass(ac);     
  } else {
    targ.addClass(ac);
  }
  
  return false;
});
    

/**
 * Social Sharing 
 */

//pop up window
function windowPopup(url, width, height) {
  // Calculate the position of the popup so
  // it’s centered on the screen.
  var left = (screen.width / 2) - (width / 2),
      top = (screen.height / 2) - (height / 2);

  window.open(
    url,
    "",
    "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left
  );
}

//use if you want the dark overlay.
//need to find the css at top of uncommon
//need to find the markup in page.tpl.php
$(".js-social-share").on("click", function(e) {
  e.preventDefault();
    //var ol = 'is-overlaying',
        //bl = 'is-blurring';

    windowPopup($(this).attr("href"), 500, 300);
  //windowPopup($(this).attr("href"), 500, 300),
    //$('body').addClass(ol),
    //$('.blur').addClass(bl);
});


/*
$('a.share-overlay-close').on('click', function(e) {
  e.preventDefault();
    var ol = 'is-overlaying',
        bl = 'is-blurring';
        
    $('body').removeClass(ol),
    $('.blur').removeClass(bl);        
});
*/


/** 
 * Highlight Panel animation center background and text
 * Panel have all different ratios and heights
 */
 
//some default settings
var hilite = {
  
  cfg : {
    panel             : $('.highlight'),
    panel_bgs         : $('.highlight-background'),
    panel_content     : $('.highlight-content'),
    title_height      : 28,
    title_padding     : 12,
  },
  
  init : function() {
    
    hilite.cfg.panel.each(function(){
      var $this = $(this);
      var bg = $this.find('.highlight-background'),
          content = $this.find('.highlight-content'),
          inside_content = $this.find('.highlight-content-inside'),
          h = $this.outerHeight();
      
      var content_hover_padding = Math.max(((h - inside_content.outerHeight()) * 0.5), 0);
      bg.css({
        'height' : h,
        'top' :  (h - (hilite.cfg.title_height + (hilite.cfg.title_padding * 2)))
      });
      
      content.css({
        'height' : h,
        'top' :  (h - (hilite.cfg.title_height + (hilite.cfg.title_padding)))
      });
      
      $this.hover(
        function() {
          $this.addClass('active');
          bg.css({
            WebkitTransform: 'translateY(-' + (h - (hilite.cfg.title_height + (hilite.cfg.title_padding * 2))) + 'px)',
            transform: 'translateY(-' + (h - (hilite.cfg.title_height + (hilite.cfg.title_padding * 2))) + 'px)',
          });
          content.css({
            WebkitTransform: 'translateY(-' + (h - content_hover_padding - hilite.cfg.title_height - hilite.cfg.title_padding) + 'px)',            
            transform: 'translateY(-' + (h - content_hover_padding - hilite.cfg.title_height - hilite.cfg.title_padding) + 'px)',
          });
        },
        function() {
          $this.removeClass('active');
          bg.css({
            WebkitTransform: 'translateY(0px)',            
            transform: 'translateY(0px)',
          });
          content.css({
            WebkitTransform: 'translateY(0px)',            
            transform: 'translateY(0px)',
          });
        }
      )
    });  //hilite.cfg.panel
    
  }, //init : function 

}//var hilite

hilite.init();
    
//Check for resize
$(window).smartresize(function(){

  //Call your function again
  hilite.init();

});//.smartresize 
  


/*----------------------------------------------------------------
	R E S P O N S I V E   T A B L E   H E A D
---------------------------------------------------------------- */
  $('.responsive-table').each(function() {
      var thetable=($(this));
      $(this).find('tbody td').each(function() {
          $(this).attr('data-heading',thetable.find('thead th:nth-child('+($(this).index()+1)+')').text());
      });
  });



  //JS ABOVE
    
// Drupal behaviours closed    
} }; 

// jQuery closed
})(jQuery);
