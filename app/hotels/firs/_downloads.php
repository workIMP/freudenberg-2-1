﻿<!DOCTYPE html>
<html class="no-js">
    <?php
    include '../../includes/header_firs.php';
    include_once '../../classes/mailing.class.php';
    $_SESSION['email'] = "";
    $Broucher = "";
    $Factsheet = "";
    $err = "";
    $success = "";
    if (isset($_POST['submitted']) && $_POST['submitted'] == "true") {
        if (isset($_POST['Broucher'])) {
            $Broucher = $_POST['Broucher'];
        }
        if (isset($_POST['Factsheet'])) {
            $Factsheet = $_POST['Factsheet'];
        }
        $email = $_POST['email'];
        if ($Broucher == "" && $Factsheet == "") {
            $err = "<li>Please select a item to download</li>";
        }
        if ($email == "") {
            $err = $err . "<li>Please provide an email address to get the downloads</li>";
        } else {
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $err = $err . "<li>Email address is invalid</li>";
                $_SESSION['email'] = $email;
            }
        }
        if ($err != "") {
            $err = "Following errors occured during your request<br/><ul>" . $err . "</ul>";
        } else {
            $today = date("Y-m-d");
            $time = date("H:i:s");
            $subject = "Firs your requested download files";
            $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Freudenberg Leisure | Firs | Downloads</title>
    </head>
    <body>
        <div style="width: 100%; height: auto; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59); background-color: #fff;">
            <div style="margin-bottom: 1%; text-align: center;">
                <img src="http://www.freudenbergleisure.com/assets/img/email_fsaleisure_firs_logo.png" width=40% height=40% alt="Freudenberg Leisure - Firs"/>
            </div>
            <div style="text-align: center; background: url(http://www.freudenbergleisure.com/assets/img/email_firs_header.jpg) repeat; padding: 1%; font-weight:600; color: #FFF; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important; -webkit-box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                <h2 style="color: #FFF;">Firs :: Downloads requested on ' . $today . ' at ' . $time . '</h2>
            </div>
            <div style="margin-top:2%; margin-bottom: 2%; z-index: 2; position: relative; height: auto;">
                <table width="100%" cellspacing="0" border="0">
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="5" style="margin-left: 5%;">
                            <div style="font-family:Helvetica Light sans-serif; font-size:18px; text-align: justify; color:rgba(102,102,102,1); padding: 3% 4% 3% 4%; position: relative; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                                We have received your request for more information on firs. We have attached a brochure / fact sheet below for your reference. If you can\'t find your attachment, you can view it at the following links
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="7">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td width="20%">&nbsp;</td>
                        <td style="text-align: left;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; text-align: center; background:#909b71; padding: 2% 0%; font-size:20px; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                                <div style="padding: 5% 0%; border: double 4px #FFFFFF;">
                                    <a href="http://www.freudenbergleisure.com/assets/uploads/Factsheet_Firs.pdf" style="text-decoration: none; font-weight: bold; color: #FFFFFF;">FACT SHEET</a>
                                </div>
                            </div>
                        </td>
                        <td width="2%">&nbsp;</td>
                        <td style="text-align: left;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; text-align: center; background:#909b71; padding: 2% 0%; font-size:20px; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                                <div style="padding: 5% 0%; border: double 4px #FFFFFF;">
                                    <a href="http://www.freudenbergleisure.com/assets/uploads/firs_brochure.exe" style="text-decoration: none; font-weight: bold; color: #FFFFFF;">BROCHURE</a>
                                </div>
                            </div>
                        </td>
                        <td width="20%">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="7">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="5" style="margin-left: 5%;">
                            <div style="font-family:Helvetica Light sans-serif; font-size:18px; color:rgba(102,102,102,1); padding: 3% 2% 3% 6%; position: relative; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                                &nbsp;
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </div>
            <div style="color: #714A1B; background: url(http://www.freudenbergleisure.com/assets/img/email_firs_footer.jpg) repeat; padding: 20px 0 20px 0; border-top: 3px solid #909b71 ; text-align: center;  box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59);">
                <span>
                    <a href="http://www.freudenbergleisure.com/hotels/firs/" style="color: #fff; line-height: 32px; font-size: 20px; font-family: futura-pt , sans-serif;" target="_blank"><img src="http://www.freudenbergleisure.com/assets/img/fsaleisure_site.png" width="250"/></a>
                </span>
            </div>
        </div>
    </body>
</html>';

            $attachments = array();

            if ($Broucher == "yes") {
                $brou_path = DOC_ROOT . "assets/uploads/firs_brochure.exe";
                //array_push($attachments,$brou_path);
            }
            if ($Factsheet == "yes") {
                $fact_path = DOC_ROOT . "assets/uploads/Factsheet_Firs.pdf";
                array_push($attachments, $fact_path);
            }

            $mail_array = array('thilina@3cs.asia', $email);
//send mails to ******
            foreach ($mail_array as $to) {
                $is_mail_send = mailing::html_mail($to, $subject, $body, 'info@firs.lk', 'noreply@firs.lk', $attachments);
            }
            if (!$is_mail_send) {
                echo "<script>alert('Mailer Error: ')</script>";
            } else {
                $_SESSION['email'] = "";
                $err = "";
                $success = "Requested files were emailed to you. Thankyou<br/><br/><br/>";
                $Broucher = "";
                $Factsheet = "";
            }
        }
    }
    ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 

            <style>
                .ctatext-wrapper{padding-top:0px !important;}
            </style>
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include '../../includes/slider_firs.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Downloads</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Downloads</div>
                                <div class="content_left"><span style="color:red; list-style:none;"><?php
                                        if ($err != "") {
                                            echo $err;
                                        }
                                        ?></span>
                                    <div style="color:green;"><?php
                                        if ($success != "") {
                                            echo $success;
                                        }
                                        ?></div>
                                    <div style="padding: 30px; padding-top: 45px"><p style="text-align:left; font-size:16px;">For further information about The Firs, you are welcome to download our brochure and fact sheet.</p>
                                        <form action="" method="POST" style="text-align:left; font-size:16px;">
                                            <input type="checkbox" name="Factsheet" value="yes" style="visibility: visible;"/>&nbsp;Fact Sheet<br/>
                                            <input type="checkbox" name="Factsheet" value="yes" style="visibility: visible;"/>&nbsp;Brochure<br/><br/>
                                            <div style="float:left;">Email : <input type="text" name="email" value="" style="width: 243px; box-shadow: 0px 0px 2px #333 inset; line-height: 30px; padding-right: 10px; padding-left: 10px; height: 30px; border: medium none;"/></div><span style="float: left; margin-left: 10px;"><input type="submit" value="Request" style="border-radius:0px;"/></span><br />
                                            <input type="hidden" name="submitted" value="true"/>

                                        </form>
                                        <br>
                                        <p style="text-align:left; font-size:16px;">* Note : We will be retaining your email address for future followup.</p>
                                    </div>
                                </div>

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->


            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_firs.php'; ?> 
            </footer>    
    </body>
</html>
