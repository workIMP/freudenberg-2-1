﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_firs.php'; ?> 

    <body class="node-type-accommodation-list">

        <header id="header" role="banner">
            <h1 class="hide-visual">The Firs - The Firs Home Page</h1>  

            <?php include '../../includes/navigation_firs.php'; ?> 

        </header><!--  #header  -->
        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  
            <div class="node--page_basic mode--full">    
                <aside role="complementary">
                    <header>
                        <h1 class="hide-visual">The Firs Slideshow</h1>
                    </header>    
                    <?php include '../../includes/_slider_firs.php'; ?>
                </aside>  

                <aside role="complementary">
                    <header>
                        <h1 class="hide-visual">Randholee Luxury Resort Experiences</h1>
                    </header> 

                    <div id="main" role="main">
                        <div class="wrapper">
                            <article role="article">
                                <div class="ctatext-wrapper">
                                    <div class="ctatext-text">
                                        <h1 class="hdr-seven">The Firs</h1>
                                        <div class="hdr-two">Heritage Upscale Bungalow</div>     
                                        <p align="justify">
                                            The Firs Nuwara Eliya upscale bungalow in a little hillock overlooking Lake Gregory in a picturesque location offers holiday makers an exclusive holiday experience.
                                             This old bungalow built over one and half centuries ago has undergone a transformation and renovations to suite the modern era.
                                        </p>
                                        <p align="justify">
                                            It has now been transformed to a true Sri Lankan up-market ‘heritage bungalow’ and conceptualised according to the British Colonial era with the intention of providing the same experience that was available during the British monarchy.
                                            Rooms consist of two suites displaying old charm and elegance and three other beautifully furnished deluxe rooms. 
                                        </p>
                                        <p align="justify">
                                            The Firs offers many tours which cover the natural beauty of Nuwara Eliya including adventurous hikes to World’s End, and Sita Eliya (also known as ‘The Glade of Sita’), which is connected to the historical Ramayana story or a casual game of golf at the Famous Victoria Golf Club, St. Andrew’s Church and the spectacular Haggala botanical gardens.
                                        </p>
                                    </div><!--  .ctatext-text  -->          
                                </div><!--  .ctatext-wrapper  -->    
                            </article>
                        </div><!--  .wrapper  -->
                    </div><!--  #main  -->


                    <div class="experience-thumblist highlight-panels">
                        <ul>  
                            <div class="experience-thumblist highlight-panels">
                                <ul>          


                                    <li class="highlight" style="background: #ebebeb url('assets/images/adventure.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="accommodation.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Upscale Suites</h2>    
                                                <div class="hdr-two fadeitem"><em>Five Tastefully - Furnished and Upscale Suites</em></div>  
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 

                                    <li class="highlight" style="background: #ebebeb url('assets/images/experiences.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="location.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">
                                                <h2 class="hdr-four">Highlights</h2> 
                                                <div class="hdr-two fadeitem"><em>The most picturesque sights and historical attractions</em></div>  
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 

                                    <li class="highlight" style="background: #ebebeb url('assets/images/promo.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="promotions.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Promotions</h2>
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">See All Promotions</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 

                                    <li class="highlight" style="background: #ebebeb url('assets/images/retreats.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="assets/360/lobby-home.html" target="_blank"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">   
                                                <h2 class="hdr-four">360 Experience</h2> 
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">View</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 

                                    <li class="highlight" style="background: #ebebeb url('assets/images/getting.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="location.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">
                                                <h2 class="hdr-four">Getting Here</h2> 
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">View</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 
                                </ul>
                            </div><!--  .experience-thumblist .highlight-panels  --> 
                        </ul>
                    </div><!--  .experience-thumblist .highlight-panels  -->
                </aside>

            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <?php include 'trip-advisor.php'; ?> 

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">    

                <?php /* ?>    <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>


                <?php include '../../includes/footer_firs.php'; ?>

                </body>
                </html>