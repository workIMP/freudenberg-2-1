<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_ellens.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <h1 class="hide-visual">Ellen's Place | Facilities</h1>    
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?>

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">

                            <!--      <div id="route">
                                    <breadcrumb class="menu">
                                        <li><a href="index.php">Home</a></li>
                                        <li><span class="arrow"> &gt; </span>Privacy Policy</li>
                                    </breadcrumb>
                                  </div>-->

                            <div class="ctatext-text">         
                                <div class="hdr-two">Privacy Policy</div>          
                                <p style="text-align:justify; font-size:15px;">This Privacy Policy governs the manner in which Ellen's Place collects, uses, maintains and discloses information collected from users (each, a "User") of the website ("Site"). This privacy policy applies to the Site and all the services offered by Ellen's Place.
                                </p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Personal identification information</strong></h1>
                                <p style="text-align:justify; font-size:14px;">We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number, credit card information. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Non-personal identification information</strong></h1>
                                <p style="text-align:justify; font-size:14px;">We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Web browser cookies</strong></h1>
                                <p style="text-align:justify; font-size:14px;">Our Site may use "cookies" to enhance User experience. Users' browsers places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>How we use collected information</strong></h1>
                                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;">Ellen's Place may collect and use Users' personal information for the following purposes:</h1>
                                <ul class="priv-poly">
                                    <li>To improve customer service - Information you provide helps us respond to your customer service requests and support needs more efficiently. </li>
                                    <li>To improve our Site - We may use feedback you provide to improve our products and services. </li>
                                    <li>To run a promotion, contest, survey or other Site feature - To send Users information they agreed to receive about topics we think will be of interest to them. </li>
                                    <li>To send periodic emails - We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.</li>
                                </ul>
                                <div style="clear:both"></div>

                                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>How we protect your information</strong></h1>
                                <p style="text-align:justify; font-size:14px;">We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.<br><br>
                                    Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures.
                                </p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Sharing your personal information</strong></h1>
                                <p style="text-align:justify; font-size:14px;">We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Third party websites</strong></h1>
                                <p style="text-align:justify; font-size:14px;">Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website's own terms and policies.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Changes to this privacy policy</strong></h1>
                                <p style="text-align:justify; font-size:14px;">Ellen's Place has the discretion to update this privacy policy at any time. When we do, we will post a notification on the main page of our Site, revise the updated date at the bottom of this page. We encourage users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Your acceptance of these terms</strong></h1>
                                <p style="text-align:justify; font-size:14px;">By using this Site, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_ellens.php'; ?> 
            </footer>    
    </body>
</html>
