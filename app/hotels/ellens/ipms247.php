<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/ipms247_header_ellens.php'; ?> 

    <body class="node-type-accommodation-list">

        <header id="header" role="banner">
            <h1 class="hide-visual">Ellen's Place - Ellen's Place Home Page</h1>  

            <?php include '../../includes/navigation_ellens.php'; ?> 

        </header><!--  #header  -->
        <?php include '../../includes/ipms247_booking_ellens.php'; ?> 

        <div class="blur">  
            <div class="node--page_basic mode--full">    
                <aside role="complementary">
                    <header>
                        <h1 class="hide-visual">Ellen's Place Slideshow</h1>
                    </header>    
                    <?php include '../../includes/_slider_ellens.php'; ?>
                </aside> 

                <aside role="complementary">
                    <header>
                        <h1 class="hide-visual">The Firs Experiences</h1>
                    </header> 

                    <div id="main" role="main">

                        <div class="trip-advisor-logo" id="demo">
                            <img class="trip-ad-close" src="assets/img/cls.png" onclick="myFunction()"  >
                            <a href="https://www.tripadvisor.co.uk/Hotel_Review-g293962-d5501950-Reviews-Ellen_s_Place-Colombo_Western_Province.html" target="_blank"><img class="trip-ad" src="assets/images/trip-advisor-ellens.png"></a></div>

                        <div class="wrapper">
                            <article role="article">
                                <div class="ctatext-wrapper">
                                    <div class="ctatext-text">
                                        <h1 class="hdr-seven">Ellen's Place</h1>
                                        <div class="hdr-two">Upscale accommodation at Ellen's Place</div>     
                                        <p align="justify">
                                            Housed in a Colombo mansion, Ellen's Place Hotel combines classic and modern décor and it is elegantly designed and delicately furnished to create an atmosphere of grandeur and opulence. The hotel provides guests pleasant accommodation with a myriad of amenities In-room including  a mini-fridge, an LCD TV with satellite channels, a digital safe, as well as tea and coffee making facilities. and blissful comforts and ample living space. It offers a wide array of facilities, services and activities to ensure that guests of all ages are well looked after during their stay. The swimming pool built underneath fragrant Araliya (temple) trees at Ellen's Place is the ideal place for guests to relax and laze the hours away.
                                        </p>
                                    </div><!--  .ctatext-text  -->          
                                </div><!--  .ctatext-wrapper  -->

                            </article>
                        </div><!--  .wrapper  -->
                    </div><!--  #main  -->

                    <div class="experience-thumblist highlight-panels">
                        <ul>  
                            <div class="experience-thumblist highlight-panels">
                                <ul>          


                                    <li class="highlight" style="background: #ebebeb url('assets/images/retreats.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="#"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Golf Club</h2>      
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 

                                    <li class="highlight" style="background: #ebebeb url('assets/images/pool.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="swimming-pool.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Swimming Pool</h2>    
                                                <div class="hdr-two fadeitem"><em>To unwind and laze the hours away</em></div>      
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  -->


                                    <li class="highlight" style="background: #ebebeb url('assets/images/generations.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="promotions.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Promotions</h2>      
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">See All Promotions</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 


                                    <li class="highlight" style="background: #ebebeb url('assets/images/suites.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="suites.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Suites</h2>    
                                                <div class="hdr-two fadeitem"><em>Te grand and graceful style</em></div>      
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 


                                    <li class="highlight" style="background: #ebebeb url('assets/images/getting.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="contact-us.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Getting Here</h2>     
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">View</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 

                                </ul>
                            </div><!--  .experience-thumblist .highlight-panels  --> 
                        </ul>
                    </div><!--  .experience-thumblist .highlight-panels  -->
                </aside>  


            </div><!--  #node-details  -->
            <?php include 'trip-advisor.php'; ?>

            <footer id="footer" role="contentinfo">    

                <?php include '../../includes/ipms247_footer_ellens.php'; ?>



                <script type="text/javascript">
                    function myFunction() {
                        document.getElementById("demo").style.display = "none";
                    }
                </script>
                </body>
                </html>