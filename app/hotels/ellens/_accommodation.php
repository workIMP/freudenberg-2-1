﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_ellens.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_ellens.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/standard_slider1.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/suites_slider1.jpg') no-repeat 50% 50%; background-size: cover;"></div>      
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/delux_slider2.jpg') no-repeat 50% 50%; background-size: cover;"></div>      
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/acc_slider1.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/acc_slider2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>

                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Accommodation</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">
                                <h1 class="hdr-seven">Rooms &amp; Suites</h1>          
                                <div class="hdr-two">Accommodation</div>          
                                <p style="text-align:justify; font-size:16px;">Ellen's Place offers guests accommodation perfected by a myriad of amenities and blissful comforts. Offering ample living space, all accommodation options at the hotel have been elegantly designed and delicately furnished to create an atmosphere of grandeur and opulence. Relaxation emanates from every guest room and suite and every detail has been thoughtfully refined to highlight the spirit of the hotel.
                                </p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;"><strong>Guests may choose from the following accommodation options at Ellen's Place:</strong></h1>
                                <ul class="accom-list">
                                    <li>2 Standard Rooms</li>
                                    <li>4 Deluxe Rooms</li>
                                    <li>1 Suite</li>
                                </ul> 
                                <div style="clear:both;"></div> 

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->



                        <div class="highlight-panels">
                            <ul>
                                <li class="highlight" style="background: #ebebeb url('assets/images/acc/suites.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="suites.php"></a>   
                                    <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Suites</h2>     
                                            <div class="hdr-two fadeitem">The magnificent suite</div>      
                                            <p class="fadeitem">The grand and graceful style that is evident throughout the property and reflects the essence of living like royalty</p>            
                                            <a href="" class="btn-arrow fadeitem">View All</a>      
                                        </div><!--  .highlight-content-inside  -->      
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  


                                <li class="highlight" style="background: #ebebeb url('assets/images/acc/standard_slider1.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="standard_rooms.php"></a>   
                                    <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Standard Rooms</h2>      
                                            <div class="hdr-two fadeitem">An atmosphere of absolute peace and harmony</div>   
                                            <a href="#" class="btn-arrow fadeitem">View All</a>      
                                        </div><!--  .highlight-content-inside  -->      
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  


                                <li class="highlight" style="background: #ebebeb url('assets/images/acc/deluxe.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="deluxe_rooms.php"></a>   
                                    <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Deluxe Rooms</h2>      
                                            <div class="hdr-two fadeitem"> Majestic Deluxe Rooms</div>          
                                            <a href="#" class="btn-arrow fadeitem">View All</a>      
                                        </div><!--  .highlight-content-inside  -->      
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->        </ul>
                        </div><!--  .highligh-panels  -->

                        <?php /* ?>      <aside role="complementary">
                          <div class="ctatext-wrapper">
                          <div class="ctatext-text">
                          <h1 class="hdr-seven">Suite &amp; Rooms</h1>
                          <div class="hdr-two">Your Perfect Getaway Awaits</div>
                          <p>In-room amenities include a mini-fridge, an LCD TV and with satellite channels, a digital safe, as well as tea and coffee making facilities. A jacuzzi is also available in the Suite.</p>
                          <a href="#" class="btn-arrow">Book Now</a>
                          </div><!--  .ctatext-text  -->
                          </div><!--  .ctatext-wrapper  -->
                          </aside>   <?php */ ?> 
                        <?php include 'trip-advisor.php'; ?>           
                    </article>  


                </main>   
            </div><!--  #node-details  -->

            <?php include '../../includes/footer_ellens.php'; ?> 
    </body>
</html>
