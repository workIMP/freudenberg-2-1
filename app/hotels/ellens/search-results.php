<?php
require_once('../../top.php');

try{
    if(!NoCSRF::check('token', $_POST)){
        header('HTTP/1.0 403 Forbidden');
        header('Location:'.HTTP_PATH);
        exit;
    }
}
catch(exception $e){
    header('HTTP/1.0 403 Forbidden');
    die($e->getMessage());
}

require_once(DOC_ROOT.'classes/GoogleCustomSearch.php');

$searchstring = Generic::noHTML(filter_var($_POST['search'], FILTER_SANITIZE_STRING));

try{
	$search = new iMarc\GoogleCustomSearch($search_engine_data['ellens']['se_id'], $search_engine_data['ellens']['api_key']);
	$results = $search->search($searchstring);
}
catch(Exception $e){
    echo $e->getMessage();
    header('Location:'.HTTP_PATH);
    exit;
}



$result_count = ($results->totalResults > 10) ? 10 : $results->totalResults;

require_once(DOC_ROOT.'includes/header_ellens.php');
?>
<style type="text/css">
	
	ol.simple-list {
	list-style-type: none;
	list-style-type: decimal !ie; /*IE 7- hack*/
	
	margin: 0;
	margin-left: 3em;
	padding: 0;
	
	counter-reset: li-counter;
}
ol.simple-list > li{
	position: relative;
	margin-bottom: 20px;
	padding-left: 0.5em;
	min-height: 3em;
	/*border-left: 2px solid #CCCCCC;*/
}
ol.simple-list > li:before {
	position: absolute;
	top: 0;
	left: -1em;
	width: 0.8em;
	
	font-size: 3em;
	line-height: 1;
	font-weight: bold;
	text-align: right;
	color: #464646;

	/*content: counter(li-counter);*/
	/*counter-increment: li-counter;*/
}
article{
	margin: 98px auto;
    width: 800px;
    /*margin-top: ;*/
}

.search-title{
	font-size: 31px;
    color: #444;
    font-weight: 400;
    color: darkolivegreen;
}

.search-heading{
	margin: 0 auto 30px;
    width: 300px;
}

</style>
 <body class="node-type-accommodation-list">
        <header id="header" role="banner">  
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header>

        <div class="blur">  
            <div>
                <div id="main" role="main">
                    <div class="wrapper">
                        <article role="article">
                        <div class="search-heading">
                        	
                        	<strong><?php echo $result_count ?></strong> results were found for the search for <strong><?php echo $searchstring ?></strong>
                        </div>
                        
                            <ol class="simple-list">

                            <?php foreach ($results->results as $item): ?>

								<li>
									<div style="width:100px; float:left;display: inline;">
										<img width="100" height="100" src="<?php echo $item->thumbnail==""? 'assets/img/logo_elp.jpg' : $item->thumbnail ?> " alt="<?php echo $item->title ?>">
									</div>
									<div style="padding-left: 120px;">
										<a href="<?php echo $item->link ?>"><h3 class="search-title"><?php echo $item->title ?></h3></a>
										<p><?php echo $item->htmlSnippet ?></p>
									</div>
								</li>
								<hr>
							<?php endforeach ?>

							</ol>
                        </article>


                    </div><!--  .wrapper  -->

                </div><!--  #main  -->

            </div><!--  #node-details  -->

            <br><br><br><br><br><br>


            <footer id="footer" role="contentinfo">  

                <?php include 'trip-advisor.php'; ?> 
                <?php include '../../includes/footer_ellens.php'; ?>  

                </body>
                </html>