<style>.ctatext-wrapper{padding-top: 20px;padding-bottom: 20px;}</style>
<div id="nt-example1-container">
    <aside role="complementary">
        <div class="ctatext-buildadventure ctatext-wrapper" style="background:#f2f2f2;">
            <div class="col-md-12 ctatext-text">
                <div class="hdr-two" style="font-style: italic; color:#484848;">Guest Comments</div>
                <ul id="nt-example1">
                    <li>“Small very welcoming place. Friendly host, comfortable...”
                        <br><div style="font-weight:700; font-style:italic;text-indent:450px">-Martin, Singapore</div></li>

                    <li>“Lovely midrange hotel in Colombo, close to the main city area...”
                        <br><div style="font-weight:700; font-style:italic;text-indent:450px">-artdeco2, United Kingdom</div></li>

                    <li>“Oasis of calm in a busy city...”
                        <br><div style="font-weight:700; font-style:italic;text-indent:450px">-Lucie, Czech Republic</div></li>

                    <li>“Small hotel about 10 minutes tuk tuk ride from the Dutch Hospital...”
                        <br><div style="font-weight:700; font-style:italic;text-indent:450px">-grahamcr, Melbourne</div></li>

                </ul>
            </div>
        </div>
    </aside>
</div>