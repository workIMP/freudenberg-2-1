<?php 
$pg = ['property' => 'ellens', 'page' => 'promotions'];
include '../../includes/header_ellens.php';
?>
    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?>
            <style>
                #route{margin-left: 20.7% !important;}
                @media screen and (max-width: 1366px){
                    #route{margin-left: 9% !important;}
                }
                @media screen and (max-width: 768px){
                    #route{margin-left: -1.3% !important;}
                } 
            </style>

        </header><!--  #header  -->

        <?php include '../../includes/booking_ellens.php'; ?> 

        <div class="blur">  

            <div class="node--page_basic mode--full">  
                <aside role="complementary">
                    <?php include '../../includes/slider_ellens.php'; ?>
                </aside>    

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Promotions</li>
                    </breadcrumb>
                </div>

                <aside role="complementary">                     
                    <div class="grid">
                        <div class="hdr-two" style="text-align: left; padding: 10px; font-size:1.2em;">ELLEN'S PLACE PROMOTIONS</div>
                        <div style="clear:both"></div>

                        <?php $promotions = $cms->getPromotions(4); ?>
                        <?php if(count($promotions) > 0): ?>
                        <?php foreach($promotions as $promo): ?>
                           <figure class="effect-chico">
                              <img src="<?=PARENT_DOMAIN.'images/promotions/'.$promo->property_slug.'/'.$promo->image?>" alt=""/>
                              <figcaption>
                                <h2><?=$promo->title?></h2>
                                <p><?=$promo->description?></p>
                                <?php if(!empty($promo->trigger_action)): ?>
                                    <?php if($promo->trigger_action == 'page'): ?>
                                        <a href="<?=ELLENS_DOMAIN.'promotions/'.$cms->getPromoPage($promo->id)?>">View More</a>
                                    <?php elseif($promo->trigger_action == 'image'): ?>
                                        <a class="promo-pop" href="<?=PARENT_DOMAIN.'images/promotions/'.$promo->property_slug.'/popups/'.$cms->getPromoPopupImage($promo->id)?>">View more</a>
                                    <?php else: ?>
                                        <a href="#">View more</a>
                                    <?php endif; ?>
                                <?php endif; ?>
                              </figcaption>
                           </figure>                                
                        <?php endforeach; ?>
                        <?php else: ?>
                          <div class="hdr-two" style="font-style: italic; margin-bottom: 15px;"> No promotions are currently available. </div>
                        <?php endif; ?>

                    </div><!--  .ctacard-wrapper  -->
                </aside>    

            </div>  

            <footer id="footer" role="contentinfo"> 

                <?php include 'trip-advisor.php'; ?>
                <?php /* ?>    <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>

                <?php include '../../includes/footer_ellens.php'; ?>

                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                <script type="text/javascript" src="assets/js/jquery.magnific-popup.min.js"></script>
 

                <script>

                    jq191 = jQuery.noConflict( true );

                    jq191('.promo-pop').magnificPopup({
                        type: 'image'
                    })
                 
                </script>

    </body>

</html>