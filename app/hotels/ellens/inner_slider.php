<div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">
        <?php if (basename($_SERVER['PHP_SELF']) == 'suites.php') { ?>
            <div class="item"><img src="assets/images/acc/inner_suites/1.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_suites/2.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_suites/3.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_suites/4.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_suites/5.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_suites/6.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_suites/7.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_suites/8.jpg"></div>
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'deluxe_rooms.php') { ?>
            <div class="item"><img src="assets/images/acc/inner_deluxe/1.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/2.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/3.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/4.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/5.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/6.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/7.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/8.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/9.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/10.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/11.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_deluxe/12.jpg"></div>             
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'standard_rooms.php') { ?>
            <div class="item"><img src="assets/images/acc/inner_standard/1.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_standard/2.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_standard/3.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_standard/4.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_standard/5.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_standard/6.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_standard/7.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_standard/8.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_standard/9.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_standard/10.jpg"></div>
            <div class="item"><img src="assets/images/acc/inner_standard/11.jpg"></div>            
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'restaurant.php') { ?>
            <div class="item"><img src="assets/images/facilities/rest/rest-1.jpg" ></div>
            <div class="item"><img src="assets/images/facilities/rest/rest-2.jpg" ></div>
            <div class="item"><img src="assets/images/facilities/rest/rest-3.jpg" ></div>
            <div class="item"><img src="assets/images/facilities/rest/rest-4.jpg" ></div>
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'swimming-pool.php') { ?>
            <div class="item"><img src="assets/images/facilities/pool/pool-1.jpg" ></div>
            <div class="item"><img src="assets/images/facilities/pool/pool-2.jpg" ></div>
            <div class="item"><img src="assets/images/facilities/pool/pool-3.jpg" ></div>
            <div class="item"><img src="assets/images/facilities/pool/pool-4.jpg" ></div>
            <?php } ?>
    </div>
</div>     