﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_ellens.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_ellens.php'; ?> 

        <div class="node--page_basic mode--full">
            <aside role="complementary">
                <?php include '../../includes/slider_ellens.php'; ?>          
            </aside> 

            <div id="route">
                <breadcrumb class="menu">
                    <li><a href="index.php">Home</a></li>
                    <li><span class="arrow"> &gt; </span>Facilities</li>
                </breadcrumb>
            </div>

            <div id="main" role="main">     
                <article role="article" style="padding-top:10px;">
                    <div class="ctatext-wrapper">
                        <div class="ctatext-text">
                            <div class="hdr-two">Facilities</div>
                            <p style="text-align:justify; font-size:17px;">Ellen's Place offers a wide array of facilities, services and activities to ensure that guests of all ages are well pampered during their stay. Bask in the true essence of peace at Ellen's Place and lose track of time as you indulge in an unforgettable stay at the hotel.</p>
                            <h1 class="hdr-seven" style="text-align:left; font-size:13px; text-transform:none;">Ellen's Place offers:</h1>
                            <ul class="accom-list">
                                <li>An outdoor swimming pool</li>
                                <li>High speed Wi-Fi</li>
                                <li>Private function areas</li>
                            </ul>
                            <ul class="accom-list">
                                <li>In-room dining</li>
                                <li>Room service</li>
                                <li>Housekeeping</li>
                            </ul>        
                        </div><!--  .ctatext-wrapper  -->
                    </div><!--  .ctatext-text  -->

                    <div class="highlight-panels">
                        <ul>
                            <li class="highlight" style="background: #ebebeb url('assets/images/facilities/rest/rest_slider1.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="restaurant.php"></a>   
                                <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Restaurant</h2>     
                                        <div class="hdr-two fadeitem">Sri Lankan and International cuisine</div>      
                                        <p class="fadeitem">A memorable gastronomical journey, with dishes to please every palate</p>            
                                        <a href="" class="btn-arrow fadeitem">View All</a>      
                                    </div><!--  .highlight-content-inside  -->      
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->  


                            <li class="highlight" style="background: #ebebeb url('assets/images/facilities/pool/pool_slider1.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="swimming-pool.php"></a>   
                                <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Swimming Pool</h2> 
                                        <div class="hdr-two fadeitem">Unwind and Laze the hours away</div>     
                                        <p class="fadeitem">The ideal place for guests to unwind and laze the hours away</p>  
                                        <a href="#" class="btn-arrow fadeitem">View All</a>      
                                    </div><!--  .highlight-content-inside  -->      
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->  
                        </ul>
                    </div><!--  .highligh-panels  -->    
                </article>
            </div><!--  #main  -->

        </div><!--  #node-details  -->

        <footer id="footer" role="contentinfo">  

            <?php include 'trip-advisor.php'; ?> 
            <?php /* ?>    <aside role="complementary">
              <div class="ctatext-buildadventure ctatext-wrapper">
              <div class="ctatext-text">
              <h1 class="hdr-seven">Build your own Adventure</h1>
              <div class="hdr-two">Your Perfect Getaway Awaits</div>
              <p>Facilities guaranteed to kindle pure relaxation and bliss, Whatever your lifestyle or pace, Randholee offers something unique for everyone.</p>
              <a class="btn-arrow" href="#">Book Now</a>
              </div><!--  .ctatext-text  -->
              </div><!--  .ctatext-wrapper  -->
              </aside><?php */ ?>

            <?php include '../../includes/footer_ellens.php'; ?> 

    </body>
</html>
