﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include '../../includes/_slider_randholee.php'; ?>
                </aside>  

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                        
                        <div id="route">
                            <breadcrumb class="menu">
                                <li><a href="index.php">Home</a></li>
                                <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                                <li><span class="arrow"> &gt; </span>Shopping Boutique</li>
                            </breadcrumb>
                        </div>
                        
                            <div class="ctatext-text">         
                                <div class="hdr-two">Shopping Boutique</div>          
                                <p style="text-align:justify; font-size:16px;">Choose from an assortment of glamourous jewellery and gemstones at Randholee's gem boutique and take back a few mementos of your visit to Kandy.</p>  

                                <?php include 'inner_slider.php'; ?>         


                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_randolee.php'; ?> 
            </footer>    
    </body>
</html>
