﻿<!DOCTYPE html>

<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">

            <?php include '../../includes/navigation_randholee.php'; ?> 

        </header><!--  #header  -->  

        <?php include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">   

            <div id="node-6" class="node--accommodation_list mode--full">

                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/signature-dining/signature_s.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/signature-dining/signature_s4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/signature-dining/signature_s3.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/signature-dining/signature_s1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>
                    
                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>

                </aside>  
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="cuisine.php">Cuisine</a></li>
                        <li><span class="arrow"> &gt; </span>Signature Dining</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">

                    <article role="article">

                        <div class="ctatext-wrapper">

                            <div class="ctatext-text">         

                                <div class="hdr-two">Signature Dining</div>          

                                <h1 class="hdr-seven" style="text-align:left; font-size:17px; padding:10px"><strong>Dining with Nature</strong></h1>
                                <p style="text-align:justify; font-size:18px;">Have a picnic in a mystical Kandyan forest. Surround yourself with the beautiful colours of nature - the greens and browns of the trees and shrubs, the blues and whites of the skies and clouds and the yellows and reds of the various birds and other woodland creatures that can be found in the forest. Pick an isolated location to spread your blanket and enjoy a day of good food, relaxation and nature. Experience peace and tranquillity amidst the Kandy woods.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:17px;padding:10px"><strong>Dining Pool-side</strong></h1>
                                <p style="text-align:justify; font-size:18px;">Watch the reflection of the stars shimmer on the water as you dine beside the pool. Titillate the most discerning palate with a candle - lit dinner as the sun sets over the distant hills. Have dessert as the sky darkens and the lights of the homes in the village below turn on one by one. Dining beside the pool, on top of Mount Pleasant, is a romantic experience that will become a treasured memory. </p>  

                                <?php include 'inner_slider.php'; ?> 

                            </div><!--  .ctatext-text  -->

                        </div><!--  .ctatext-wrapper  -->                          

                    </article>            

                </main>   

            </div><!--  #node-details  -->



            <div style="clear:both"></div>

            <footer id="footer" role="contentinfo">  

                <?php include '../../includes/footer_randolee.php'; ?> 

            </footer>    

    </body>

</html>

