<!DOCTYPE html>
<html class="no-js">
<?php include '../../includes/header_randholee.php';?> 

<body class="node-type-accommodation-list">
<header id="header" role="banner">
  <?php include '../../includes/navigation_randholee.php';?> 
  </header><!--  #header  -->
  
<?php include '../../includes/booking_randholee.php';?> 

<div class="blur">  
  
  <div id="node-6" class="node--accommodation_list mode--full">
    <aside role="complementary">

<?php include '../../includes/_slider_randholee.php'; ?>

  </aside>
  
  <div id="route">
        <breadcrumb class="menu">
            <li><a href="index.php">Home</a></li>
            <li><span class="arrow"> &gt; </span>About Us</li>
        </breadcrumb>
  </div>  

  <main id="main" role="main">
    <article role="article">
      <div class="ctatext-wrapper">
        <div class="ctatext-text">         
          <div class="hdr-two">About Us</div>   
    <p style="text-align:justify; font-size:16px;">In May 2007, Randholee Luxury Resorts opened its doors to the world. Built on two acres of land, Randholee Luxury Resorts was the brain-child of Mrs. Suwanitha Senanayake, the Managing Director. The Randholee theme, the uniqueness of the hotel, the interior designing, the landscaping and so much more, are her marvellous ideas which she executed to perfection. All of the artistic influence in the hotel - the paintings in the rooms and the lobby are the works of Mrs. Suwanitha Senanayake. Late Mr. Ranjith Senanayake was the Chairman of Randholee Luxury Resorts and Freudenburg Shipping Agency; he was also the grandson of Mr. Don Stephen Senanayake, the first Prime Minister of Ceylon (now Sri Lanka) from 1947 to 1952. </p>
       
    <h1 class="hdr-seven" style="text-align:left; font-size:13px;"><strong>Kandy Esala Perahera</strong></h1>
      <p style="text-align:justify; font-size:14px;">
        Considered one of the most colourful religious pageants in Asia, Kandy's ten-day Esala Perahera is one of Sri Lanka's most spectacular festivals. The perahera (procession) is held to honour the sacred tooth enshrined in the Temple of the Sacred Tooth Relic.  
        <br><br>According to Buddhist history, Sri Lanka was chosen as the new home for the tooth relic of Lord Buddha because he had declared that his religion would be protected and preserved  there (in Sri Lanka) for over   2,500 years. 
        <br><br>The perahera is traditionally held over the last nine days of the lunar month of Esala, finishing on Nikini Poya day. This usually falls during late July or early August through the exact dates vary according to the vagaries of the lunar calendar; the authorities sometimes opt for slightly different dates depending on practical and astrological considerations. Filled with brilliant colour, dance and tradition, the essence of Sri Lankan history is captured in the annual Kandy Esala Perahera.
     
    <h1 class="hdr-seven" style="text-align:left; font-size:13px;"><strong>Temple of the Tooth Relic</strong></h1>
      <p style="text-align:justify; font-size:14px;">
        When Buddha attained nirvana his body was cremated and his tooth was acquired from the ashes. The tooth soon became an object of veneration and a belief grew that whoever had possession of the tooth would have the power to rule that land. In the ancient times , Sri Lanka was threatened with many foreign invasions and the seat of the kingdom was moved from city to city. Upon each change of capital, a new palace was built to enshrine the relic. Finally, it was bought to Kandy where it is at present in the Temple of the Sacred Tooth Relic. 
        <br><br>The temple which houses the sacred tooth relic is built in the royal palace complex in Kandy, which was the last capital of the Sinhalese Kings. The temple now comprises the royal palace, an audience hall and the mahamaluwa, which is a lawn area situated in front of the temple compound. The Royal Palace used to be the official residence of the government agent, Sir John D'Oyly and is now preserved as an archaeological museum. The audience hall is where the Kandyan kings held their royal court and was where the Kandyan Convention was drawn up and read out to the people. It is now used for state ceremonies and conserved under the department of archaeology. 
      </p>
     
        <h1 class="hdr-seven" style="text-align:left; font-size:13px;"><strong>About Kandy</strong></h1>
       <p style="text-align:justify; font-size:14px;">
       		Situated at an elevation of 500m above sea level, the historic city of Kandy is nestled among the misty hills in the central region of Sri Lanka. The shrine holding the Sacred Tooth Relic of the Lord Buddha (The Temple of the Tooth) is located in the centre of the city, making Kandy the most venerated city in Sri Lanka. Yearly, the Sacred Tooth Relic is honoured with a ten-day procession including lavishly decorated elephants, drummers, dancers, acrobats and baton twirlers. Classified as a World Heritage City by UNESCO, many Sinhalese traditions are kept alive in the city’s distinctive music, dance and architecture. The Temple of the Tooth, Peradeniya Botanical Gardens and the Riverside Elephant Park are some of the remarkable sites of this city.

<br><br>Kandy is considered the cultural capital of Sri Lanka and has an abundance of history and nature to offer. It is only 115km away from Colombo and has many sites to enjoy.</p>

         </div><!--  .ctatext-text  -->
      </div><!--  .ctatext-wrapper  -->                          
    </article>      
          
  </main>   
</div><!--  #node-details  -->
  
    <div style="clear:both"></div>
  <footer id="footer" role="contentinfo">  
  <?php include '../../includes/footer_randolee.php';?> 
   </footer>    
</body>
</html>
