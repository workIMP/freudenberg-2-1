<!DOCTYPE html>
<html class="no-js">
<?php include '../../includes/header_randholee.php';?> 

<body class="node-type-accommodation-list">
<header id="header" role="banner">
  <?php include '../../includes/navigation_randholee.php';?> 
  </header><!--  #header  -->
  
<?php include '../../includes/booking_randholee.php'; ?> 
<div class="blur">  
  
  <div id="node-6" class="node--accommodation_list mode--full">

  <main id="main" role="main">
    <article role="article">
      <div class="ctatext-wrapper">
      
<!--      <div id="route" style="margin-top: 10px;">
            <breadcrumb class="menu">
                <li><a href="index.php">Home</a></li>
                <li><span class="arrow"> &gt; </span>Terms and Conditions</li>
            </breadcrumb>
        </div>-->
      
        <div class="ctatext-text">         
          <div class="hdr-two">Terms and Conditions</div>          
          <p style="text-align:justify; font-size:15px;">This Web Site is owned and operated by Randholee Resorts (Private) Limited, a company duly incorporated under the Companies Law of Sri Lanka, having its registered office at 103/14 Dharmapala Mawatha,Colombo 7, in the Democratic Socialist Republic of Sri Lanka (hereinafter, as and when the context so requires, called and referred to as "Randholee Resorts").<br><br>

By accessing and using this Web Site, you agree to be bound by the terms and conditions of use, set out herein. The attached terms of online reservations shall apply, when you make your reservations or purchases online and shall form part and parcel of this Agreement. The Principals of Data Protection of Randholee Resorts is also annexed hereto for your reference.</p>
        
			<h1 class="hdr-seven" style="text-align:left; font-size:16px; text-transform:none;"><strong>Terms and Conditions of Use</strong></h1>
            
            <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Acceptable Use</strong></h1>
				<p style="text-align:justify; font-size:14px;">You may use this Web Site in accordance with these terms and conditions for lawful and proper purposes complying with all applicable laws, regulations and codes of practice in Sri Lanka and/or other jurisdictions from which you are accessing the Web Site.</p>
                
                <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;">In particular you agree you will not -</h1>
                <ul class="priv-poly">
                    <li style="margin-bottom: 10px;">Post, transmit or disseminate any information via this Web Site which is or may be harmful, obscene or otherwise illegal.</li>
                    <li style="margin-bottom: 10px;">Use this Web Site in a manner which causes or may cause infringement of the rights of another.</li>
                    <li style="margin-bottom: 10px;">Use any software, routine, procedure or device to interfere or attempt to interfere with the operation or functionality of this Web Site including but not limited to making available corrupt and/or fraudulent data, viruses via whatever means or interfere with its availability to others.</li>
                    <li style="margin-bottom: 10px;">Deface, alter or interfere with the look and feel of this Web Site or underlying software code.</li>
                    <li style="margin-bottom: 10px;">Take any action that imposes an unreasonable or disproportionately large load on the Web Site or related infrastructure.</li>
                    <li style="margin-bottom: 10px;">Obtain or attempt to obtain unauthorized access by whatsoever means to any of our networks.</li>
                </ul>
                <p style="text-align:justify; font-size:14px;">Where Randholee Resorts believes in its absolute discretion that you are in breach of any of these terms and conditions, without prejudice to any of our rights at law or otherwise, we reserve the right to deny you access to this Web Site.</p>
                
   			<h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Copyright and Restrictions on Use</strong></h1>
				<p style="text-align:justify; font-size:14px;">All trademarks, service marks, copyright, database rights, and other intellectual property rights in materials on this Web Site as well as the organization and layout of this Web Site together with the underlying software belong to Randholee Resorts or its licensors. The trademark, service mark and logos of Randholee Resorts and its licensors must not be used, modified or adapted in any way without the prior written authorization. The material on this Web Site and the underlying software codes may not be copied, modified, altered, published, broadcast, distributed, linked to another Web Site, sold or transferred in whole or in part without an express written permission. However, the information contained here may be downloaded, printed or copied for your personal non-commercial use.</p>
                
            <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Disclaimer, Limitation of Liability and Indemnity</strong></h1>
                <ul class="priv-poly">
                    <li style="margin-bottom: 10px;">This Web Site has been compiled in good faith by Randholee Resorts. No representation is made or warranty given, either express or implied as to the completeness or accuracy of the information that it contains, that it will be uninterrupted or error free or that any information is free of bugs, viruses, worms, trojan horses or other harmful components.</li>
                    <li style="margin-bottom: 10px;">To the maximum extent permitted by law, Randholee Resorts disclaims all implied warranties with regard to information, products, services and material provided through this Web Site. All such information, products, services and materials are provided "as is" and "as available" without warranty of any kind.</li>
                    <li style="margin-bottom: 10px;">This Web Site may contain links to Web Sites operated by third parties. Such links are provided for your reference only and your use of these sites may be subject to terms and conditions posted on them. Randholee Resorts inclusion of links to other Web Sites does not imply any endorsement of the material on such Web Sites and Randholee Resorts accepts no liability for their contents.</li>
                    <li style="margin-bottom: 10px;">By accessing this Web Site you agree that Randholee Resorts will not be liable for any direct, indirect or consequential loss or damages of any nature arising from the use of this Web Site, including information and material contained in it, from your access of other material on the Internet via web links from this Web Site, delay or inability to use this Web Site or the availability and utility of the products and services.</li>
                    <li style="margin-bottom: 10px;">You further agree to indemnify, hold Randholee Resorts, and its service providers harmless from and covenant not to sue Randholee Resorts for any claims based on using this Web Site.</li>
                </ul>
               <div style="clear:both"></div>
                
            <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Modification of Terms</strong></h1>
                <p style="text-align:justify; font-size:14px;">Randholee Resorts may make improvements and/or changes in the products, services and prices described in this Web Site at anytime without notice. Changes are periodically made to the Web Site. Randholee Resorts may amend these terms by posting the amended terms on this Web Site. Continued use of this Web Site following such changes shall constitute your acceptance of such changes.</p>
             
             <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Age and Responsibility</strong></h1>
				<p style="text-align:justify; font-size:14px;">You represent you are of sufficient legal age to use this Web Site and you possess the legal right and ability to enter into this Agreement and use this site in accordance with all of the applicable terms and conditions and create binding legal obligations for any liability you may incur as a result of the use of this Web Site under your name or account including, without limitation, all use of your account by others including minors living with you.</p>
              
             <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Child Policy </strong></h1>
               <ul class="priv-poly">
                    <li>Children between 0-5.99 years will be accommodated in the parents/adults room free of charge</li>
                    <li>Children between 6-11.99 years will be charged a child supplement of 25% of the base category double room rate on the booked boarding basis (charged per child per night). The child supplement will be offered a maximum of one extra bed per room.</li>
                </ul> 
              
             <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Cancellation Policy </strong></h1>
                 <ul class="priv-poly">
                    <li>Cancelations made fourteen (14) or more days prior to the date of arrival will incur a 0% charge. (100 % Refund)</li>
                    <li>Cancelations made three (3) to fourteen (14) days prior to the date of arrival will incur a 50% of first night Room charge.</li>
                    <li>Cancelations made less than two (2) days prior to the arrival date will incur a 100% of First Night room charge.</li>
                    <li>All No shows will be charged at 100% of their full stay. (Include meal plans).</li>
                    <li>If the room you are booking is labeled as non-refundable, non-cancellable or similar, all cancellations will incur a 100% charge, regardless of the date in which the cancellation is requested.</li>
                </ul>
                <p style="text-align:justify; font-size:14px;">Cancellation policies can vary seasonally or depending on the hotel room type.</p> 
                
             <h1 class="hdr-seven" style="text-align:left; font-size:15px; text-transform:none;"><strong>Special value added services :</strong></h1>
                 <ul class="priv-poly">
                    <li>Child care services – US$ 15 net per day.</li>
                    <li>Guide services for excursions – US$ 25 net per day</li>
                    <li>Kandy city & shopping tour – US$ 60 net per day.</li>
                </ul>
                          
          </div><!--  .ctatext-text  -->
      </div><!--  .ctatext-wrapper  -->                          
    </article>      
          
  </main>   
</div><!--  #node-details  -->
  
    <div style="clear:both"></div>
  <footer id="footer" role="contentinfo">  
  <?php include '../../includes/footer_randolee.php';?> 
   </footer>    
</body>
</html>
