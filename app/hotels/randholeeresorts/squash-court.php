﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php // include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/squash-court/squash1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/squash-court/squash2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/squash-court/squash3.jpg') no-repeat 50% 50%; background-size: cover;"></div>

                    </div> 
                    
                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                        <li><span class="arrow"> &gt; </span>Squash Court</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Squash Court</div>          
                                <p style="text-align:justify; font-size:16px;">Randholee offers guests a state-of-the-art squash court ideal for both training and matches. The glass-backed, indoor court is fully equipped with racquets, balls and gear, and features an ample viewing area to facilitate sporting events. The services of a personal trainer are also offered by the hotel to beginners.<br><br>

                                    Spend a few minutes practising your serve or sportily challenge another guest to a friendly match. This popular activity provides guests with an excellent opportunity to get to know each other and have fun.</p>  

                                <?php include 'inner_slider.php'; ?> 

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_randolee.php'; ?> 
            </footer>    
    </body>
</html>
