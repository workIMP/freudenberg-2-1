﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php // include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/cuisine_s.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/cuisine_s1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/cuisine_s2.jpg') no-repeat 50% 50%; background-size: cover;"></div> 
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/cuisine_s3.jpg') no-repeat 50% 50%; background-size: cover;"></div> 
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/cuisine_s4.jpg') no-repeat 50% 50%; background-size: cover;"></div>  
                    </div>
                    
                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Cuisine</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Cuisine</div>          
                                <p align="justify">Dining at Randholee Luxury Resort is a sumptuous experience fit for royalty. Savour fragrant rice from India, creamy pasta from Italy or soft couscous from the Middle East during our International Buffet nights. Our Sri Lankan buffet allows guests to experience the texture and taste of traditional foods rich with spices and coconut milk. If you are in the mood for something unique, order from our a la carte menu which has an array of local and international choices.</p>

                                <p align="justify">During the weekends, a variety of theme nights are organized. Among them, guest favourites include Barbecue Nights, Sri Lankan Nights and Mongolian Nights. Theme nights, weather permitting, are usually held by the pool and include a calypso band. </p> 
                                <!--<a href="#" class="btn-underline">Book Your Getaway</a>-->        

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->



                        <div class="highlight-panels">
                            <ul>
                                <?php /* ?>          <li class="highlight" style="background: #ebebeb url('assets/images/cuisine/buffet.jpg') no-repeat 50% 50%; background-size: cover;">
                                  <a class="highlight-hotspot" href="#"></a>
                                  <div class="highlight-background"></div>  .highlight-content-inside
                                  <div class="highlight-content">
                                  <div class="highlight-content-inside">
                                  <h2 class="hdr-four">Buffet</h2>
                                  <div class="hdr-two fadeitem">A sumptuous experience fit for royalty</div>
                                  <a href="#" class="btn-arrow fadeitem">View All</a>
                                  </div>
                                  </div>
                                  </li> <?php */ ?>

                                <li class="highlight" style="background: #ebebeb url('assets/images/cuisine/bar.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Bar</h2>      
                                            <div class="hdr-two">Bar</div>      
                                            <p class="fadeitem">Step into an extraordinary world fashioned for uninterrupted, luxurious peace.</p>            
                                            <a href="bar.php" class="btn-arrow">View All</a>      
                                        </div><!--  .highlight-content-inside  -->      
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  


                                <li class="highlight" style="background: #ebebeb url('assets/images/cuisine/signature_dining.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Signature Dining</h2>      
                                            <div class="hdr-two">Absolutely Breathtaking</div>      
                                            <p class="fadeitem">Nestled in the heart of Mount Pleasant and surrounded by its beauty.</p>            
                                            <a href="signature_dining.php" class="btn-arrow">View All</a>      
                                        </div><!--  .highlight-content-inside  -->      
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->        </ul>
                        </div><!--  .highligh-panels  -->

                        <?php /* ?>      <aside role="complementary">
                          <div class="ctatext-wrapper">
                          <div class="ctatext-text">
                          <h1 class="hdr-seven">Bar, Buffet &amp; Signature Dining</h1>
                          <div class="hdr-two">Your Perfect Getaway Awaits</div>
                          <p>Breakfast with the rising sun, lunch with the passing clouds and enjoy a candlelight dinner with the glimmering stars.</p>
                          <a href="#" class="btn-arrow">Book Now</a>
                          </div><!--  .ctatext-text  -->
                          </div><!--  .ctatext-wrapper  -->

                          </aside>  <?php */ ?>             
                    </article>  


                </main>   
            </div><!--  #node-details  -->
            <?php include 'trip-advisor.php'; ?> 

            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_randolee.php'; ?> 

                </body>
                </html>
