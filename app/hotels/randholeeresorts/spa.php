﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php // include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/spa/spa1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/spa/spa2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/spa/spa3.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/spa/spa5.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/spa/spa6.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div> 
                    
                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                        <li><span class="arrow"> &gt; </span>Spa</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Spa</div>          
                                <p style="text-align:justify; font-size:16px;">Revel in a blissful sensory experience at the Randholee Spa as you awaken your inner spirit amidst a warm and nurturing environment. We offer an array of treatments, massages and rituals that have been designed to pamper the body and replenish the soul, leaving you blessed with a wonderful feeling of inner peace and harmony.<br><br> 

                                    Luxuriate in a soothing warm Jacuzzi or simply succumb to the hands of our expert masseurs as you let them whisk you away to a state of inner harmony. Each moment spent at the spa will leave you feeling refreshed and fully invigorated. </p>  
                                <?php include 'inner_slider.php'; ?> 


                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_randolee.php'; ?> 
            </footer>    
    </body>
</html>
