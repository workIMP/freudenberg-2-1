﻿<!DOCTYPE html>
<html class="no-js">
<?php include '../../includes/header_randholee.php'; ?>

<body class="node-type-accommodation-list">
<header id="header" role="banner">
  <?php include '../../includes/navigation_randholee.php';?> 
  </header><!--  #header  -->
  
<?php // include '../../includes/booking_randholee.php';?> 
  
<div class="node--page_basic mode--full">
      <aside role="complementary">
     		 <?php include '../../includes/slider_randholee.php';?>          
      </aside> 
      
      <div id="route">
            <breadcrumb class="menu">
                <li><a href="index.php">Home</a></li>
                <li><span class="arrow"> &gt; </span>Activities</li>
            </breadcrumb>
      </div>

  <div id="main" role="main">     
    <article role="article" style="padding-top:10px;">
        <div class="ctatext-wrapper">
            <div class="ctatext-text">
                <div class="hdr-two">Activities</div>
                  <p style="text-align:justify; font-size:17px;">Randholee Luxury Resort offers various facilities to make your stay with us more pleasurable. Work out in the gym, swim a lap in the pool or schedule a massage at the Ayurveda spa. Our sophisticated Business Centre includes computers with high speed Internet access and the entire premise of the hotel is wi-fi enabled so that you may browse the Internet from any corner of the hotel. Hike up a misty mountains during the day and spend your evenings playing a game of darts or pool in the comforts of the hotel. Should you run out of local currency, head straight to our Currency Exchange Centre where you can buy local currency at competitive bank rates. With so many facilities within the hotel you will never want to check-out.</p>        
            </div><!--  .ctatext-wrapper  -->
        </div><!--  .ctatext-text  -->
            
    
      <div class="activities-list highlight-panels">
        <ul>
          <li class="highlight" style="background: #ebebeb url('assets/images/squash_court/sc2.jpg') no-repeat 50% 50%; background-size: cover;"> 
    <a class="highlight-hotspot" href="#"></a>
    <div class="highlight-background"></div><!--  .highlight-background  -->
      <div class="highlight-content">
        <div class="highlight-content-inside">
          <h2 class="hdr-four">Squash Court</h2>     
            <div class="hdr-two fadeitem">a state-of-the-art squash court</div>      
              <p class="fadeitem">The glass-backed, indoor court is fully equipped with racquets, balls and gear, and features an ample viewing area to facilitate sporting events.</p>
              <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
      </div><!--  .highlight-content-inside  -->
    </div><!--  .highlight-content  -->
  </li><!--  .highlight  -->  

      <li class="highlight" style="background: #ebebeb url('assets/images/badminton_and_table_tennis/bm3.jpg') no-repeat 50% 50%; background-size: cover;">
    <a class="highlight-hotspot" href="#"></a>
    <div class="highlight-background"></div><!--  .highlight-background  -->
    	<div class="highlight-content">
      		<div class="highlight-content-inside">
                <h2 class="hdr-four">Badminton & Table Tennis</h2>
                	<div class="hdr-two fadeitem">Ideal for family and friends</div>
                <p class="fadeitem">Indulging in a game of badminton or table tennis is an excellent way to stay fit during your stay at Randholee. Ideal for family and friends, the table tennis area and the badminton court at the hotel are located in close proximity to each other for convenience and have been designed to offer both a stimulating and relaxing experience.</p>     
        	<!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
      </div><!--  .highlight-content-inside  -->
    </div><!--  .highlight-content  -->
  </li><!--  .highlight  -->  
  
      <li class="highlight" style="background: #ebebeb url('assets/images/shopping_boutique/sb3.jpg') no-repeat 50% 50%; background-size: cover;">
    <a class="highlight-hotspot" href="#"></a>
    <div class="highlight-background"></div><!--  .highlight-background  -->
    	<div class="highlight-content">
      		<div class="highlight-content-inside">
                <h2 class="hdr-four">Shopping Boutique</h2>
                <p class="fadeitem">Choose from an assortment of souvenirs and precious gemstones at Randholee's shopping boutique and take back a few mementos of your visit to Kandy.</p>     
        	<!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
      </div><!--  .highlight-content-inside  -->
    </div><!--  .highlight-content  -->
  </li><!--  .highlight  -->  

      <li class="highlight" style="background: #ebebeb url('assets/images/infinity_pool/ip3.jpg') no-repeat 50% 50%; background-size: cover;">
    <a class="highlight-hotspot" href="#"></a>       
    <div class="highlight-background"></div><!--  .highlight-background  -->
    	<div class="highlight-content">
      		<div class="highlight-content-inside">
                <h2 class="hdr-four">Infinity Pool</h2>
                	<div class="hdr-two fadeitem">Immerse yourself in the refreshing water</div>
                <p class="fadeitem">An endless stretch of water merges into the sky to make you feel as though you are swimming in the clouds.</p>              
        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
      </div><!--  .highlight-content-inside  -->
    </div><!--  .highlight-content  -->
  </li><!--  .highlight  -->  
  
     <li class="highlight" style="background: #ebebeb url('assets/images/wedding/wed1.jpg') no-repeat 50% 50%; background-size: cover;">
    <a class="highlight-hotspot" href="wedding.php"></a>       
    <div class="highlight-background"></div><!--  .highlight-background  -->
    	<div class="highlight-content">
      		<div class="highlight-content-inside">
                <h2 class="hdr-four">Weeding</h2>
                	<div class="hdr-two fadeitem">A royal wedding experience</div>
                <p class="fadeitem">Exchange your wedding vows as the sun sets behind the misty mountains of Kandy</p>              
        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
      </div><!--  .highlight-content-inside  -->
    </div><!--  .highlight-content  -->
  </li><!--  .highlight  -->  

      <li class="highlight" style="background: #ebebeb url('assets/images/spa/spa1.jpg') no-repeat 50% 50%; background-size: cover;">
    <a class="highlight-hotspot" href="#"></a>
    <div class="highlight-background"></div><!--  .highlight-background  -->
    	<div class="highlight-content">
      		<div class="highlight-content-inside">
                <h2 class="hdr-four">Spa</h2>
                	<div class="hdr-two fadeitem">Revel in a blissful sensory experience at spa</div>
                <p class="fadeitem">Luxuriate in a soothing warm Jacuzzi or simply succumb to the hands of our expert masseurs as you let them whisk you away to a state of inner harmony. Each moment spent at the spa will leave you feeling refreshed and fully invigorated. </p>
        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
      </div><!--  .highlight-content-inside  -->
    </div><!--  .highlight-content  -->
  </li><!--  .highlight  -->
  
  <li class="highlight" style="background: #ebebeb url('assets/images/conferences_and_workshops/conf1.jpg') no-repeat 50% 50%; background-size: cover;">
    <a class="highlight-hotspot" href="#"></a>
    <div class="highlight-background"></div><!--  .highlight-background  -->
    	<div class="highlight-content">
      		<div class="highlight-content-inside">
                <h2 class="hdr-four">Conferences and Workshops</h2>
                	<div class="hdr-two fadeitem">The remarkable conference centre with a resplendent view.</div>
                <p class="fadeitem">Randholee Luxury Resorts is a wonderful location for modest conferences and corporate events.</p>
        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
      </div><!--  .highlight-content-inside  -->
    </div><!--  .highlight-content  -->
  </li><!--  .highlight  -->    

      <li class="highlight" style="background: #ebebeb url('assets/images/fitness_centre/fc4.jpg') no-repeat 50% 50%; background-size: cover;">
    <a class="highlight-hotspot" href="#"></a>
    <div class="highlight-background"></div><!--  .highlight-background  -->
    	<div class="highlight-content">
      		<div class="highlight-content-inside">
                <h2 class="hdr-four">Fitness Centre</h2>
                	<div class="hdr-two fadeitem">A state-of-the-art gym</div>
                <p class="fadeitem">Equipped with weight lifting machines, treadmills and steppers, Randholee Luxury Resort offers you a state-of-the-art gym to get a fabulous workout during your holidays.</p>
        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
      </div><!--  .highlight-content-inside  -->
    </div><!--  .highlight-content  -->
  </li><!--  .highlight  -->  

      <li class="highlight" style="background: #ebebeb url('assets/images/business_centre/bc2.jpg') no-repeat 50% 50%; background-size: cover;">
    <a class="highlight-hotspot" href="#"></a>
    <div class="highlight-background"></div><!--  .highlight-background  -->
    	<div class="highlight-content">
      		<div class="highlight-content-inside">
                <h2 class="hdr-four">Business Centre</h2>
                <p class="fadeitem">Randholee Luxury Resort offers our esteemed guests various facilities to utilize when conducting business from our hotel, leaving you free to leave your laptops and cell phones at home. Computers with high-speed Internet access, printers, scanners, fax machines, copy machines and international direct dialling facilities are available for guests to utilise. </p>
        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
      </div><!--  .highlight-content-inside  -->
    </div><!--  .highlight-content  -->
  </li><!--  .highlight  -->        
 </ul>
      </div><!--  .highlight-panels  -->
    </article>
      </div><!--  #main  -->

    <?php /*?><aside role="complementary">
    	<h1 class="hide-visual">Sonora Resort Offerings</h1>
    		<div class="ctacard-wrapper">
      			<div class="ctacard-card equal-height">
    				<div class="inner-ratio">
        				<img class="lazy" data-src="https://sonoraresort.com/sites/default/files/call_to_actions/sonora_20141006_wp_0990_1000x750_0.jpg" alt="Preparing for your Adventure">
  					</div><!--  .inner-ratio  -->
  				<div class="ctacard-text">
    				<h2 class="hdr-four">Preparing for your Adventure</h2>
    				<p>Find out when to book, what to pack and what to wear.</p>
  				</div><!--  .ctacard-text  -->
  <div class="ctacard-button">
    <a href="preparing-your-adventure.htm" class="btn-arrow">Read More</a>  </div><!--  .ctacard-button  -->
      
				</div><!--  .ctacard-card  -->
                
            <div class="ctacard-card equal-height">
    				<div class="inner-ratio">
        				<img class="lazy" data-src="https://sonoraresort.com/sites/default/files/call_to_actions/sonora_20141006_wp_0990_1000x750_0.jpg" alt="Preparing for your Adventure">
  					</div><!--  .inner-ratio  -->
  				<div class="ctacard-text">
    				<h2 class="hdr-four">Preparing for your Adventure</h2>
    				<p>Find out when to book, what to pack and what to wear.</p>
  				</div><!--  .ctacard-text  -->
  <div class="ctacard-button">
    <a href="preparing-your-adventure.htm" class="btn-arrow">Read More</a>  </div><!--  .ctacard-button  -->
      
				</div><!--  .ctacard-card  -->
            
            <div class="ctacard-card equal-height">
                                <div class="inner-ratio">
                                    <img class="lazy" data-src="https://sonoraresort.com/sites/default/files/call_to_actions/sonora_20141006_wp_0990_1000x750_0.jpg" alt="Preparing for your Adventure">
                                </div><!--  .inner-ratio  -->
                            <div class="ctacard-text">
                                <h2 class="hdr-four">Preparing for your Adventure</h2>
                                <p>Find out when to book, what to pack and what to wear.</p>
                            </div><!--  .ctacard-text  -->
              <div class="ctacard-button">
                <a href="preparing-your-adventure.htm" class="btn-arrow">Read More</a>  </div><!--  .ctacard-button  -->
                  
                            </div><!--  .ctacard-card  -->    
         </div><!--  .ctacard-wrapper  -->
  </aside><?php */?> 
  </div><!--  #node-details  -->

  
    
  <footer id="footer" role="contentinfo">  
          
  
    <aside role="complementary">
      <div class="ctatext-buildadventure ctatext-wrapper">
        <div class="ctatext-text">
          <h1 class="hdr-seven">Build your own Adventure</h1>
          <div class="hdr-two">Your Perfect Getaway Awaits</div>
          <p>Facilities guaranteed to kindle pure relaxation and bliss, Whatever your lifestyle or pace, Randholee offers something unique for everyone.</p>
          <a class="btn-arrow" href="#">Book Now</a>
        </div><!--  .ctatext-text  -->
      </div><!--  .ctatext-wrapper  -->
    </aside>
  
    <section role="region">
      <div class="newsletter">
      
        <div class="newsletter-thumb">
          <div class="inner-ratio">
            <img class="lazy" data-src="assets/images/newsletter-thumb-left-1.jpg?4056">
          </div>
        </div>
      
        <div class="newsletter-subscribe">
          <div class="inner-ratio">            
            <img class="lazy" data-src="assets/images/newsletter-thumb-mid.jpg">
            <div class="highlight-background-static"></div>   
            <form action="#" method="post" id="newsletter-subscribe">
              <input type="hidden" id="hotel" name="hotel" value="MTk2">
              <input type="hidden" id="redirect-url" name="redirect_url" value="#">            
              <h1 class="hdr-seven">Subscribe to our newsletter</h1>
              
            	<!--[if lt IE 10]><label for="first-name">First Name</label><![endif]-->
            	<input type="text" value="" name="subscriber[first_name]" class="required" id="first-name" placeholder="First Name">
               
              <!--[if lt IE 10]><label for="last-name">Last Name</label><![endif]-->
              <input type="text" value="" name="subscriber[last_name]" class="required" id="last-name" placeholder="Last Name">
            
              <!--[if lt IE 10]><label for="email">Email</label><![endif]-->
              <input type="email" value="" name="subscriber[email]" class="required" id="email" placeholder="Email">
              
              <input type="submit" value="Subscribe" class="btn-arrow" name="submit" id="submit">
            </form>
          </div>
        </div><!--  .newsletter-subscribe  -->
        
        <div class="newsletter-thumb">
          <div class="inner-ratio">
            <img class="lazy" data-src="assets/images/newsletter-thumb-right-5.jpg?4056">
          </div>
        </div>        
      
      </div><!--  .newsletter"  -->
    </section>
    
  <?php include '../../includes/footer_randolee.php';?> 

</body>
</html>
