﻿<!DOCTYPE html>
<html class="no-js">
    <?php
    include '../../includes/header_randholee.php';
    include_once '../../classes/mailing.class.php';
    include_once '../../config/country_list.php';


    $name = "";
    $subject = "";
    $email = "";
    $comments = "";
    $address = "";
    $country = "";
    $errors = "";

    if (isset($_POST['contact_submit'])) {

        if (isset($_POST['g-recaptcha-response'])) {
            $recaptcha = new \ReCaptcha\ReCaptcha($secret);
            $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
            if ($resp->isSuccess()) {
                $google_verification = TRUE;
            } else {
                $google_verification = TRUE;
                $hasError = true;
                $err = $resp->getErrorCodes();
                $errors = '<li>Please prove your not a robot</li>';
//                header('Location:' . HTTP_PATH . 'contact-us.php');
            }
        }

        if ($google_verification) {
            //Check to make sure that the name field is not empty
            if (trim($_POST['contact_name']) == '') {
                $hasError = true;
                $errors .="<li>Name Cannot Blank</li>";
            } else {
                $name = stripslashes(trim($_POST['contact_name']));
            }

//Check to make sure that the subject field is not empty
            if (trim($_POST['contact_subject']) == '') {
                $hasError = true;
                $errors .="<li>Subject Cannot Blank</li>";
            } else {
                $subject = stripslashes(trim($_POST['contact_subject']));
            }

//                $address = stripslashes(trim($_POST['address']));
            $contact_country = trim($_POST['countat_country']);
            $contactno = stripslashes(trim($_POST['contact_number']));

//Check to make sure sure that a valid email address is submitted
            if (trim($_POST['contact_email']) == '') {
                $hasError = true;
                $errors .="<li>Email Cannot Be Blank</li>";
            } else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['contact_email']))) {
                $hasError = true;
                $errors .="<li>Please Enter Valid Email</li>";
            } else {
                $email = trim($_POST['contact_email']);
            }

//Check to make sure comments were entered
            if (trim($_POST['contact_message']) == '') {
                $hasError = true;
                $errors .="<li>Message Cannot be Blank</li>";
            } else {
                if (function_exists('stripslashes')) {
                    $comments = stripslashes(trim($_POST['contact_message']));
                } else {
                    $comments = trim($_POST['contact_message']);
                }
            }
        }

//If there is no error, send the email
        if (!isset($hasError)) {
            $today = date("Y-m-d");
            $time = date("H:i:s");
            $IP = $_SERVER['REMOTE_ADDR'];
            $subject = $subject;
            $random_hash = md5(time());
            $headers = "";
            $headers .= "\r\nContent-Type:text/html; charset=iso-8859-1\n boundary=\"PHP-alt-" . $random_hash . "\"";
            $headers = "MIME-Version: 1.0\n";
            $headers .= "Content-Type:text/html; charset=iso-8859-1\n";
            $headers .= "From:reservations@randholeeresorts.com\n";
            $headers .= "Return-Path:reservations@randholeeresorts.com\n";
            $emailbody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Freudenberg Leisure | Contact Us</title>
    </head>
    <body>
        <div style="width: 100%; height: auto; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59); background-color: #fff;">
            <div style="margin-bottom: 1%; text-align: center;">
                <img src="http://www.freudenbergleisure.com/assets/img/email_fsaleisure_randholee_logo.png" width=40% height=40% alt="Freudenberg Leisure - Randholee Resorts"/>
            </div>
            <div style="text-align: center; background: url(http://www.freudenbergleisure.com/assets/img/email_randholee_header.jpg) repeat; padding: 1%; font-weight:600; color: #FFF; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important; -webkit-box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                <h2 style="color: #FFF;">Messages / Comments / Inquiries - ' . $today . ' at ' . $time . '</h2>
            </div>
            <div style="margin-top:2%; margin-bottom: 2%; z-index: 2; position: relative; height: auto;">
                <table width="100%" cellspacing="0" border="0">
                    <tr>
                        <td>&nbsp;</td>
                        <td width="8%" style="text-align: center;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; text-align: center; font-weight: 800; padding: 20% 0%; font-size:18px; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">1</div>
                        </td>
                        <td width="2%">&nbsp;</td>
                        <td width="20%" style="text-align: left;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; font-weight: 600; background-color: #FFFFFF; padding: 8% 0% 8% 25%; font-size:18px; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">Name</div>
                        </td>
                        <td width="2%">&nbsp;</td>
                        <td width="50%" style="margin-left: 5%;">
                            <div style="font-family:Helvetica Light sans-serif; font-size:18px; color:rgba(102,102,102,1); padding: 3% 2% 3% 6%; position: relative; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                                ' . $name . '
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="6">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td width="8%" style="text-align: center;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; text-align: center; font-weight: 800; padding: 20% 0%; font-size:18px; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">2</div>
                        </td>
                        <td width="2%">&nbsp;</td>
                        <td width="20%" style="text-align: left;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; font-weight: 600; background-color: #FFFFFF; padding: 8% 0% 8% 25%; font-size:18px; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">E-Mail</div>
                        </td>
                        <td width="2%">&nbsp;</td>
                        <td width="50%" style="margin-left: 5%;">
                            <div style="font-family:Helvetica Light sans-serif; font-size:18px; color:rgba(102,102,102,1); padding: 3% 2% 3% 6%; position: relative; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                                ' . $email . '
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="6">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td valign="top" style="text-align: center;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; text-align: center; font-weight: 800; padding: 20% 0%; font-size:18px; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">3</div>
                        </td>
                        <td>&nbsp;</td>
                        <td valign="top" style="text-align: left;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; font-weight: 600; background-color: #FFFFFF; padding: 8% 0% 8% 25%; font-size:18px; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">IP Address</div>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <div style="font-family:Helvetica Light sans-serif; font-size:18px; color:rgba(102,102,102,1); padding: 3% 2% 3% 6%; position: relative; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                                ' . $IP . '
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="6">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td width="8%" style="text-align: center;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; text-align: center; font-weight: 800; padding: 20% 0%; font-size:18px; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">4</div>
                        </td>
                        <td width="2%">&nbsp;</td>
                        <td width="20%" style="text-align: left;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; font-weight: 600; background-color: #FFFFFF; padding: 8% 0% 8% 25%; font-size:18px; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">Contact No.</div>
                        </td>
                        <td width="2%">&nbsp;</td>
                        <td width="50%" style="margin-left: 5%;">
                            <div style="font-family:Helvetica Light sans-serif; font-size:18px; color:rgba(102,102,102,1); padding: 3% 2% 3% 6%; position: relative; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                                ' . $contactno . '
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="6">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td valign="top" style="text-align: center;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; text-align: center; font-weight: 800; padding: 20% 0%; font-size:18px; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">5</div>
                        </td>
                        <td>&nbsp;</td>
                        <td valign="top" style="text-align: left;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; font-weight: 600; background-color: #FFFFFF; padding: 8% 0% 8% 25%; font-size:18px; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">Country</div>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <div style="font-family:Helvetica Light sans-serif; font-size:18px; color:rgba(102,102,102,1); padding: 3% 2% 3% 6%; position: relative; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                                ' . $contact_country . '
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="6">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td valign="top" style="text-align: center;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; text-align: center; font-weight: 800; padding: 20% 0%; font-size:18px; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">6</div>
                        </td>
                        <td>&nbsp;</td>
                        <td valign="top" style="text-align: left;">
                            <div style="font-family:Helvetica Light sans-serif; position: relative; font-weight: 600; background-color: #FFFFFF; padding: 8% 0% 8% 25%; font-size:18px; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">Message</div>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <div style="font-family:Helvetica Light sans-serif; font-size:18px; color:rgba(102,102,102,1); padding: 3% 2% 3% 6%; position: relative; background-color: #FFFFFF; border-radius: 0px; border-bottom: solid transparent; box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59) !important;">
                                ' . $comments . '                           
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </div>
            <div style="color: #714A1B; background: url(http://www.freudenbergleisure.com/assets/img/email_randholee_footer.jpg) repeat; padding: 20px 0 20px 0; border-top: 3px solid #7d2c2c ; text-align: center;  box-shadow: 0px 0px 6px 0px rgba(74, 50, 39, 0.59);">
                <span>
                    <a href="http://www.freudenbergleisure.com/hotels/randholee/" style="color: #fff; line-height: 32px; font-size: 20px; font-family: futura-pt , sans-serif;" target="_blank"><img src="http://www.freudenbergleisure.com/assets/img/fsaleisure_site.png" width="250"/></a>
                </span>
            </div>
        </div>
    </body>
</html>';
            $mail_array = array('thilina@3cs.asia', $email, 'reservations@randholeeresorts.com', 'vikum@fsalk.com', 'foe@randholeeresorts.com', 'mgr.sales@freudenbergleisure.com');
//send mails to ******
            foreach ($mail_array as $to) {
                mailing::html_mail($to, $subject, $emailbody, 'reservations@randholeeresorts.com', $email);
            }
//echo $emailbody;
            $emailSent = true;
            if ($emailSent) {
// slack channel alert start
                $room = "client-website-forms";
                $data = "payload=" . json_encode(array(
                            "channel" => "#{$room}",
                            "text" => "
========================================\n
*Message from* http://www.freudenbergleisure.com/hotels/randholee/ *Contact us* \n
*Name :* {$name}\n
*Email :* {$email}\n
*Contact No :* {$contactno}\n
*Country :* {$contact_country}\n
*Subject :* {$subject}\n
*Inquiry :* {$comments}\n
-----------------------------------------------------------------\n",
                ));
                $ch = curl_init("https://hooks.slack.com/services/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");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);
// slack channel alert end
            }
            $success_msg = "Your inquiry was submitted successfully. We will contact you soon.";

            $errors = "";
            $err = "";
            $name = "";
            $contactno = "";
            $email = "";
            $comments = "";
            $subject = "";
            $address = "";
            $contact_country = "";
            $hotel_email = "";
            $hotel_name = "";
        }
    }
    ?>
    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <h1 class="hide-visual">Randholee Luxury Resort - Contact Us</h1>    
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->
        <div class="blur">  
            <div>
                <div id="main" role="main">
                    <div class="wrapper">
                        <article role="article">
                            <div class="ddd" style="position:relative; top:80px; float:right; width: 44%;"><img src="assets/images/cont-us.jpg" style="border: 8px solid #DB9C72; border-radius: 10px;"></div>
                            <div style="position:relative; top:110px;">
                                <?php if (isset($hasError)) { ?>
                                    <div class="form-item webform-component webform-component-errors" id="error_msg">
                                        <label>The following fields are missing or incorrect</label>
                                        <ul>
                                            <?php echo $errors; ?>
                                        </ul>
                                    </div>
                                <?php } if (isset($success_msg)) { ?>
                                    <div class="form-item webform-component webform-component-success" id="success_msg">
                                        <label><?php echo $success_msg; ?></label>
                                    </div>
                                <?php } ?>
                                <form class="webform-client-form webform-client-form-78" enctype="multipart/form-data" action="" method="post" id="webform-client-form-78" accept-charset="UTF-8">
                                    <div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--name">
                                            <label for="edit-submitted-name">Subject </label>
                                            <input type="text" id="edit-submitted-name" name="contact_subject" value="<?php echo $subject; ?>" size="60" maxlength="128" class="form-text">
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--name">
                                            <label for="edit-submitted-name">Name </label>
                                            <input type="text" id="edit-submitted-name" name="contact_name" value="<?php echo $name; ?>" size="60" maxlength="128" class="form-text">
                                        </div>
                                        <div class="form-item webform-component webform-component-email webform-component--email">
                                            <label for="edit-submitted-email">Email Address </label>
                                            <input class="form-text" type="contact_email" id="edit-submitted-email" name="contact_email" value="<?php echo $email; ?>" size="60">
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--email">
                                            <label for="edit-submitted-email">Contact Number </label>
                                            <input class="form-text" type="text" id="edit-submitted-email" name="contact_number" value="<?php echo $contactno; ?>" size="60">
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--email">
                                            <label for="edit-submitted-email">Country</label>
                                            <select name="countat_country" id="countat_country" class="form-text">
                                                <option value="">Select Your Country</option>
                                                <?php
                                                foreach ($countries as $country) {
                                                    echo '<option value="' . $country . '"';
                                                    if ($contact_country == $country) {
                                                        echo 'selected>' . $country . '</option>';
                                                    } else {
                                                        echo '>' . $country . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-item webform-component webform-component-textarea webform-component--comment---question">
                                            <label for="edit-submitted-comment-question">Message </label>
                                            <div class="form-textarea-wrapper resizable">
                                                <textarea id="edit-submitted-comment-question" name="contact_message" cols="60" rows="5" class="form-textarea"></textarea>
                                            </div>
                                        </div>
                                        <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                                        <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en"></script>
                                        <div class="form-actions">
                                            <input class="webform-submit button-primary form-submit" type="submit" name="contact_submit" value="Submit">
                                        </div>
                                    </div>
                                </form>
                            </div>      
                            <ul class="add_freud">
                                <li><strong>Freudenberg Leisure</strong></li>
                                <li>103/14,</li>
                                <li>Dharmapala Mawatha,</li>
                                <li>Colombo 7,</li>
                                <li>Sri Lanka.</li><br>
                                <li>Tel.: +94 (0) 11 2445282</li>
                                <li>Fax: +94 (0) 11 2440083</li>
                                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                            </ul>
                            <ul class="add_rand">
                                <li><strong>Randholee Resort and Spa</strong></li>
                                <li>Heerassagala Rd,</li>
                                <li>Bowalawatte,</li>
                                <li>Kandy,</li>
                                <li>Sri Lanka.</li><br>
                                <li>Tel.: + 94 81 2217741 – 3</li>
                                <li>Fax: + 94 81 2217744</li>
                                <li>E-mail:  reservations@randholeeresorts.com</li>
                            </ul>             
                            <div style="clear:both"></div>     
                            <link rel="stylesheet" href="assets/css/colorbox.css" />
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                            <script src="assets/js/jquery.colorbox.js"></script>
                            <script>
                                $(function ()
                                {
                                    $(".example6").colorbox({iframe: true, innerWidth: 800, innerHeight: 300});
                                })
                            </script>
                            <div id="contact_box">
                                <div class="qr_main" style="display:none;">
                                    <div class="qr_pic"><a href="map_pop.html" class="example6"><img src="assets/img/map.jpeg" width="63" height="63" /></a></div>
                                    <div class="qr_cnt">Google map</div>
                                </div>
                                <div class="qr_main" style="display:none;">
                                    <div class="qr_pic"><a href="assets/img/contt_qr.jpg" class="example6"><img src="assets/img/contact.jpeg" width="63" height="63" /></a></div>
                                    <div class="qr_cnt">Contact Us</div>
                                </div> 
                            </div>     
                            <div style="clear:both"></div>
                            <script src="http://maps.googleapis.com/maps/api/js"></script>
                            <script>
                                var myCenter = new google.maps.LatLng(7.263146, 80.616914);
                                function initialize()
                                {
                                    var mapProp = {
                                        center: myCenter,
                                        zoom: 18,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    };
                                    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
                                    var marker = new google.maps.Marker({
                                        position: myCenter,
                                    });
                                    marker.setMap(map);
                                }
                                google.maps.event.addDomListener(window, 'load', initialize);
                            </script>
                            <div id="googleMap"></div>      
                        </article>
                    </div><!--  .wrapper  -->   
                </div><!--  #main  -->
            </div><!--  #node-details  -->
            <br><br><br><br><br><br>
            <footer id="footer" role="contentinfo">  
                <?php /* ?>     <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div>
                  </div>
                  </aside><?php */ ?>
                <?php include 'trip-advisor.php'; ?>
                <?php include '../../includes/footer_randolee.php'; ?>  
                </body>
                </html>
