﻿<?php 
$pg = ['property' => 'freudenberg', 'page' => 'accommodation'];
include 'includes/header.php'; 
?> 

    <body>
        <header id="header" role="banner">   
            <?php include 'includes/navigation.php'; ?> 
        </header><!--  #header  -->

        <?php include 'includes/booking.php' ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include 'includes/slider.php' ?>
                </aside>  
                
                    <div id="route">
                        <breadcrumb class="menu">
                            <li><a href="index.php">Home</a></li>
                            <li><span class="arrow"> &gt; </span>Accommodation</li>
                        </breadcrumb>
                    </div>      
 

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper"> 
                            <div class="ctatext-text pad_top">
                                <?php require 'includes/showdescription.php'; ?>
                                <!--
                                <h1 class="hdr-seven">Rooms, Suites &amp; Private Villas</h1>          
                                <div class="hdr-two">Accommodation</div>          
                                <p style="text-align:justify; font-size:16px;">We offer delightful accommodation facilities in Colombo, Kandy and Nuwara Eliya. Contact us to arrange a round-trip package  if you want to stay in one or more cities during  your visit to Sri Lanka. Enjoy the rooms and suites at Ellen’s Place in Colombo, at the Randholee Resort or suites at Firs.</p>     
                                -->     
                                <!--<a href="#" class="btn-underline">Book Your Getaway</a>-->     
                            </div><!--  .ctatext-text  --> 
                        </div><!--  .ctatext-wrapper  -->



                        <div class="highlight-panels">
                            <?php require 'includes/shownavigation-1.php'; ?>
                        </div><!--  .highligh-panels  -->

                        <!--<aside role="complementary">
                                  <div class="ctatext-wrapper">
                            <div class="ctatext-text">            
                                          <h1 class="hdr-seven">Seasonal Room, Suite &amp; Villa Rates</h1>                   
                                          <div class="hdr-two">Your Perfect Getaway Awaits</div>                       
                                            <p style="text-align:justify; font-size:16px;">Guests may also take advantage of a number of in-room amenities and services including complimentary Wi-Fi internet access, personalised butler service, 24 hour in-room dining, as well as access to CD/DVD libraries.</p>                    
                                          <a href="#" class="btn-arrow">Book Now</a>                      
                                          </div>
                          </div>        
                                </aside> -->              
                    </article>           
                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>.
            <?php include 'trip-advisor.php'; ?>
            <footer id="footer" role="contentinfo">  

                <?php include 'includes/footer.php'; ?> 

                </body>
                </html>
