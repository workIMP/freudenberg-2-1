<div class="tab">
  <button class="tablinks active" onclick="openTab(event, 'Description')">Page Description</button>
  <button class="tablinks" onclick="openTab(event, 'ImgPanel')">Navigation Panel</button>
</div>

<div id="Description" class="tabcontent">
  <h1>Edit <?=$pagedata->page_name?> Page Description</h1>
  <form id="desc">
  	<table>
  		<tr>
  			<td><label for="subtitle">Sub Title</label></td>
  			<td><input type="text" name="subtitle" id="subtitle" value=""/></td>
  		</tr>
  		<tr>
   			<td><label for="maintitle">Main Title</label></td>
  			<td><input type="text" name="maintitle" id="maintitle" value=""/></td> 		
  		</tr>
  		<tr>
   			<td><label for="bodytext">Body Text</label></td>
  			<td><textarea name="bodytext" id="bodytext" rows="10" cols="75"></textarea></td> 		
  		</tr> 
  		<tr>
  			<td></td>
  			<td><button type="button" id="updbtn" name="updbtn">Update</button></td>
  		</tr> 		
  	</table>
  </form>
</div>

<div id="ImgPanel" class="tabcontent" style="height: 100%;">
  <h1>Manage <?=$pagedata->page_name?> Page Navigation</h1>
  <div class="gridcontainer" id="dropzone">
  	<div class="mainbox">
  		<form action="/file-upload" class="dropzone" id="mainimage-dropzone" style="height: 316px;"></form>
  	</div>
  	<div class="otherboxes">
  		<div class="smallbox" style="width: 33%; float: left; margin-left: 5px;">
  			<form action="/file-upload" class="dropzone" id="smallimage-dropzone-1"></form>
  		</div>
  		<div class="smallbox" style="width: 33%; float: left; margin-left: 5px;">
  			<form action="/file-upload" class="dropzone" id="smallimage-dropzone-2"></form>
  		</div>
  		<div class="smallbox" style="clear: both; width: 33%; float: left; margin-left: 5px;">
  			<form action="/file-upload" class="dropzone" id="smallimage-dropzone-3"></form>
  		</div>
  		<div class="smallbox" style="width: 33%; float: left; margin-left: 5px;">
  			<form action="/file-upload" class="dropzone" id="smallimage-dropzone-4"></form>
  		</div>
  	</div>
  </div>
</div>