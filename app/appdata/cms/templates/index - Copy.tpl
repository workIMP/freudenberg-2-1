<html>
<head>
	<title>Freudenberg Leisure CMS Login</title>
	<style type="text/css">
		.error{
			background: yellow;
			border: 1px solid red;
			padding: 10px;
		}
	</style>
</head>
<body>
	<h1>Freudenberg Leisure CMS Login</h1>
	<?php if($errors): ?>
	<div class="error">
		<ul>
		<?php foreach($errmsg as $err): ?>
			<li><?=$err?></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
	<form method="post" action="">
		<table>
			<tr>
				<td><label for="username">User Name</label></td>
				<td><input type="text" id="username" name="username" value="<?php echo (isset($_POST['username']) ? htmlspecialchars($_POST['username'], ENT_QUOTES) : '') ?>"/></td>
			</tr>
			<tr>
				<td><label for="password">Password</label></td>
				<td><input type="password" id="password" name="password" value=""/></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="submit" value="Login"/></td>
			</tr>
		</table>
	</form>
</body>
</html>