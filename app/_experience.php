﻿<?php 
$page = 'experience';
include 'includes/header.php'; 
?>

    <style>
        .promo_header{margin-top:20px;}
    </style>

    <body class="node-type-accommodation-list">

        <header id="header" role="banner">
            <?php include 'includes/navigation.php'; ?> 

        </header><!--  #header  -->
        <?php include 'includes/booking.php'; ?> 

        <div style="clear:both"></div>

        <div class="blur">  
            <div class="node--page_basic mode--full">    
                <aside role="complementary">
                    <header>
                    </header>    
                    <?php include 'includes/_slider.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Experiences</li>
                    </breadcrumb>
                </div>

                <div id="main" role="main">     
                    <article role="article" style="padding-top:10px;">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text pad_top">
                                <div class="hdr-two">Experiences</div>
                                <p style="text-align:justify; font-size:17px;">Our three indulgence way outs pack a potent dose in terms of unique experiences won’t be anywhere else while enjoying the best hospitality and world class service. When you are staying in the dense, high-rise city of Colombo at Ellen’s Place you can visit Royal Colombo Golf club for a round of golf or enjoy panoramic views and wonderful photo opportunities, shopping and nightlife with a joyful mix of cultures and cuisines. One good way to experience the splendor of Kandy is to stay at the Randholee Resort, a great entry point to view Kandy Perehera, the Temple of the Tooth Relic, the Royal Botanical Gardens or the Hantane Mountain Range. Lastly, for a totally different experience to your urban adventure you must stay at Firs Bungalow in Nuwara Eliya to use the Victoria Golf Course, go horse riding, visit the Hakgala Botanical Gardens, hike on Horton Plains or take in the breathtaking views of World’s End.</p>        
                            </div><!--  .ctatext-wrapper  -->
                        </div><!--  .ctatext-text  -->


                        <div class="activities-list highlight-panels">
                            <h1 class="hdr-seven promo_header">DESTINATIONS AT COLOMBO:</h1>
                            <ul>

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/galle.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">The Galle Face Green</h2>
                                            <div class="twoo fadeitem">An urban park facing the glorious Indian Ocean</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/Independence_Arcade.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Independence Arcade</h2>
                                            <div class="twoo fadeitem">The ideal place for shopping, fun and food in the heart of Colombo</div>
                                            <div class="hdr-two fadeitem"></div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/gangaramaya.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">The Gangaramaya Temple</h2>
                                            <div class="twoo fadeitem">Witness the Gangaramaya, a temple with unique architecture and beautiful surroundings in the heart of Colombo</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/Independance-Square.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Independence Memorial Hall</h2>
                                            <div class="twoo fadeitem">A beautiful monument commemorating Sri Lanka’s independence</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/Viharamahadevi_Park.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Viharamahadevi Park</h2>
                                            <div class="twoo fadeitem">A public park located a few minutes away from the Sri Lanka National Museum</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/golf.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Colombo Golf Club</h2>
                                            <div class="twoo fadeitem">Play a game of Golf at the Colombo golf club</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->
                            </ul>

                            <div style="clear:both"></div>     
                            <ul>
                                <h1 class="hdr-seven promo_header">DESTINATIONS AT KANDY:</h1>

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/perahera.jpg') no-repeat 50% 50%; background-size: cover;"> 
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Esala Perahera</h2>     
                                            <div class="twoo fadeitem">Witness a majestic elephant pageant</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  --> 

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/temple.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>       
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Temple of the Tooth Relic</h2>
                                            <div class="twoo fadeitem">The Sri Dalada Maligawa houses the tooth relic of Lord Buddha</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  --> 

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/botanical-gardens-peradeniya.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Peradeniya Botanical Garden</h2>
                                            <div class="twoo fadeitem">A spectacular botanical garden with a variety trees and flowers</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  --> 

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/Pinnawela.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Pinnawala Elephant Orphange</h2>
                                            <div class="twoo fadeitem">Learn all about the lives of the World’s largest land animals</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/kandy-lake.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Kandy Lake</h2>
                                            <div class="twoo fadeitem">A beautiful lake in the vibrant and exotic locale of Kandy</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  --> 

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/hantana.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Hantana</h2>
                                            <div class="twoo fadeitem">Hantana, the scenic paradise of the Sri Lankan hill country awaits you</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  --> 
                            </ul>

                            <div style="clear:both"></div>
                            <h1 class="hdr-seven promo_header">DESTINATIONS AT NUWARA ELIYA:</h1>
                            <ul>
                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/victoriya.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>       
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Queen Victoria Park</h2>
                                            <div class="twoo fadeitem">Named after Queen Victoria to commemorate her diamond jubilee. A gorgeous public park in Nuwara Eliya</div>
                                            <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/gregory.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Gregory Lake</h2>
                                            <div class="twoo fadeitem">A lake surrounded by breathtaking mountain ranges</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/hortan.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Horton Plains National Park</h2>
                                            <div class="twoo fadeitem">Witness a stunning 880m plunge, commonly referred to as “the world’s end”</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/hakgala.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Botanical Garden Hakgala</h2>
                                            <div class="twoo fadeitem">A temperate wonderland that flaunts a variety of plant species</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  --> 

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/golf_n.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Golf Club Nuwara Eliya</h2>
                                            <div class="twoo fadeitem">Play an enthralling and entertaining game of golf in Nuwara Eliya</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  --> 

                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/Maskeliya.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Maskeliya</h2>
                                            <div class="twoo fadeitem">Maskeliya Reservoir from Sri Pada</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  --> 


                                <li class="highlight" style="background: #ebebeb url('assets/images/experiences/stclairs.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="#"></a>
                                    <div class="highlight-background"></div><!--  .highlight-background  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">St. Clair's Waterfall</h2>
                                            <div class="twoo fadeitem">A site that is alluring, jaw-dropping and spectacular</div>
                                        </div><!--  .highlight-content-inside  -->
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  --> 

                            </ul>
                        </div><!--  .highlight-panels  -->
                    </article>
                </div><!--  #main  -->
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <?php include 'trip-advisor.php'; ?>         

            <?php /* ?>    <aside role="complementary">
              <div class="ctatext-buildadventure ctatext-wrapper">
              <div class="ctatext-text">
              <h1 class="hdr-seven">Build your own Adventure</h1>
              <div class="hdr-two">Your Perfect Getaway Awaits</div>
              <p>Facilities guaranteed to kindle pure relaxation and bliss, Whatever your lifestyle or pace, Randholee offers something unique for everyone.</p>
              <a class="btn-arrow" href="#">Book Now</a>
              </div><!--  .ctatext-text  -->
              </div><!--  .ctatext-wrapper  -->
              </aside><?php */ ?>

            <footer id="footer" role="contentinfo"> 
                <?php include 'includes/footer.php'; ?> 

                </body>
                </html>
