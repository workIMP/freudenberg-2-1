<?php
class Generic {

/**
 * Removes all html contents from given string
 */
	public static function noHTML($input, $encoding = 'UTF-8')
	{
	    return htmlentities($input, ENT_QUOTES | ENT_HTML5, $encoding);
	}

/**
 * Validate CSRF token
**/
	public static function validCSRF($origin, $key)
	{
		if(isset($_SESSION) && isset($_SESSION['csrf_'.$key]) && $_SESSION['csrf_'.$key] == $origin[$key]){
			return true;
		}
		return false;
	}

	public static function generateCSRFToken($key)
	{
		if(!isset($_SESSION)){
			session_start();
		}

		$token = self::randomString(30);
		$_SESSION['csrf_'.$key] = $token;
		
		return $token;
	}

/**
* Generate a random string value
**/
	public static function randomString($length = 6)
	{
	    $str = "";
	    $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	    $max = count($characters) - 1;
	    for ($i = 0; $i < $length; $i++) {
	        $rand = mt_rand(0, $max);
	        $str .= $characters[$rand];
	    }
	    return $str;
	}

/**
 * Parse a template with real data
 * @param $template string
 * @param $data array
 * @return string Parsed template
 */
	public static function parseTemplate($template, $data)
	{
		foreach ($data as $key => $value) {
			$template = str_replace('{{'.$key.'}}', $value, $template);
		}

		return $template;
	}

	/**
 * Function to detect SSL requests
 * @return boolean ssl status
 */
	public static function is_ssl() {
	    
	    if ( isset($_SERVER['HTTPS']) ) {
	        if ( 'on' == strtolower($_SERVER['HTTPS']) )
	            return true;
	        if ( '1' == $_SERVER['HTTPS'] )
	            return true;
	    } 
	    elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
	        return true;
	    }
	    elseif(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && 'https' == $_SERVER['HTTP_X_FORWARDED_PROTO']){
	        return true;
	    }

	    return false;
	}

}