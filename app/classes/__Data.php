<?php

class Data {

	private $db;
	private $propertyData;
	private $pageData;

	public function __construct($dbconn, $pg)
	{
		$this->db = $dbconn;
		$this->propertyData = $this->db->getRow('SELECT * FROM tblproperties WHERE `property_slug` = ?', array($pg['property']));
		$this->pageData = $this->db->getRow('SELECT * FROM tblpages WHERE `property_id` = ? AND `page_slug` = ?', array($this->propertyData->id, $pg['page']));	
	}
	
	public function isValidPage()
	{
		return (isset($this->pageData->id)) ? true : false;
	}	

	public function getDescription()
	{
		// check for page description
		if($this->pageData->section_desc)
		{
			return $this->db->getRow('SELECT * FROM tbldescriptions WHERE `page_id` = ?', array($this->pageData->id));
		}
	}

	public function getNavigation()
	{
		// check for navigation section
		if($this->pageData->section_nav)
		{
			return $this->db->getRows('SELECT tblnavigation.*, tblproperties.property_name FROM tblnavigation INNER JOIN tblpages ON tblnavigation.target_pg = tblpages.id INNER JOIN tblproperties ON tblpages.property_id = tblproperties.id WHERE tblnavigation.page_id = ? ORDER BY tblnavigation.id', array($this->pageData->id));
		}		
	}

	public function getSlider($pid = 0, $override = false)
	{
		// check for slider
		$getslider = false;

		if(!$override)
		{
			// this will execute by default
			if($this->pageData->section_slider <> 0)
			{
				$getslider = true;
			}			
		}
		else
		{
			// this is an override. force $getslider to true. 
			$getslider = true;
		}

		$pageid = ($pid <> 0 ? $pid : $this->pageData->id);

		/*
		switch($this->propertyData->id)
		{
			case 1:
				if(in_array($this->pageData->page_slug, array('home','experience','accommodation','aboutus','tandc','pp','downloads')))
				{
					$pageid = 27;
					$getslider = true;
				}

			break;


			case 2:
			break;


			case 3:
			break;


			case 4:
			break;
		}*/


		if($getslider)
		{
			return $this->db->getRows('SELECT * FROM tblslides WHERE `page_id` = ? AND `section_id` = ?', array($pageid, 'slider'));
		}		
	}

	/* 
	 * For getting inner page slider images
	 */
	public function getGallery()
	{
		// check for gallery
		if($this->pageData->section_gallery)
		{
			return $this->db->getRows('SELECT * FROM tblslides WHERE `page_id` = ? AND `section_id` = ?', array($this->pageData->id, 'gallery'));
		}		
	}

	public function getExperiences()
	{
		return $this->db->getRows('SELECT * FROM tblexperiences ORDER BY `destination_id` ASC', array());
	}


	public function getProperties($propid = 0)
	{	
		if($propid == 0)
		{
			return $this->db->getRows('SELECT * FROM tbl_home_property_data', array());
		}
		else
		{
			return $this->db->getRow('SELECT * FROM tbl_home_property_data WHERE id = ?', array($propid));
		}
	}

	public function getFreudenbergProperties()
	{
		return $this->db->getRows('SELECT * FROM tblproperties ORDER BY id ASC');
	}

	public function getPromotions($prop)
	{
		return $this->db->getRows('SELECT tbl1.*, tbl2.property_slug FROM tblpromotions tbl1 INNER JOIN tblproperties tbl2 ON tbl1.property_id = tbl2.id WHERE tbl1.property_id = ? AND tbl1.enabled = 1', array($prop));
	}

	public function getPromoPage($promoid)
	{
		$res = $this->db->getRow('SELECT page_slug FROM tblpages INNER JOIN tblpromotionpages ON tblpages.id = tblpromotionpages.page_id WHERE tblpromotionpages.promotion_id = ?', array($promoid));
		return $res->page_slug;
	}

	public function getPromoPopupImage($promoid)
	{
		$res = $this->db->getRow('SELECT image_src FROM tblpromotionimages WHERE promotion_id = ?', array($promoid));
		return $res->image_src;
	}		

	public function getDownloads($propid = 0)
	{
		if($propid == 0)
		{
			// get all download items
			return $this->db->getRows('SELECT tbldownloaditems.*, tblproperties.property_name FROM tbldownloaditems INNER JOIN tblproperties ON tbldownloaditems.property_id = tblproperties.id ORDER BY property_id ASC', array());
		}
		else
		{
			return $this->db->getRows('SELECT tbldownloaditems.*, tblproperties.property_name FROM tbldownloaditems INNER JOIN tblproperties ON tbldownloaditems.property_id = tblproperties.id WHERE tbldownloaditems.property_id = ? ORDER BY tbldownloaditems.property_id ASC', array($propid));
		}		
	}


	public function getPageUrl($pageid, $section = '')
	{
		$data = $this->db->getRow('SELECT tblpages.*, tblproperties.property_slug FROM tblpages INNER JOIN tblproperties ON tblpages.property_id = tblproperties.id WHERE tblpages.id = ?', array($pageid));
		
		switch($section)
		{
			case 'facilities':
			return 'facilities/'.$data->page_slug;
			break;

			case 'accommodation':
			return 'accommodation/'.$data->page_slug;
			break;

			case 'cuisine':
			return 'cuisine/'.$data->page_slug;
			break;

			default:
			return $data->page_slug;
			break;
		}

		
	}


	public function getPropertyMapCoordinates()
	{
		return $this->db->getRows('SELECT property_map_icon, property_address, property_map_longitude, property_map_latitude FROM tblproperties WHERE `property_map_enabled` = ?', array(1));
	}	

	public function hasNav()
	{
		return $this->pageData->section_nav;
	}

	public function hasSlider()
	{
		return ($this->pageData->section_slider == 0 ? false : true);
	}

	// this is for handling default sliders
	public function getSliderFlag()
	{
		return $this->pageData->section_slider;
	}

	public function hasGallery()
	{

		return $this->pageData->section_gallery;
	}

	public function getPropertyID()
	{
		return $this->propertyData->id;
	}

	public function getPropertyGallery()
	{
		return $this->db->getRows('SELECT * FROM tblslides WHERE `property_id` = ? AND `section_id` = ?', array($this->propertyData->id, 'gallery'));
	}


	public function getPropertyDetails($prop)
	{
		return $this->db->getRow('SELECT * FROM tblproperties WHERE `id`=?', array($prop));
	}


}
