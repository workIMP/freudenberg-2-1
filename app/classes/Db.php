<?php
class Db {

	private $db;
	private $charset = 'utf8';
	private $port = 3306;
	private $driver = 'mysql';
	private $fetchmode = \PDO::FETCH_OBJ;
	private $error = false;
        private $query = '';

	//private $logger;

	public function __construct($config)
	{

		//$this->logger = $c->get('logger');

		$dsn = $this->driver.':host='.$config['host'].';dbname='.$config['database'].';charset='.$this->charset.';port='.$this->port;


		$this->db = new \PDO($dsn,'webcruzs_freudenberg','freudenberg@123');
		// echo "string";die();

		$this->db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, $this->fetchmode);
		
	}

	protected function _prepare($query, $data)
	{
		$statement = $this->db->prepare($query);

		/** bind parameters */
		$paramcounter = 1;
		
		if(count($data) > 0)
		{
			foreach($data as $val)
			{
				$statement->bindValue($paramcounter, $val);
				$paramcounter++;
			}			
		}

                
		return $statement;
	}	


	public function getRows($query, $data = array())
	{
		$statement = $this->_prepare($query, $data);

		$res = $statement->execute();

		if($res)
		{
			// return a row
			return $statement->fetchAll();
		}
		else
		{
			return false;
		}

	}

	public function getRow($query, $data = array())
	{

		$statement = $this->_prepare($query, $data);

		$res = $statement->execute();

		if($res)
		{
			// return a row
			return $statement->fetch();
		}
		else
		{
			return false;
		}

	}


	public function executeQuery($query, $data = array(), $type)
	{
		$statement = $this->_prepare($query, $data);

		$res = $statement->execute();
                //$this->query = $statement->debugDumpParams();

		if($res === false)
		{
			$this->error = true;
			return false;
		}
		else
		{
			if($type == 'insert')
			{
				/** Since this is an insert we only need to know whether it was suceeded or not */

					// return lastinsertid
					return $this->db->lastInsertId(); 
					
			}
			else if($type == 'update')
			{

					return $statement->rowCount();
							
			}			
		}

	}	


	public function addRecord($query, $data = array())
	{
		$statement = $this->_prepare($query, $data);

		$res = $statement->execute();
                //$this->query = $statement->debugDumpParams();

			/** Since this is an insert we only need to know whether it was suceeded or not */
			if($res === false)
			{
				$this->error = true;
				return false;
			}
			else
			{
				// return lastinsertid
				return $this->db->lastInsertId(); 
			}
	}


	public function updateRecord($query, $data)
	{
		$statement = $this->_prepare($query, $data);

		$res = $statement->execute();

			/** Since this is an update we only need to know whether it was suceeded or not */
			if($res === false)
			{
				$this->error = true;
				return false;
			}
			else
			{
				// return no. of affected rows
				return $statement->rowCount(); 
			}
	}

	public function deleteRecord($query, $data = array())
	{
		$statement = $this->_prepare($query, $data);

		$res = $statement->execute();

		if($res === false)
		{
			$this->error = true;
			return false;
		}
		else
		{
			return $statement->rowCount();
		}
	}	


	public function getError()
	{
		return $this->db->errorInfo();			// returns an array 3 elements 
	}

	public function getDbInstance()
	{
		return $this->db;
	}

        public function getQuery()
        {
           return $this->query;
        }

}