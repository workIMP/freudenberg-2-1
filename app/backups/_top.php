<?php

ini_set('display_errors', 'Off');
session_start();

require_once('config/config.php');

require_once('classes/ReCaptcha/autoload.php');

date_default_timezone_set("Asia/Colombo");
