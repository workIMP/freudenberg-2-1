﻿﻿<!DOCTYPE html>

<html class="no-js">

    <?php include 'includes/header.php'; ?> 
    <style>
        .ctacard-wrapper {clear: both;overflow: hidden;padding-top: 0px !important;padding-bottom: 0px !important;}
        .ctacard-wrapper .ctacard-card:nth-child(2) {margin-left: 0% !important; margin-right: 0% !important; }
        .ctacard-wrapper .ctacard-card:nth-child(3) {margin-left: 5%; margin-right: 5% }
        #route{margin-left:21.4%}	




        #fade{
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index:+111111;

            opacity:.80;
            filter: alpha(opacity=70);
        }
        #light{

            display: none;
            margin:0 auto;
            width: 50%;



        }

        .promo{
            position:fixed;

            z-index:+11111111;
            top: 75px;
            overflow:visible;
            -moz-animation: fadein 2s; /* Firefox */
            -webkit-animation: fadein 2s; /* Safari and Chrome */
            -o-animation: fadein 2s; /* Opera */
        }
        @keyframes fadein {
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-moz-keyframes fadein { /* Firefox */
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-webkit-keyframes fadein { /* Safari and Chrome */
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-o-keyframes fadein { /* Opera */
            from {
                opacity:0;
            }
            to {
                opacity: 1;
            }
        }
   


.tabs nav a {
    
    font-size: 15px !important;}

  

        .clo{
            position:fixed;
            z-index:1003;
            margin-top: 4%;
            margin-left: 33%;
            width: initial;

        }
.tabs nav #freuden.tab-current {
    background-color: rgb(231, 240, 246);
    border-top: solid 3px #1a97eb;
    border-left: solid 1px #388bc2;
    border-right: solid 1px #519bcd;
}
.tabs nav li.tab-current a {
    color: #171616;
}
.tabs nav #freuden.tab-current:before, .tabs nav #freuden.tab-current:after {
    background: #1a97eb;
}
@media screen 
  and (device-width: 360px) 
  and (device-height: 640px) 
  and (-webkit-device-pixel-ratio: 3) {



.tabs nav ul, .tabs nav ul li a {
    
    font-size: 7px !important;
}
.grid figure, .grid1 figure, .grid2 figure {
  
    margin: 5px 17% !important;
    min-width: 232px !important;
   
}
.tabs {
   
    margin-top: 240px !important;
}
.booking_room {
   
    top: 10.5em !important;
   
}
}

@media only screen 
  and (min-device-width: 320px) 
  and (max-device-width: 340px)
  and (-webkit-min-device-pixel-ratio: 2)
  and (orientation: portrait) {


.tabs nav a {
   
    font-size: 6px !important;}

    .grid figure, .grid1 figure, .grid2 figure {
  
    margin: 5px 11% ;
    min-width: 232px !important;
   
}
.tabs {
   
    margin-top: 235px !important;
}
.booking_room {
   
    top: 9.9em ;
   
}
}
@media only screen 
  and (min-device-width: 375px) 
  and (max-device-width: 667px) 
  and (-webkit-min-device-pixel-ratio: 2) { 
    .tabs nav a {
   
    font-size: 7px !important;}

    .grid figure, .grid1 figure, .grid2 figure {
  
    margin: 5px 19% !important;
    min-width: 232px !important;
   
}
.tabs {
   
    margin-top: 240px !important;
}
}
@media only screen 
  and (min-device-width: 768px) 
  and (max-device-width: 1024px) 
  and (-webkit-min-device-pixel-ratio: 1) {

    .tabs nav a {
    
   
    font-size: 12px !important;
    line-height: 2em !important;
    padding: 0px 1em !important;
    
}
.tabs {
   
    margin-top: 240px !important;
}
  }

@media only screen 
  and (min-device-width: 375px) 
  and (max-device-width: 667px) 
  and (-webkit-min-device-pixel-ratio: 2) { 
.booking_room {
    
    top: 11em;
   
}

}
@media only screen 
  and (min-device-width: 414px) 
  and (max-device-width: 736px) 
  and (-webkit-min-device-pixel-ratio: 3) {
 .booking_room {
    
    top: 12em;
   
}

}




    </style>

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">

            <h1 class="hide-visual">Freudenberg Leisure</h1>  
            <?php include 'includes/navigation.php'; ?> 

        </header><!--  #header  -->

        <?php include 'includes/booking.php'; ?> 

        <div class="blur">  

            <div class="node--page_basic mode--full">  
                <aside role="complementary">
                    <?php include 'includes/slider.php'; ?>
                </aside>    

                <div id="route" style="margin-left: 21.4% !important;">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Special Offers</li>
                    </breadcrumb>
                </div>

                <aside role="complementary" id="main">

                    <div id="tabs" class="tabs">
                        <nav>
                            <ul style="list-style:none;">
                                <li id="freuden"><a href="#section-4">FREUDENBERG LEISURE<br/>PROMOTIONS</a></li>
                                <li id="randh2"><a href="#section-1">RANDHOLEE<br/>PROMOTIONS</a></li>
                                <li id="firs2"><a href="#section-2">THE FIRS<br/>PROMOTIONS</a></li>
                                <li id="ellens2"><a href="#section-3">ELLEN'S PLACE<br/>PROMOTIONS</a></li>
                            </ul>
                        </nav>

<!--====================FREUDENBERG================-->
                        <div class="content" style="background-color: rgba(206, 224, 237, 0.5);">  
                            <section id="section-4">
                                <div class="mediabox">
                                    <div class="grid">
                                        <div class="hdr-two hide ddd" style="text-align: left; padding: 10px; font-size:1.2em;">FREUDENBERG PROMOTIONS</div>
                                        <!--   <figure class="effect-oscar">
                                               <img src="assets/images/promotion/randholee/lastmin_promo.jpg" alt="img09"/>
                                               <figcaption>
                                                   <h2>LAST MINUTE <span>HOT DEAL</span></h2>
                                                   <p>Calling all last minute decision makers! Book your holiday with us within 7 days of your arrival and enjoy an astonishing 40% discount!</p>
                                                   <a href="#">View more</a>
                                               </figcaption>      
                                           </figure>
                                           <figure class="effect-oscar">
                                               <img src="assets/images/promotion/randholee/honeymoon_promo.jpg" alt="img10"/>
                                               <figcaption>
                                                   <h2>Honeymoon at <span>Randholee</span></h2>
                                                   <p>Wrapped in a pleasant climate, surrounded by the beauty of nature and pampered like royalty, have the most memorable honeymoon.</p>
                                                   <a href="#">View more</a>
                                               </figcaption>      
                                           </figure>
                                           <figure class="effect-oscar">
                                               <img src="assets/images/promotion/randholee/resort_promo.jpg" alt="img10"/>
                                               <figcaption>
                                                   <h2>RANDHOLEE RESORTS <span>ADVANCE PURCHASE</span></h2>
                                                   <p>RANDHOLEE Resort’s Advance Purchase rate offers savings of 20% for bookings made 30 days in advance, a relaxing vacation</p>
                                                   <a href="#">View more</a>
                                               </figcaption>      
                                           </figure>
                                           <figure class="effect-oscar">
                                               <img src="assets/images/promotion/randholee/stay49t_promo.jpg" alt="img10"/>
                                               <figcaption>
                                                   <h2>STAY 4 NIGHTS AND GET THE <span>ONE NIGHT FREE!</span></h2>
                                                   <p>Book 04 consecutive nights at Randholee Resorts. Receive our best available rate and your fourth night free!</p>
                                                   <a href="#">View more</a>
                                               </figcaption>      
                                           </figure>-->
                                        <figure class="effect-oscar">
                                            <img src="assets/images/beurgen.jpg" alt="img10"/>
                                            <figcaption>
                                                <h2>FREUDENBERG LEISURE<BR><span>PRE-CHRISTMAS SALE</span></h2>
                                                <p>6 DAYS & 5 NIGHTS <br>IN SRI LANKA FOR £650<BR>(TWIN SHARING)<BR></p>
                                                <a href="freudenberg-promotion.php">View more</a>                      <!--onclick="lightbox_open();"-->
                                            </figcaption>     
                                        </figure>  

                                        <div id="light">

                                            <a href="freudenberg-promotion.php"></a> 
                                        </div>
                                        <div id="fade" onClick="lightbox_close();">
                                            <div class="clo" >  <a  onClick="lightbox_close();">  <></a></div>

                                        </div> 


                                    </div> 
                                </div>
                            </section>
                        </div>

                        <div class="content" style="background-color: rgba(253, 229, 229, 0.5);">  
                            <section id="section-1">
                                <div class="mediabox">
                                    <div class="grid">
                                        <div class="hdr-two hide ddd" style="text-align: left; padding: 10px; font-size:1.2em;">RANDHOLEE PROMOTIONS</div>
                                        <!--   <figure class="effect-oscar">
                                               <img src="assets/images/promotion/randholee/lastmin_promo.jpg" alt="img09"/>
                                               <figcaption>
                                                   <h2>LAST MINUTE <span>HOT DEAL</span></h2>
                                                   <p>Calling all last minute decision makers! Book your holiday with us within 7 days of your arrival and enjoy an astonishing 40% discount!</p>
                                                   <a href="#">View more</a>
                                               </figcaption>			
                                           </figure>
                                           <figure class="effect-oscar">
                                               <img src="assets/images/promotion/randholee/honeymoon_promo.jpg" alt="img10"/>
                                               <figcaption>
                                                   <h2>Honeymoon at <span>Randholee</span></h2>
                                                   <p>Wrapped in a pleasant climate, surrounded by the beauty of nature and pampered like royalty, have the most memorable honeymoon.</p>
                                                   <a href="#">View more</a>
                                               </figcaption>			
                                           </figure>
                                           <figure class="effect-oscar">
                                               <img src="assets/images/promotion/randholee/resort_promo.jpg" alt="img10"/>
                                               <figcaption>
                                                   <h2>RANDHOLEE RESORTS <span>ADVANCE PURCHASE</span></h2>
                                                   <p>RANDHOLEE Resort’s Advance Purchase rate offers savings of 20% for bookings made 30 days in advance, a relaxing vacation</p>
                                                   <a href="#">View more</a>
                                               </figcaption>			
                                           </figure>
                                           <figure class="effect-oscar">
                                               <img src="assets/images/promotion/randholee/stay49t_promo.jpg" alt="img10"/>
                                               <figcaption>
                                                   <h2>STAY 4 NIGHTS AND GET THE <span>ONE NIGHT FREE!</span></h2>
                                                   <p>Book 04 consecutive nights at Randholee Resorts. Receive our best available rate and your fourth night free!</p>
                                                   <a href="#">View more</a>
                                               </figcaption>			
                                           </figure>-->
                                        <figure class="effect-oscar">
                                            <img src="hotels/randholee/assets/images/royal_promo.jpg" alt="img10"/>
                                            <figcaption>
                                                <h2>RANDHOLEE RESORTS <br><span>The Royal Escape</span></h2>
                                                <p>Two Nights For Rs.36,930/= </p>
                                                <a  onclick="lightbox_open();">View more</a>
                                            </figcaption>			
                                        </figure>  

                                        <div id="light">

                                            <img style=" margin: 0 auto;width: 30%;" class="promo" src="hotels/randholee/assets/images/promo1.png"> 
                                        </div>
                                        <div id="fade" onClick="lightbox_close();">
                                            <div class="clo" >	<a  onClick="lightbox_close();">	<img class="clo" src="hotels/randholee/assets/images/close.png"  alt="close"/></a></div>

                                        </div> 


                                    </div> 
                                </div>
                            </section>
                        </div>


                        <div class="content" style="background-color: rgba(240, 249, 215, 0.5);">  
                            <section id="section-2">
                                <div class="mediabox">     
                                    <div class="grid1">
                                        <div class="hdr-two hide ddd" style="text-align: left; padding: 10px; font-size:1.2em;">THE FIRS PROMOTIONS</div>
                                        <div style="clear:both"></div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="hdr-two" style="font-style: italic; margin-bottom: 15px;">  No promotions are currently available. </div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <!--  <figure class="effect-oscar">
                                              <img src="assets/images/promotion/firs/early_promo.jpg" alt="img10"/>
                                              <figcaption>
                                                  <h2>SAVE 20% WITH ADVANCE <span>PURCHASE DISCOUNT</span></h2>
                                                  <p>Time to think ahead! Make your booking 30 days in advance and enjoy 20% off on any room or meal plan</p>
                                                  <a href="#">View more</a>
                                              </figcaption>			
                                          </figure>
                                          
                                          <figure class="effect-oscar">
                                              <img src="assets/images/promotion/firs/longstay_promo.jpg" alt="img10"/>
                                              <figcaption>
                                                  <h2>LONG STAY OFFER ON OCTOBER TO FEBRUARY - <span>SUITE ROOMS</span></h2>
                                                  <p>GET THE 4th NIGHT FREE Suite rooms on Room Only / Bed & Breakfast plans</p>
                                                  <a href="#">View more</a>
                                              </figcaption>			
                                          </figure>
                                          
                                          <figure class="effect-oscar">
                                              <img src="assets/images/promotion/firs/lastmin_promo.jpg" alt="img10"/>
                                              <figcaption>
                                                  <h2>40% LAST MINUTE DISCOUNT - <span>SUITE ROOMS</span></h2>
                                                  <p>GET THE 4th NIGHT FREE Suite rooms on Room Only / Bed & Breakfast plans</p>
                                                  <a href="#">View more</a>
                                              </figcaption>			
                                          </figure> -->
                                    </div> 
                                </div>
                            </section>
                        </div> 


                        <div class="content" style="background-color: rgba(223, 239, 255, 0.5);">  
                            <section id="section-3">
                                <div class="mediabox">
                                    <div class="grid2">
                                        <div class="hdr-two hide ddd" style="text-align: left; padding: 10px; font-size:1.2em;">ELLEN'S PLACE PROMOTIONS</div>
                                        <div style="clear:both"></div>

                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="hdr-two" style="font-style: italic; margin-bottom: 15px;">  No promotions are currently available. </div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>

                                        <!-- <figure class="effect-oscar">
                                             <img src="assets/images/promotion/ellens/lastmin_promo.jpg" alt="img10"/>
                                             <figcaption>
                                                 <h2>LAST MINUTE DISCOUNT - <span>DELUXE & STANDARD ROOMS</span></h2>
                                                 <p>Enjoy our last minute offers with amazing discounts for travel in the next four days!</p>
                                                 <a href="#">View more</a>
                                             </figcaption>			
                                         </figure>
                                         
                                         <figure class="effect-oscar">
                                             <img src="assets/images/promotion/ellens/longstay_promo.jpg" alt="img10"/>
                                             <figcaption>
                                                 <h2>15% OFF LAST MINUTE DISCOUNT - <span>SUITE ROOMS</span></h2>
                                                 <p>15% discount for last minute reservations made within 7 days of arrival on Room Only / Bed & Breakfast  plans</p>
                                                 <a href="#">View more</a>
                                             </figcaption>			
                                         </figure>
                                         
                                         <figure class="effect-oscar">
                                             <img src="hotels/ellens/assets/images/stay49t_promo.jpg" alt="img10"/>
                                             <figcaption>
                                                 <h2>STAY 4 NIGHTS AND GET THE <span>ONE NIGHT FREE </span></h2>
                                                 <p>Book 04 consecutive nights at Ellen's place. Receive our best available rate and your fourth night free!</p>
                                                 <a href="#">View more</a>
                                             </figcaption>			
                                         </figure>
                                        -->






                                    </div> 
                                </div>
                            </section>
                        </div>              


                        <?php /* ?><div class="ctacard-wrapper">
                          <h1 class="hdr-seven promo_header">ELLEN'S PLACE PROMOTIONS</h1>

                          <div class="ctacard-card equal-height ch-item">
                          <div class="inner-ratio ch-info">
                          <div class="ch-info-front ch-img-8"></div>
                          <div class="ch-info-back"><h3>Hurry Up!!!</h3><p>35% LAST MINUTE DISCOUNT  - DELUXE & STANDARD ROOMS <a href="#">View Package</a></p></div>
                          </div><!--  .inner-ratio  -->
                          <div class="ctacard-text">
                          <h2 class="hdr-four">35% OFF LAST MINUTE DISCOUNT - DELUXE & STANDARD ROOMS</h2>
                          <p>Enjoy our last minute offers with amazing discounts for travel in the next four days!</p>
                          </div><!--  .ctacard-text  -->
                          <div class="ctacard-button">
                          <a href="#" class="btn-arrow">More Details</a>  </div><!--  .ctacard-button  -->
                          </div><!--  .ctacard-card  -->


                          <div class="ctacard-card equal-height ch-item">
                          <div class="inner-ratio ch-info">
                          <div class="ch-info-front ch-img-9"></div>
                          <div class="ch-info-back"><h3>Hurry Up!!!</h3><p>15 % LAST MINUTE DISCOUNT SEPTEMBER / OCTOBER / NOVEMBER / DECEMBER - SUITE ROOMS<a href="#">View Package</a></p></div>
                          </div><!--  .inner-ratio  -->
                          <div class="ctacard-text">
                          <h2 class="hdr-four">15% OFF LAST MINUTE DISCOUNT - SUITE ROOMS</h2>
                          <p>Enjoy our last minute offers with amazing discounts for travel in the next seven days!</p>
                          </div><!--  .ctacard-text  -->
                          <div class="ctacard-button">
                          <a href="#" class="btn-arrow">More Details</a>  </div><!--  .ctacard-button  -->
                          </div><!--  .ctacard-card  -->


                          <div class="ctacard-card equal-height ch-item">
                          <div class="inner-ratio ch-info">
                          <div class="ch-info-front ch-img-10"></div>
                          <div class="ch-info-back"><h3>Hurry Up!!!</h3><p>LONG STAY OFFER ON SEPTEMBER / OCTOBER / NOVEMBER / DECEMBER  - SUITE ROOMS<a href="#">View Package</a></p></div>
                          </div><!--  .inner-ratio  -->
                          <div class="ctacard-text">
                          <h2 class="hdr-four">STAY 4 NIGHTS AND GET THE ONE NIGHT FREE!</h2>
                          <p>Book 04 consecutive nights at Ellen's place. Receive our best available rate and your fourth night free!</p>
                          </div><!--  .ctacard-text  -->
                          <div class="ctacard-button">
                          <a href="#" class="btn-arrow">More Details</a>  </div><!--  .ctacard-button  -->
                          </div><!--  .ctacard-card  -->
                          </div><?php */ ?><!--  .ctacard-wrapper  -->
                </aside>    

            </div>  

            <footer id="footer" role="contentinfo"> 

                <?php include 'trip-advisor.php'; ?>
                <?php /* ?>    <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>

                <?php include 'includes/footer.php'; ?>

                <script>


                    function lightbox_open() {
                        window.scrollBy(0, 0);
                        document.getElementById('light').style.display = 'block';
                        document.getElementById('fade').style.display = 'block';

                    }

                    function lightbox_close() {
                        document.getElementById('light').style.display = 'none';

                        document.getElementById('fade').style.display = 'none';
                    }


                </script>

                </body>

                </html>