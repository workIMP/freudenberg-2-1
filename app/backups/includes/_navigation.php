<nav id="nav-primary-wrapper">
    <a class="skiptomain" href="#main">Skip to Main Content</a>
    <div class="logo"><a href="index.php" class="logo_bg">Freudenberg Leisure - Home</a></div>
    <div class="mobile-toggle">MENU<span></span></div>    
    <div class="nav-primary"> 


        <div class="accordion">
            <dl>
                <dt><a href="index.php" aria-controls="accordion1" class="accordion-title accordionTitle">Home</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">&nbsp;</dd>

                <dt>
                    <a href="accordion2" aria-expanded="false" aria-controls="accordion2" class="accordion-title accordionTitle js-accordionTrigger">Hotels</a>
                </dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion2" aria-hidden="true">
                 <a title="Randholee Luxury Resorts" href="http://randholeeresort.freudenbergleisure.com/" >Randholee Luxury Resorts</a> 
                 <a title="The Firs" href="http://firs.freudenbergleisure.com/" >The Firs</a> 
                 <a title="Ellen's Place" href="http://ellensplace.freudenbergleisure.com/">Ellen's Place</a> 
                </dd>

                <dt><a href="experience.php" aria-controls="accordion3" class="accordion-title accordionTitle">Experiences</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion3" aria-hidden="true">&nbsp;</dd>

                <dt><a href="promotions.php" aria-controls="accordion4" class="accordion-title accordionTitle">Special Offers</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion4" aria-hidden="true">&nbsp;</dd>

                <dt><a href="accommodation.php" aria-controls="accordion5" class="accordion-title accordionTitle">Accommodation</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion5" aria-hidden="true">&nbsp;</dd>

                <!--          <dt><a href="travel.php" aria-controls="accordion5" class="accordion-title accordionTitle">Travel</a></dt>
                          <dd class="accordion-content accordionItem is-collapsed" id="accordion5" aria-hidden="true">&nbsp;</dd>-->

                <dt><a href="about-us.php" aria-controls="accordion6" class="accordion-title accordionTitle">About Us</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion6" aria-hidden="true">&nbsp;</dd>

                <dt><a href="contact-us.php" aria-controls="accordion7" class="accordion-title accordionTitle">Contact Us</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion7" aria-hidden="true">&nbsp;</dd>          
            </dl>
        </div>



        <li class="prime-main-link active-accom ddd"><a href="index.php">Home</a></li>
        <li class="prime-main-link active-accom ddd"><a href="#">Hotels</a>
            <ul class="mega-dropdown">

                <li><a title="Randholee Luxury Resorts" href="http://randholeeresort.freudenbergleisure.com/" ><div id="randh"></div><span class="hide">Randholee Luxury Resorts</span></a></li>
                <li><a title="The Firs" href="http://firs.freudenbergleisure.com/" ><div id="firs"></div><span class="hide">The Firs</span></a></li>
                <li><a title="Ellen's Place" href="http://ellensplace.freudenbergleisure.com/"><div id="ellens"></div><span class="hide">Ellen's Place</span></a></li>

            </ul>

        </li>
        <li class="prime-main-link active-accom ddd"><a href="experience.php">Experiences</a></li>      
        <li class="prime-main-link active-accom ddd"><a href="promotions.php">Special Offers</a></li>
        <li class="nav-logo-center"><a href="index.php">Freudenberg Leisure - Home</a></li>
        <li class="prime-main-link active-accom ddd"><a href="accommodation.php">Accommodation</a></li>
        <!--<li class="prime-main-link active-accom ddd"><a href="travel.php">Travel</a></li>-->
        <li class="prime-main-link active-resort ddd"><a href="about-us.php">About Us</a></li>
        <li class="prime-main-link active-here ddd"><a href="contact-us.php">Contact Us</a></li>
        <li class="prime-quick-link quick-link-contact ddd contact-no">Tel: +94 11 2445282</li>
        <li class="prime-quick-link quick-link-contact">
            <div class="search">
                <form>
                    <input name="search" type="text" size="15" class="input">
                    <input type="submit" class="submit" value="SEARCH" style="background:none; border: none;">
                    <div class="clear"></div>
                </form>
            </div>
        </li>
        <div class="prime-quick-link quick-link-contact lanuge ddd">
            <div id="google_translate_element"></div><script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL, autoDisplay: false}, 'google_translate_element');
                }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
    </div>
</nav>