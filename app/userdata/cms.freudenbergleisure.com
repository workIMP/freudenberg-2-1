--- 
customlog: 
  - 
    format: combined
    target: /etc/apache2/logs/domlogs/cms.freudenbergleisure.com
documentroot: /home2/fyfxamvcmvss/cms.freudenbergleisure.com
group: fyfxamvcmvss
hascgi: 1
homedir: /home2/fyfxamvcmvss
ifmodulealiasmodule: 
  scriptalias: 
    - 
      path: /home2/fyfxamvcmvss/cms.freudenbergleisure.com/cgi-bin/
      url: /cgi-bin/
ifmoduleconcurrentphpc: {}

ifmoduleincludemodule: 
  directoryhomefyfxamvcmvsscmsfreudenbergleisurecom: 
    ssilegacyexprparser: 
      - 
        value: " On"
ifmodulelogconfigmodule: 
  ifmodulelogiomodule: {}

ifmoduleuserdirmodule: 
  ifmodulempmitkc: 
    ifmoduleruidmodule: {}

ip: 128.199.220.102
ipv6: ~
no_cache_update: 1
owner: root
phpopenbasedirprotect: 1
port: 82
serveradmin: webmaster@cms.freudenbergleisure.com
serveralias: www.cms.freudenbergleisure.com
servername: cms.freudenbergleisure.com
ssl: 1
usecanonicalname: 'On'
user: fyfxamvcmvss
userdirprotect: ''
