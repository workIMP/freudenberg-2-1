--- 
customlog: 
  - 
    format: combined
    target: /etc/apache2/logs/domlogs/randholeeresort.freudenbergleisure.com
documentroot: /home2/fyfxamvcmvss/public_html/hotels/randholee
group: fyfxamvcmvss
hascgi: 1
homedir: /home2/fyfxamvcmvss
ifmodulealiasmodule: 
  scriptalias: 
    - 
      path: /home2/fyfxamvcmvss/public_html/hotels/randholee/cgi-bin/
      url: /cgi-bin/
ifmoduleconcurrentphpc: {}

ifmoduleincludemodule: 
  directoryhomefyfxamvcmvsspublichtmlhotelsrandholee: 
    ssilegacyexprparser: 
      - 
        value: " On"
ifmodulelogconfigmodule: 
  ifmodulelogiomodule: {}

ifmodulemodincludec: 
  directoryhomefyfxamvcmvsspublichtmlhotelsrandholee: 
    ssilegacyexprparser: 
      - 
        value: " On"
ifmodulemodsuphpc: 
  group: fyfxamvcmvss
ifmoduleuserdirmodule: 
  ifmodulempmitkc: 
    ifmoduleruidmodule: {}

ip: 128.199.220.102
ipv6: ~
is_addon: 1
no_cache_update: 1
owner: root
phpopenbasedirprotect: 1
port: 82
scriptalias: 
  - 
    path: /home/fyfxamvcmvss/public_html/hotels/randholee/cgi-bin/
    url: /cgi-bin/
serveradmin: webmaster@randholeeresort.freudenbergleisure.com
serveralias: kandy.holiday mail.kandy.holiday mail.randholeeresorts.asia mail.randholeeresorts.co.uk mail.randholeeresorts.com mail.randholeeresorts.lk mail.randholeeresorts.ru randholeeresorts.asia randholeeresorts.co.uk randholeeresorts.com randholeeresorts.lk randholeeresorts.ru www.kandy.holiday www.randholeeresort.freudenbergleisure.com www.randholeeresorts.asia www.randholeeresorts.co.uk www.randholeeresorts.com www.randholeeresorts.lk www.randholeeresorts.ru
servername: randholeeresort.freudenbergleisure.com
ssl: 1
usecanonicalname: 'Off'
user: fyfxamvcmvss
userdirprotect: ''
