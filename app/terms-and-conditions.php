<?php 
$pg = ['property' => 'freudenberg', 'page' => 'tandc'];
include 'includes/header.php'; 
?>

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include 'includes/navigation.php'; ?> 
        </header><!--  #header  -->

        <?php include 'includes/booking.php' ?> 
        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">

                            <div class="ctatext-text">         
                                <?php require 'includes/showdescription.php'; ?>
                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include 'includes/footer.php'; ?> 
            </footer>    
    </body>
</html>
